#include "pump_part.h"

Pump::Pump(double ap, double bp, double cp, double dp,
     double ep, double R_recp, double L_perp, double R_perp, double S)
{
    a = ap / pow(1000.0 / 60, 2); // mmHg / (L / min)^2 -> mmHg / (ml / s)^2
    b = bp / (1000.0 / 60); // mmHg / (rpm L / min) -> mmHg / (rpm ml / s)
    c = cp; // mmHg / rpm^2
    d = dp / 1000; // mmHg s^2 / L -> mmHg s^2 / ml
    e = ep * (1000.0 / 60); //  (L / min) / rpm -> (ml / s) / rpm
    R_rec = R_recp / pow(1000.0 / 60, 2); // mmHg / (L / min)^2 -> mmHg / (ml / s)^2
    L_per = L_perp / 1000; //  mmHg s^2 / L -> mmHg s^2 / ml
    R_per = R_perp / pow(1000.0 / 60, 2); // mmHg / (L / min)^2 -> mmHg / (ml / s)^2
    S_pump = S; //cm^2
    w = 0;
}

double Pump::pump_eq(const double & lv_pressure,
               const double & aortic_pressure,
               const double & p_flow)
{
    //in form Q' = ...
    //lv_pressure, aortic_pressure: mmHg
    //p_flow: ml / s
    const double bf = p_flow - e * w;

    return  -1.0 / (d + L_per)
            * (lv_pressure - aortic_pressure
               + a * pow(p_flow, 2)
               + b * w * p_flow
               + c * w * w
               + (bf < 0) * R_rec * bf * bf
               - R_per * pow(p_flow, 2) * (p_flow > 0? 1 : -1)
               );
}

double Pump::pump_eq_d_lvp(const double & lv_pressure,
               const double & aortic_pressure,
               const double & p_flow)
{
    return -1.0 / (d + L_per);
}

double Pump::pump_eq_d_ap(const double & lv_pressure,
               const double & aortic_pressure,
               const double & p_flow)
{
    return 1.0 / (d + L_per);
}

double Pump::pump_eq_d_pflow(const double & lv_pressure,
               const double & aortic_pressure,
               const double & p_flow)
{
    const double bf = p_flow - e * w;

    return  -1.0 / (d + L_per)
            * (2 * a * p_flow
               + b * w
               + (bf < 0) * R_rec * 2 * bf
               - R_per * 2 * p_flow * (p_flow > 0? 1 : -1)
               );
}

void Pump::set_w(const double & ww)
{
    w = ww;
}
