#!/usr/bin/env/python3

import csv


total_r1 = 0
total_r2 = 0
total_c = 0
total_s_r1r2=0
verts = [[[]] for i in range(78)]
with open('adan56.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    for row in csv_reader:
        if line_count == 0:
            print(f'Column names are {", ".join(row)}')
            line_count += 1
        else:
            print(f'\"{row[0]}\": {{')
            print('\"Type\": \"ADAN56\",')
            print(f'\"v1\": \"{row[1]}\",')
            print(f'\"v2\": \"{row[2]}\",')
            print(f'\"length\": {float(row[4])*100},')
            print(f'\"RadProx\": {float(row[6])*100},')
            print(f'\"RadDistal\": {float(row[7])*100}')
            print('},')


            verts[int(row[1])-1][0].append(row[0])
            verts[int(row[2])-1][0].append(row[0])
            if row[10] != "0.":
                verts[int(row[2])-1].append([float(row[10])/10**5,\
                    float(row[11])/10**5,\
                        10**5 * float(row[12])])
                total_r1 += 1.0/(float(row[10])/10**5)
                total_r2 += 1.0/(float(row[11])/10**5)
                total_s_r1r2 += 1.0/( float(row[10])/10**5+ float(row[11])/10**5)
                total_c += 10**5 * float(row[12])
            #print(f'{row[0]} : v1: {row[1]} v2: {row[2]} len: {float(row[4])*100} \
            #Rp: {float(row[6])*100} Rd: {float(row[7])*100} R1: \
            #{float(row[10])} R2: {float(row[11])} C:\
            #{float(row[12])}')
            line_count += 1

    for i in range(1,78):
        print()
        if len(verts[i][0]) == 1:
            print(f'\"WK{i+1}\": {{')
            print('\"Type\": \"Windkessel_vertex\",')
            print(f'\"edge\": \"{verts[i][0][0]}\",')
            print(f'\"vertex\": \"{i+1}\",')
            print('\"P_out\": 0,')
            print(f'\"R1\": {verts[i][1][0]},')
            print(f'\"R2\": {verts[i][1][1]},')
            print(f'\"C\": {verts[i][1][2]}')
        else:
            print(f'\"Internal{i+1}\": {{')
            print('\"Type\": \"Internal\",')
            print(f'\"edges\": {verts[i][0]},')
            print(f'\"vertex\": \"{i+1}\"')

        print('},')

print(f'total R1: {1.0/total_r1}, total R2: {1.0/total_r2}, total C:\
        {total_c}, total R1R2: {1.0/total_s_r1r2} vs R1+R2:\
        {1.0/total_r1+1.0/total_r2}')
