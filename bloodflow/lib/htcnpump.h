#ifndef HTCNPUMP_H
#define HTCNPUMP_H

#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include <Eigen/Dense>
#include <cmath>

#include "vertex.h"

template<typename Edge,
         template<typename, int, int ...> typename Matrix, typename Scalar, auto Dynamic>
class H2CV_Pump:
    public Pump_0d_heart<Edge, Matrix, Scalar, Dynamic>
{
    typedef Pump_0d_heart<Edge, Matrix, Scalar, Dynamic> Base;
    using Base::OutgoingCompatibilityCoeffs;
    using Base::T;
    using Base::dt;
    using Base::id;

    using Base::get_pressure;
    using Base::get_eos_deriv;
    using Base::setSU;


    typedef Eigen::Matrix<double, 12, 12> Matrix12d;
    typedef Eigen::Matrix<double, 9, 9> Matrix9d;
    typedef Eigen::Matrix<double, 8, 8> Matrix8d;
    typedef Eigen::Matrix<double, 9, 12> Matrix912;

    typedef Eigen::Matrix<double, 9, 1> Vector9d;
    typedef Eigen::Matrix<double, 12, 1> Vector12d;
    typedef Eigen::Matrix<double, 1, 12> RowVector12d;

    enum {v1, v1_d, v4, v4_d, tet51, tet51_d, tet14, tet14_d, qpump, s0, s1, s2};

    Edge * edge1; //from valve to the pump
    Edge * edge2; //from the pump to the end
    Simple_vertex * heart_v;
    Simple_vertex * pump_connect_v;
    const double cc = 1333.22;

    double alfa0, beta0; //flow from valve to pump
    double alfa1, beta1; //flow from pump to valve
    double alfa2, beta2; //flow from pump to the end of the ascending aorta


    //pump stuff
        const double S_p = M_PI/4;//test it! pump area
        const double ce1 = -8.5e-1 * cc * 0.0036;
        const double ce2 = -7.3e-4 * cc * 0.06;
        const double ce3 = 7.5e-7 * cc;
        const double ce4 = -9.89e+1 * cc * 6e-4  +  15*cc/1000 ; //plus periphial
        const double ce = 1.174422e-4 * 1000/60;
        const double ancf = 6.62775 * cc * 0.0036;
        const double Rper = 2e-1 * cc/278; //plus periphial
        double w;



        //pump 2
        const double b1 = -1.15e+1 * cc * 0.06;
        const double b2 = 8e-7 * cc;
        const double b3 = -1.055e+2 * cc * 6e-4;

        double H_pump;
        double density;
        double viscosity;
public:
        void set_w(const double & ww)
        {
            w = ww;
        }
        double get_H()
        {
            return H_pump;
        }
        double get_Q()
        {
            return y0(qpump);
        }
private:


    double pump_eq(const double & lv_pressure,
                   const double & aortic_pressure,
                   const double & p_flow)
    {//no mmhg!

        //in form Q' = ...
       // return (aortic_pressure-lv_pressure - b1*p_flow -b2*w*w)/b3;

        const double bf = p_flow - ce*w;
        return -1.0/ce4*(lv_pressure - aortic_pressure
                        + ce1*pow(p_flow, 2)
                        + ce2*w*p_flow
                        + ce3*w*w
                        + (bf < 0)*ancf*bf*bf
                        - Rper*pow(p_flow, 2)*(p_flow > 0? 1 : -1)
                        );

    }

    double pump_eq_d_lvp(const double & lv_pressure,
                   const double & aortic_pressure,
                   const double & p_flow)
    {//no mmhg!
        //in form Q' = ...
        //return -1.0/b3;
        return -1.0/ce4;
    }
    double pump_eq_d_ap(const double & lv_pressure,
                   const double & aortic_pressure,
                   const double & p_flow)
    {//no mmhg!
        //in form Q' = ...
        //return 1.0/b3;

            return 1.0/ce4;
    }

    double pump_eq_d_pflow(const double & lv_pressure,
                   const double & aortic_pressure,
                   const double & p_flow)
    {//no mmhg!
        //in form Q' = ...
      //  return - b1/b3;

        const double bf = p_flow - ce*w;
        return -1.0/ce4*(2*ce1*p_flow
                        + ce2*w
                        + (bf < 0)*2*ancf*bf
                        - 2*Rper*p_flow*(p_flow > 0? 1 : -1)
                        );

    }



    const double r2d = M_PI/180;

    //flow coefficient of atrium entering, ml/(s*mmHg^0.5)

    const double CQ48 = 25.;// where it cames from, nothing in original article

    //flow coefficient of aortic valve, ml/(s*mmHg^0.5)

    const double CQ51 = CQ48/3*8;//50;//160;//

    //flow coefficient of mitral valve, ml/(s*mmHg^0.5)

    const double CQ14 = CQ48/3*10;//57;//200;//


    //peak of systolic phase

    const double Ts1 = 0.3;

    //end of systolic phase

    const double Ts2 = 0.44;

    const double Tpb = 0.9;

    const double Tpw = 0.1;





    //pressure in pulmonary veins, mmHg

    double P8;//from config file (try different values up to 12 mmHg)


    const double V1_0;//set in config file 5 for normal, a bit more for failure
    const double V4_0 = 20;

    //elastance of ventricle in diastolic phase, mmHg/ml

    const double E1d;

    //elastance of ventricle in systolic phase, mmHg/ml

    double E1s;




    //elastance of atrium in systolic phase, mmHg/ml

    const double E4s = 1.5;

    //elastance of atrium in diastolic phase, mmHg/ml

    const double E4d = 0.2;

    //coefficient of pressure force effect in valve)

    const double Kp = 5500;

    //coefficient of frictional force effect in valve

    const double Kf = 50;


    //max opening angle of both valves, degrees

    const double thetamax1_n = 75;
    const double thetamax = thetamax1_n*r2d;
    const double thetamin1_n = 0;
    const double thetamin = thetamin1_n*r2d;
    const double AR0 = (1-cos(thetamax))*(1-cos(thetamax));

    //duration of heart period, s

    const double Period = 1;

    //coefficient of inertia effect in ventricle

    const double I1 = 1.e-5;

    //coefficient of inertia effect in atrium

    const double I4 = 1.e-4;

    //coefficient of resistance of ventricle

    const double R1 = 4.e-5;

    //cofficient of resistance of atrium

    const double R4 = 4.e-5;


    //time normalization
    inline double tnorm (const double & t) {
        return fmod(t, Period);
    }

    //ventricular activation function

    double e10 (const double & tcurr) {

        const double t = tnorm(tcurr);
        assert(t >= 0);

        if (t < Ts1) {

            return 1.0 - cos(t*M_PI/Ts1);

        } else if (t < Ts2) {

            return  1.0 - cos((t-Ts2)*M_PI/(Ts1-Ts2));

        } else {

            return 0;

        }
    }

    double e10_t (const double & tcurr) {

        const double t = tnorm(tcurr);
        assert(t >= 0);
        if (t < Ts1) {

            return sin(t*M_PI/Ts1)*M_PI/Ts1;

        } else if (t <= Ts2) {

            return sin((t-Ts2)*M_PI/(Ts1-Ts2))*M_PI/(Ts1-Ts2);

        } else {

            return 0;

        }
    }

    //atrium activation function

    /*
        double e40 (const double & tcurr) {

            const double t = tnorm(tcurr);
            assert(t >= 0);

            if (t < Tpb || t > Tpb + Tpw) {

                return 0;

            } else {

                return 1 - cos((t-Tpb)*2*M_PI/Tpw);

            }
        }

        double e40_t (const double & tcurr) {

            const double t = tnorm(tcurr);
            assert(t >= 0);

            if (t < Tpb || t > Tpb + Tpw) {

                return 0;

            } else {

                return sin((t-Tpb)*2*M_PI/Tpw)*2*M_PI/Tpw;

            }
        }
    */

        double e40 (const double & tcurr) {

            const double t = tnorm(tcurr);
            assert(t >= 0);
            const double t1 = 0.07;
            const double t2 = 0.03;
            if (t < Tpb) {

                return 0;

            } else if (t < Tpb + t1) {

                return 1.0 - cos((t - Tpb) * M_PI / t1);

            } else {

                return 1.0 - cos((Tpb + t1 + t2 - t) * M_PI / t2);

            }
        }

        double e40_t (const double & tcurr) {

            const double t = tnorm(tcurr);
            assert(t >= 0);
            const double t1 = 0.07;
            const double t2 = 0.03;
            if (t < Tpb) {

                return 0;

            } else if (t < Tpb + t1) {

                return sin((t - Tpb) * M_PI / t1)* M_PI / t1;

            } else {

                return  sin((t - Tpb - t1) * M_PI / (t2 - t1))* M_PI / (t2 - t1);

            }
        }

    //time-varying ventricular elastance

    double e1 (const double & t) {

        return E1d + (E1s-E1d)*e10(t)/2.0;

    }

    double e1_t (const double & t) {

        return  (E1s-E1d)*e10_t(t)/2.0;

    }


    //time-varying atrium elastance

    double e4 (const double & t) {

        return E4d + (E4s-E4d)*e40(t)/2.0;

    }

    double e4_t (const double & t) {

        return (E4s-E4d)*e40_t(t)/2.0;

    }


    //valve opening function
    const double veps = 1e-9; //doesnt work with 0

    double AR (const double & y) {
        if (y <= thetamin) {

            return veps;

        } else if (y >= thetamax) {

            return 1;

        } else {

            return (1-veps)*(1-cos(y))*(1-cos(y))/AR0 + veps;

        }

    }

    double AR_y (const double & y) {

        if (y < thetamax && y > thetamin) {

            return (1-veps)*2*(1-cos(y))*sin(y)/AR0;

        } else {

            return 0;

        }
    }

    double AR_yy (const double & y) {

        if (y < thetamax && y > thetamin) {

            return (1-veps)*2*(sin(y)*sin(y)+(1-cos(y))*cos(y))/AR0;

        } else {

            return 0;

        }
    }

    //coefficient of resistance force in the valve
    //both work fine
    const double ec = 1000;
    const double thetaeps = 0;
    double Kr (const double & y) {
        if (y >= thetamax) {
            return expm1(ec*(y-thetamax));
        } else if (y <= thetamin+thetaeps) {
            return -expm1(ec*(thetamin+thetaeps-y));
        } else {
            return 0;
        }
    }
    double Kr_y (const double & y) {
        if (y >= thetamax) {
            return ec*exp(ec*(y-thetamax));
        } else if (y <= thetamin+thetaeps) {
            return ec*exp(ec*(thetamin+thetaeps-y));
        } else {
            return 0;
        }
    }
    double Kr_yy (const double & y) {
        if (y >= thetamax) {
            return ec*ec*exp(ec*(y-thetamax));
        } else if (y <= thetamin+thetaeps) {
            return -ec*ec*exp(ec*(thetamin+thetaeps-y));
        } else {
            return 0;
        }
    }

    /*
    double Kr (const double & y) {

        if (y >= thetamax) {

            return Kr0+1000000*atan(10*(y-thetamax));

        } else if (y <= thetamin) {

            return Kr0-1000000*atan(10*(y-thetamin));

        } else {

            return Kr0;

        }
    }

    double Kr_y (const double & y) {

        if (y >= thetamax) {

            return 10000000/(100*(y-thetamax)*(y-thetamax)+1);

        } else if (y <= thetamin) {

            return -10000000/(100*(y-thetamin)*(y-thetamin)+1);

        } else {

            return 0;

        }
    }

    double Kr_yy (const double & y) {

        if (y >= thetamax) {

            return -(2000000000*(y-thetamax))/
            ((100*(y-thetamax)*(y-thetamax)+1)*(100*(y-thetamax)*(y-thetamax)+1));

        } else if (y <= thetamin) {

            return (2000000*(y-thetamin))/
            ((100*(y-thetamin)*(y-thetamin)+1)*(100*(y-thetamin)*(y-thetamin)+1));

        } else {
            return 0;
        }
    }
    */


    //pressure in aorta
    double P5(const double & Sq)
    {
        return edge1->get_pressure(Sq, heart_v, 0)/cc;
    }

    double P5_d(const double & Sq)
    {
        return edge1->get_d_pressure_d_s(Sq, heart_v, 0)/cc;
    }

    //pressure in left ventricle

    double P1 (const double & Sq, const double & teta) {

        return P5(Sq) + Sq*(alfa0*Sq + beta0) / (CQ51*AR(teta));

    }
    double P1_s (const double & Sq, const double & teta) {

        return P5_d(Sq) + (2*alfa0*Sq + beta0) / (CQ51*AR(teta));

    }

    double P1_tet (const double & Sq, const double & teta) {

        return  - AR_y(teta)* Sq * (alfa0*Sq + beta0) / (CQ51 * pow(AR(teta), 2));

    }

    //pressure in left atrium

    double P4 (const double & V1_D, const double & V4_D, const double & Sq, const double & QPump) {

        return P8 - (V1_D + QPump + V4_D + Sq*(alfa0*Sq + beta0))/CQ48;

    }

    double P4_v1_d (const double & V1_D, const double & V4_D, const double & Sq, const double & QPump) {

        return - 1/CQ48;

    }

    double P4_qpump (const double & V1_D, const double & V4_D, const double & Sq, const double & QPump) {

        return - 1/CQ48;

    }

    double P4_v4_d (const double & V1_D, const double & V4_D, const double & Sq, const double & QPump) {

        return - 1/CQ48;

    }
    double P4_s (const double & V1_D, const double & V4_D, const double & Sq, const double & QPump) {

        return - (2*alfa0*Sq + beta0)/CQ48;

    }


    //frictional force

    double Ff (const double & y3) {

        return Kf*y3;

    }

    double Ff_tet_d (const double & y3) {
        return Kf;
    }

    double Ff_y3y3 (const double & y3) {
        return 0;
    }

    //resistance force

    double Fr (const double & y2) {

        return Kr(y2);

    }

    double Fr_tet (const double & y2) {

        return Kr_y(y2);

    }

    double Fr_y2y2 (const double & y2) {

        return Kr_yy(y2);

    }

    //pressure force

    double Fp1 (const double & Sq, const double & tet) {

        return Kp*(P1(Sq, tet) - P5(Sq))
                 *cos(tet);

    }

    double Fp1_s (const double & Sq, const double & tet) {

        return Kp*(P1_s(Sq, tet) - P5_d(Sq))
                 *cos(tet);

    }
    double Fp1_tet (const double & Sq, const double & tet) {

        return Kp*(P1(Sq, tet) - P5(Sq))
                 *(-sin(tet)) +
                Kp*P1_tet(Sq, tet)*cos(tet);

    }

    double Fp2 (const double & V1_D, const double & V4_D, const double & Sq, const double & TET51, const double & TET14, const double & QPump) {

        return Kp*(P4(V1_D, V4_D, Sq, QPump) - P1(Sq, TET51))*cos(TET14);

    }
    double Fp2_v1_d (const double & V1_D, const double & V4_D, const double & Sq, const double & TET51, const double & TET14, const double & QPump) {

        return Kp*P4_v1_d(V1_D, V4_D, Sq, QPump)*cos(TET14);

    }
    double Fp2_qpump (const double & V1_D, const double & V4_D, const double & Sq, const double & TET51, const double & TET14, const double & QPump) {

        return Kp*P4_qpump(V1_D, V4_D, Sq, QPump)*cos(TET14);

    }
    double Fp2_v4_d(const double & V1_D, const double & V4_D, const double & Sq, const double & TET51, const double & TET14, const double & QPump) {

        return Kp*P4_v4_d(V1_D, V4_D, Sq, QPump)*cos(TET14);

    }
    double Fp2_s(const double & V1_D, const double & V4_D, const double & Sq, const double & TET51, const double & TET14, const double & QPump) {

        return Kp*(P4_s(V1_D, V4_D, Sq, QPump) - P1_s(Sq, TET51))*cos(TET14);

    }
    double Fp2_tet51(const double & V1_D, const double & V4_D, double Sq, double TET51, double TET14, const double & QPump) {

        return Kp*(- P1_tet(Sq, TET51))*cos(TET14);

    }
    double Fp2_tet14(const double & V1_D, const double & V4_D,
                     const double & Sq, const double & TET51, const double & TET14, const double & QPump) {

        return Kp*(P4(V1_D, V4_D, Sq, QPump) - P1(Sq, TET51))*(-sin(TET14));

    }



    double D1(const Vector12d & y)
    {//s1 and qpump only function!
        //no mmhg
        return edge1->get_pressure(y(s1), pump_connect_v, 0) + density*pow( alfa1*y(s1) + beta1, 2)/2 -
                density*pow(y(qpump)/S_p, 2)/2;
    }
    double D1_d_s1(const Vector12d & y)
    {
        return edge1->get_d_pressure_d_s(y(s1), pump_connect_v, 0) + density*alfa1*(alfa1*y(s1) + beta1);
    }
    double D1_d_qpump(const Vector12d & y)
    {
        return -density*y(qpump)/(S_p*S_p);
    }
    //right part of system
    Vector9d F (const Vector12d & y, const double & t) {
        Vector9d func;

        func(0) = y(v1_d);
        func(1) = -R1*y(v1_d)/I1 - e1(t)*(y(v1) - V1_0)/I1 + P1(y(s0), y(tet51))/I1;
        func(2) = y(v4_d);
        func(3) = -R4*y(v4_d)/I4 - e4(t)*(y(v4) - V4_0)/I4 + P4(y(v1_d), y(v4_d), y(s0), y(qpump))/I4;
        func(4) = y(tet51_d);
        func(5) = -Fr(y(tet51)) - Ff(y(tet51_d)) + Fp1(y(s0), y(tet51));
        func(6) = y(tet14_d);
        func(7) = -Fr(y(tet14)) - Ff(y(tet14_d)) + Fp2(y(v1_d), y(v4_d), y(s0), y(tet51), y(tet14), y(qpump));
        func(8) = pump_eq(cc*P1(y(s0), y(tet51)), D1(y), y(qpump));//it asks not mmhg

        return func;
    }

    Matrix912 F_d(const Vector12d & y, const double & t) {
        Matrix912 res;
        res.setZero();

        res(0, v1_d) = 1;

        res(1, v1_d) = -R1/I1;
        res(1, v1) = - e1(t)/I1;
        res(1, tet51) = P1_tet(y(s0), y(tet51))/I1;
        res(1, s0) = P1_s(y(s0), y(tet51))/I1;

        res(2, v4_d) = 1;

        res(3, v4_d) = -R4/I4 + P4_v4_d(y(v1_d), y(v4_d), y(s0), y(qpump))/I4;
        res(3, v4) = - e4(t)/I4;
        res(3, v1_d) = P4_v1_d(y(v1_d), y(v4_d), y(s0), y(qpump))/I4;
        res(3, s0) = P4_s(y(v1_d), y(v4_d), y(s0), y(qpump))/I4;
        res(3, qpump) = P4_qpump(y(v1_d), y(v4_d), y(s0), y(qpump))/I4;


        res(4, tet51_d) = 1;

        res(5, tet51) = -Fr_tet(y(tet51)) + Fp1_tet(y(s0), y(tet51));
        res(5, tet51_d) = - Ff_tet_d(y(tet51_d));
        res(5, s0) = Fp1_s(y(s0), y(tet51));

        res(6, tet14_d) = 1;

        res(7, tet14) = -Fr_tet(y(tet14)) + Fp2_tet14(y(v1_d), y(v4_d), y(s0), y(tet51), y(tet14), y(qpump));
        res(7, tet14_d) = - Ff_tet_d(y(tet14_d));
        res(7, tet51) = Fp2_tet51(y(v1_d), y(v4_d), y(s0), y(tet51), y(tet14), y(qpump));
        res(7, s0) = Fp2_s(y(v1_d), y(v4_d), y(s0), y(tet51), y(tet14), y(qpump));
        res(7, v1_d) = Fp2_v1_d(y(v1_d), y(v4_d), y(s0), y(tet51), y(tet14), y(qpump));
        res(7, qpump) = Fp2_qpump(y(v1_d), y(v4_d), y(s0), y(tet51), y(tet14), y(qpump));

        res(7, v4_d) = Fp2_v4_d(y(v1_d), y(v4_d), y(s0), y(tet51), y(tet14), y(qpump));
//again no mmhg, take care of cc!
        res(8, s0) = pump_eq_d_lvp(cc*P1(y(s0), y(tet51)), D1(y), y(qpump))*cc*P1_s(y(s0), y(tet51));
        res(8, tet51) = pump_eq_d_lvp(cc*P1(y(s0), y(tet51)), D1(y), y(qpump))*cc*P1_tet(y(s0), y(tet51));
        res(8, s1) = pump_eq_d_ap(cc*P1(y(s0), y(tet51)), D1(y), y(qpump))*D1_d_s1(y);
        res(8, qpump) = pump_eq_d_ap(cc*P1(y(s0), y(tet51)), D1(y), y(qpump))*D1_d_qpump(y)
                       + pump_eq_d_pflow(cc*P1(y(s0), y(tet51)), D1(y), y(qpump));

        return res;
    }


    double f(const Vector12d & y)
    {
        const double coef14 = CQ14*AR(y(tet14));
        const double coef51 = CQ51*AR(y(tet51));

        const double A = (coef14 + coef51)*CQ48 + coef14*coef51;
        const double G = coef14*coef51*CQ48;
        const double L = coef14*coef51;
        const double M = L + CQ48*coef51;

        return A*y(s0)*(alfa0*y(s0) + beta0) + G*(P5(y(s0)) - P8)
               + L*y(v4_d) + M*y(v1_d) + M*y(qpump);

    }
    RowVector12d f_d(const Vector12d & y)
    {
        RowVector12d res;
        res.setZero();

        const double coef14 = CQ14*AR(y(tet14));
        const double coef14_d14 = CQ14*AR_y(y(tet14));
        const double coef51 = CQ51*AR(y(tet51));
        const double coef51_d51 = CQ51*AR_y(y(tet51));

        const double A = (coef14 + coef51)*CQ48 + coef14*coef51;
        const double A_d14 = CQ48*coef14_d14 + coef51*coef14_d14;
        const double A_d51 = CQ48*coef51_d51 + coef14*coef51_d51;

        const double L = coef14*coef51;
        const double L_d14 = coef51*coef14_d14;
        const double L_d51 = coef14*coef51_d51;

        const double G = L*CQ48;
        const double G_d14 = CQ48*L_d14;
        const double G_d51 = CQ48*L_d51;

        const double M = L + CQ48*coef51;
        const double M_d14 = L_d14;
        const double M_d51 = L_d51 + CQ48*coef51_d51;

        res(v1) = 0;
        res(v1_d) = M;
        res(qpump) = M;
        res(v4) = 0;
        res(v4_d) = L;
        res(tet51) = A_d51*y(s0)*(alfa0*y(s0) + beta0) + G_d51*(P5(y(s0)) - P8)
                + L_d51*y(v4_d) + M_d51*y(v1_d);
        res(tet51_d) = 0;
        res(tet14) = A_d14*y(s0)*(alfa0*y(s0) + beta0) + G_d14*(P5(y(s0)) - P8)
                + L_d14*y(v4_d) + M_d14*y(v1_d);
        res(tet14_d) = 0;
        res(s0) = A*(2*alfa0*y(s0) + beta0) + G*P5_d(y(s0));

        return res;
    }

    double g(const Vector12d & y)
    {

        return y(qpump) - y(s1)*(alfa1*y(s1) + beta1) - y(s2)*(alfa2*y(s2) + beta2);

    }
    RowVector12d g_d(const Vector12d & y)
    {
        RowVector12d res;
        res.setZero();

        res(qpump) = 1;
        res(s1) = - (2*alfa1*y(s1) + beta1);
        res(s2) = - (2*alfa2*y(s2) + beta2);
        return res;
    }

    double z(const Vector12d & y)
    {
        return edge1->get_pressure(y(s1), pump_connect_v, 0)
              - edge2->get_pressure(y(s2), pump_connect_v, 0)
               + density*pow(alfa1*y(s1) + beta1, 2)/2
              -  density*pow(alfa2*y(s2) + beta2, 2)/2;
    }
    RowVector12d z_d(const Vector12d & y)
    {
        RowVector12d res;
        res.setZero();
        res(s1) = edge1->get_d_pressure_d_s(y(s1), pump_connect_v, 0)
                 + density*alfa1*(alfa1*y(s1) + beta1);
        res(s2) = - edge2->get_d_pressure_d_s(y(s2), pump_connect_v, 0)
                -  density*alfa2*(alfa2*y(s2) + beta2);
        return res;
    }


    //flows
    double Q51 (const Vector9d & y) {
        return y(s0)*(alfa0*y(s0) + beta0);
    }

    double Q14 (const Vector9d & y) {
        return y(v1_d) + y(qpump) + Q51(y);
    }


    Vector12d y0, yn, R;
    Matrix12d B;

    double LV_V, LV_P;
    double LA_V, LA_P;
    double aortic_valve, mitral_valve;
public:
    double get_LV_V()
    {
        return LV_V;
    }
    double get_LV_P()
    {
        return LV_P;
    }
    double get_LA_V()
    {
        return LA_V;
    }
    double get_LA_P()
    {
        return LA_P;
    }
    double get_aortic_valve()
    {
        return aortic_valve;
    }
    double get_mitral_valve()
    {
        return mitral_valve;
    }
    double get_flow_av()
    {
        return y0(s0)*(alfa0*y0(s0) + beta0);
    }
    double get_flow_total()
    {
        return y0(s2) * (alfa2 * y0(s2) + beta2);
    }
    double get_ESPVR()
    {
        return E1s;
    }
    void set_ESPVR(double new_ESPVR)
    {
        E1s = new_ESPVR;
    }
    double get_PveinPressure()
    {
        return P8;
    }
    void set_PveinPressure(double new_PveinPressure)
    {
        P8 = new_PveinPressure;
    }
    double get_aortic_root_pressure()
    {
        return edge1->get_pressure(y0(s0), heart_v, 0)/cc;
    }
bool is_ft;
int ff;

    H2CV_Pump(const std::string & id, Edge * edge1, Edge * edge2, Simple_vertex * heart, Simple_vertex * pump_connect,
              double pulmVeinsPressure, double initLVvolume, double LV_V0, double LV_EDPVR, double LV_ESPVR)
    : Base(id), edge1(edge1), edge2(edge2), heart_v(heart), pump_connect_v(pump_connect),
        P8(pulmVeinsPressure), V1_0(LV_V0),
        E1d(LV_EDPVR), E1s(LV_ESPVR)
    {
        w = 0;
        ff = 0;
is_ft = 1;
        y0(v1) =      initLVvolume;
        y0(v1_d) =    0;
        y0(v4) =      70;
        y0(v4_d) =    0;
        y0(tet51) =   0.1;
        y0(tet51_d) = 0;
        y0(tet14) =   0.1;
        y0(tet14_d) = 0;
        y0(qpump) =   0;
        y0(s0) =      10;
        y0(s1) =      10;
        y0(s2) =      10;

        B.setZero();
        R.setZero();
        yn.setZero();
        H_pump = 0;
        density = edge1->get_density();
    }
    void update()
    {
        OutgoingCompatibilityCoeffs(edge1, heart_v, alfa0, beta0);
        OutgoingCompatibilityCoeffs(edge1, pump_connect_v, alfa1, beta1);
        OutgoingCompatibilityCoeffs(edge2, pump_connect_v, alfa2, beta2);

        if (is_ft) {
            is_ft = 0;
            y0(qpump) = 4.9*(alfa1*4.9 + beta1)
                         + 4.9*(alfa2*4.9 + beta2);
            y0(qpump) =   10;

        }

        double ts = T-dt;
        double tau = dt;
        while (tau > 1e-12) {

            double t = ts + tau;
            yn = y0;

            int j = 0;
            double R0_norm = 0;
            const double atol = 1e-5, rtol = 1e-12;
            int MaxIt = 5;//was 20
            while (1) {
                //Newton
                if (j > MaxIt) {
                    j = 0;
                    //yn = y0;
              //      if ( R.lpNorm<Eigen::Infinity>() < 1e-5 && R.lpNorm<Eigen::Infinity>() < 1e-9*R0_norm && T < 0.1)
                //        break;
                    tau/=2;
                    //std::cout << "!" << std::endl;
                    t = ts + tau;

                    if (tau < 1e-12) {
                        std::cout << "TIME: " << T << std::endl;
                        std::cout << y0 << std::endl;
                        std::cout << yn << std::endl;
                        std::cout << "Residual:" << std::endl;
                        std::cout << R << std::endl;
                        std::cout << "Residual nor,: " << R.lpNorm<Eigen::Infinity>() << std::endl;
                        std::cout << "Relative error: " << R.lpNorm<Eigen::Infinity>()/R0_norm << std::endl;
                        //break;

                        throw("Newton from Heart with Pump and valves failed");
                     }
                }

                R.head<9>() = yn.head<9>() - y0.head<9>() - tau * F(yn, t);
                R(9) = f(yn);
                R(10) = g(yn);
                R(11) = z(yn);

                if (R0_norm == 0) {
                    R0_norm = R.lpNorm<Eigen::Infinity>();
                }
//std::cout << "RES NORM: " << R.lpNorm<Eigen::Infinity>() << std::endl;
//std::cout << "Residual:" << std::endl;
//std::cout << R << std::endl;
                if (R.lpNorm<Eigen::Infinity>() < atol || R.lpNorm<Eigen::Infinity>() < rtol * R0_norm) {
                   // std::cout << R.lpNorm<Eigen::Infinity>() << std::endl;
                    break;
                }
                B.setZero();
                B.topLeftCorner<9, 9>().setIdentity();
                B.topLeftCorner<9, 12>() -= tau * F_d(yn, t);
                B.row(9) = f_d(yn);
                B.row(10) = g_d(yn);
                B.row(11) = z_d(yn);

                yn -= B.colPivHouseholderQr().solve(R);
                j++;
            }
            y0 = yn;
            //std::cout << y0(qpump) << std::endl;
    /*
            const auto f_yynt = f_y(yn,t);
            const auto eigenvalues = f_yynt.eigenvalues();
            for (int i = 0; i < 8; i++) {
                if (eigenvalues(i).real() > 1)
                    cout << t << " " << eigenvalues(i) << endl;
            }
    */
            ts += tau;
            tau = T - ts;
        }


        if (y0(v1) < 10 || y0(v4) < 2) {
            std::cout << "time: " << T << std::endl;
            std::cout << "LV vol/pressure: " << y0(v1) << " " << P1(y0(s0), y0(tet51)) << std::endl;
            std::cout << "LA vol/pressure: " << y0(v4) << " " << P4(y0(v1_d), y0(v4_d), y0(s0), y0(qpump)) << std::endl;

            throw("OMG");
        }
        /*
        if (T >= 6 && T <= 7) {
            ff++;
            if (ff % 6 == 0)
                std::cout << y0(v1) << " " << P1(y0(s0), y0(tet51)) << std::endl;
        }
        */
        LV_V = y0(v1);
        LV_P = P1(y0(s0), y0(tet51));
        LA_V = y0(v4);
        LA_P = P4(y0(v1_d), y0(v4_d), y0(s0), y0(qpump));
        aortic_valve = y0(tet51)/r2d;
        mitral_valve = y0(tet14)/r2d;
        H_pump = edge1->get_pressure(y0(s1), pump_connect_v, 0)/cc - LV_P;
        //save results
        setSU(edge1, heart_v, y0(s0), alfa0*y0(s0) + beta0);
        setSU(edge1, pump_connect_v, y0(s1), alfa1*y0(s1) + beta1);
        setSU(edge2, pump_connect_v, y0(s2), alfa2*y0(s2) + beta2);
    }
};


#endif // HTCNPUMP_H
