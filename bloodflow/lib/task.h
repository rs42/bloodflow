﻿#ifndef TASK_H
#define TASK_H

#include <Eigen/Dense>
#include <unordered_map>
#include <exception>
#include "edge.h"
#include "vertex.h"
#include "heart_twocompvalves.h"
#include "htcnpump.h"
#include "rcrwindkessel.h"
#include "heart_advanced_valves.h"
#include "hav_pump.h"

/**
 * @brief Error class to throw while reading config file with vascular graph
 */
class GraphConfigError
: public std::logic_error
{
public:
  explicit GraphConfigError(const std::string& what_arg);
  explicit GraphConfigError(const char* what_arg);
};


/**
 * @brief Pop key-value pair from map and return value
 */
template<typename Map, typename Key>
auto map_pop(Map & map, const Key & key)
{
    auto iter = map.find(key);
    if (iter == map.end()) {
        throw std::out_of_range("map_pop: no key was found");
    } else {
        const auto res = iter->second;
        map.erase(iter);
        return res;
    }
}


/**
 * @brief Class for problem initialization and run
 */
struct Task
{
    typedef ::Edge<Eigen::Matrix, double, Eigen::Dynamic> Edge;
    typedef ::EdgeClassicTubeLaw<Eigen::Matrix, double, Eigen::Dynamic>  EdgeClassicTubeLaw;
    typedef ::EdgeAdan56TubeLaw<Eigen::Matrix, double, Eigen::Dynamic>  EdgeAdan56TubeLaw;

    typedef ::Vertex<Edge, Eigen::Matrix, double, Eigen::Dynamic> Vertex;
    typedef ::Internal_vertex<Edge, Eigen::Matrix, double, Eigen::Dynamic> Internal_vertex;
    //typedef ::Free_flow_vertex<Edge, Eigen::Matrix, double, Eigen::Dynamic> Free_flow_vertex;
    typedef ::TerminalVertex<Edge, Eigen::Matrix, double, Eigen::Dynamic> TerminalVertex;
    typedef ::RCR_Windkessel_vertex<Edge, Eigen::Matrix, double, Eigen::Dynamic> Windkessel_vertex;
    typedef ::TerminalResistanceCoronaryArteries<Edge, Eigen::Matrix, double, Eigen::Dynamic> TerminalResistanceCoronaryArteries;

    typedef ::Heart<Edge, Eigen::Matrix, double, Eigen::Dynamic> Heart;
    typedef ::Constant_flow_heart<Edge, Eigen::Matrix, double, Eigen::Dynamic> Constant_flow_heart;
    typedef ::Simple_heart<Edge, Eigen::Matrix, double, Eigen::Dynamic> Simple_heart;

    typedef ::True_0d_heart<Edge, Eigen::Matrix, double, Eigen::Dynamic> True_0d_heart;
    typedef ::Heart_2CV<Edge, Eigen::Matrix, double, Eigen::Dynamic> Heart_2CV;
    typedef ::Heart_AdValves<Edge, Eigen::Matrix, double, Eigen::Dynamic> Heart_AdValves;

    typedef ::Pump_0d_heart<Edge, Eigen::Matrix, double, Eigen::Dynamic> Pump_0d_heart;
    typedef ::H2CV_Pump<Edge, Eigen::Matrix, double, Eigen::Dynamic> H2CV_Pump;
    typedef ::Heart_AV_pump<Edge, Eigen::Matrix, double, Eigen::Dynamic> Heart_AV_pump;

    double virtual_time; // time inside a model
    double time_max; // max time inside a model

    double computation_time; // time spent on computations
    double Courant_number;
    double max_abs_eigenvalue_div_dx;

    std::vector<Edge *> edges;
    std::vector<Simple_edge *> s_edges; // pointers to Edge objects from edges vector but casted to Simple_edge

    std::vector<Simple_vertex *> s_vertices;
    std::vector<Vertex *> vertices;

    std::vector<Windkessel_vertex *> wk_vertices;
    std::vector<TerminalVertex *> terminal_vertices;

    std::unordered_map<std::string, Simple_vertex *> s_vert_map;
    std::unordered_map<std::string, Edge *> edge_map;

    std::vector<Edge *> edges_to_study;

    Heart * heart; ///@todo pointer is stored in vertices vector which is cleaned in the Task destructor but better to use smart pointers
    True_0d_heart * true_0d_heart;
    Pump_0d_heart * pump_0d_heart;

private:
    double density, viscosity, zeta, dx_step, gamma, terminalResistanceScaler;

public:

    template<typename Mjson>
    void get_metavertex_from_config(const Mjson &mv)
    {
        double w = 0; // default pump speed
        const std::string type = mv.value()["Type"].template get<std::string>();
        if (type == "Heart_AV_pump") {
            Edge * e0, * e1, * e2;
            try {
                e0 = edge_map.at(mv.value()["edge0"].template get<std::string>());
                e1 = edge_map.at(mv.value()["edge1"].template get<std::string>());
                e2 = edge_map.at(mv.value()["edge2"].template get<std::string>());
            } catch (const std::out_of_range &) {
                throw(GraphConfigError(mv.key() + ": cannot find its edges among Edges"));
            }

            Simple_vertex * sv0, *sv1;
            try {
                sv0 = map_pop(s_vert_map, mv.value()["vertex0"].template get<std::string>());
                sv1 = map_pop(s_vert_map, mv.value()["vertex1"].template get<std::string>());
                //sv0 = s_vert_map.at(mv.value()["vertex0"].get<std::string>());
                //sv1 = s_vert_map.at(mv.value()["vertex1"].get<std::string>());
            } catch (const std::out_of_range &) {
                throw(GraphConfigError(mv.key() + ": cannot find its vertices among SimpleVertices"));
            }
            if (!e0->test_connection(sv0))
                throw(GraphConfigError(mv.key() + ": edge " + e0->get_id() + " does not have a required Simple_vertex"));
            if (!e1->test_connection(sv1))
                throw(GraphConfigError(mv.key() + ": edge " + e1->get_id() + " does not have a required Simple_vertex"));
            if (!e2->test_connection(sv1))
                throw(GraphConfigError(mv.key() + ": edge " + e2->get_id() + " does not have a required Simple_vertex"));

            w = mv.value()["ImpellerSpeed0"].template get<double>();

            pump_0d_heart =  new Heart_AV_pump(mv.key(), e0, e1, e2, sv0, sv1,
                                               density, viscosity,
                    mv.value()["L_av"].template get<double>(),
                    mv.value()["L_pu"].template get<double>(),
                    mv.value()["L_mi"].template get<double>(),
                    mv.value()["B_pu_const"].template get<double>(),
                    mv.value()["B_av_denomin"].template get<double>(),
                    mv.value()["B_mi_denomin"].template get<double>(),
                    mv.value()["pulmVeinsPressure"].template get<double>(),
                    mv.value()["LV_initialVolume"].template get<double>(),
                    mv.value()["LV_V0"].template get<double>(),
                    mv.value()["LA_V0"].template get<double>(),
                    mv.value()["LV_ESPVR"].template get<double>(),
                    mv.value()["LV_EDPVR"].template get<double>(),
                    mv.value()["LA_ESPVR"].template get<double>(),
                    mv.value()["LA_EDPVR"].template get<double>(),
                    mv.value()["valvePressureForceCoeff"].template get<double>(),
                    mv.value()["valveFrictionalForceCoeff"].template get<double>(),
                    mv.value()["LV_inertiaCoeff"].template get<double>(),
                    mv.value()["LV_dynamicResistanceCoeff"].template get<double>(),
                    mv.value()["LA_inertiaCoeff"].template get<double>(),
                    mv.value()["LA_dynamicResistanceCoeff"].template get<double>(),

                    Pump(mv.value()["a"].template get<double>(),
                         mv.value()["b"].template get<double>(),
                         mv.value()["c"].template get<double>(),
                         mv.value()["d"].template get<double>(),
                         mv.value()["e"].template get<double>(),
                         mv.value()["R_rec"].template get<double>(),
                         mv.value()["L_per"].template get<double>(),
                         mv.value()["R_per"].template get<double>(),
                         mv.value()["S_tube"].template get<double>()
                        )
                    );
            vertices.push_back(pump_0d_heart);
            heart = true_0d_heart = pump_0d_heart;
            set_pump_speed(w);

        } else if (type == "Heart_AdValves") {
            Edge * e;
            try {
                e = edge_map.at(mv.value()["edge"].template get<std::string>());
            } catch (const std::out_of_range &) {
                throw(GraphConfigError(mv.key() + ": cannot find its edge among Edges"));
            }
            Simple_vertex * sv;
            try {
                sv = map_pop(s_vert_map, mv.value()["vertex"].template get<std::string>());
                //sv = s_vert_map.at(mv.value()["vertex"].template get<std::string>());
            } catch (const std::out_of_range &) {
                throw(GraphConfigError(mv.key() + ": cannot find its vertex among SimpleVertices"));
            }
            if (!e->test_connection(sv))
                throw(GraphConfigError(mv.key() + ": edge " + e->get_id() + " does not have a required Simple_vertex"));

            true_0d_heart =  new Heart_AdValves(mv.key(), e, sv, density, viscosity,
                    mv.value()["L_av"].template get<double>(),
                    mv.value()["L_pu"].template get<double>(),
                    mv.value()["L_mi"].template get<double>(),
                    mv.value()["B_pu_const"].template get<double>(),
                    mv.value()["B_av_denomin"].template get<double>(),
                    mv.value()["B_mi_denomin"].template get<double>(),
                    mv.value()["pulmVeinsPressure"].template get<double>(),
                    mv.value()["LV_initialVolume"].template get<double>(),
                    mv.value()["LV_V0"].template get<double>(),
                    mv.value()["LA_V0"].template get<double>(),
                    mv.value()["LV_ESPVR"].template get<double>(),
                    mv.value()["LV_EDPVR"].template get<double>(),
                    mv.value()["LA_ESPVR"].template get<double>(),
                    mv.value()["LA_EDPVR"].template get<double>(),
                    mv.value()["valvePressureForceCoeff"].template get<double>(),
                    mv.value()["valveFrictionalForceCoeff"].template get<double>(),
                    mv.value()["LV_inertiaCoeff"].template get<double>(),
                    mv.value()["LV_dynamicResistanceCoeff"].template get<double>(),
                    mv.value()["LA_inertiaCoeff"].template get<double>(),
                    mv.value()["LA_dynamicResistanceCoeff"].template get<double>()
                    );
            vertices.push_back(true_0d_heart);
            heart = true_0d_heart;

        } else if (type == "Heart_2CV") {
            Edge * e;
            try {
                e = edge_map.at(mv.value()["edge"].template get<std::string>());
            } catch (const std::out_of_range &) {
                throw(GraphConfigError(mv.key() + ": cannot find its edge among Edges"));
            }
            Simple_vertex * sv;
            try {
                sv = map_pop(s_vert_map, mv.value()["vertex"].template get<std::string>());
                //sv = s_vert_map.at(mv.value()["vertex"].template get<std::string>());
            } catch (const std::out_of_range &) {
                throw(GraphConfigError(mv.key() + ": cannot find its vertex among SimpleVertices"));
            }
            if (!e->test_connection(sv))
                throw(GraphConfigError(mv.key() + ": edge " + e->get_id() + " does not have a required Simple_vertex"));

            true_0d_heart =  new Heart_2CV(mv.key(), e, sv,
                    mv.value()["atriumEnteringCoeff"].template get<double>(),
                    mv.value()["mitralValveCoeff"].template get<double>(),
                    mv.value()["aorticValveCoeff"].template get<double>(),
                    mv.value()["pulmVeinsPressure"].template get<double>(),
                    mv.value()["LV_initialVolume"].template get<double>(),
                    mv.value()["LV_V0"].template get<double>(),
                    mv.value()["LA_V0"].template get<double>(),
                    mv.value()["LV_ESPVR"].template get<double>(),
                    mv.value()["LV_EDPVR"].template get<double>(),
                    mv.value()["LA_ESPVR"].template get<double>(),
                    mv.value()["LA_EDPVR"].template get<double>(),
                    mv.value()["valvePressureForceCoeff"].template get<double>(),
                    mv.value()["valveFrictionalForceCoeff"].template get<double>(),
                    mv.value()["LV_inertiaCoeff"].template get<double>(),
                    mv.value()["LV_dynamicResistanceCoeff"].template get<double>(),
                    mv.value()["LA_inertiaCoeff"].template get<double>(),
                    mv.value()["LA_dynamicResistanceCoeff"].template get<double>()
                    );
            vertices.push_back(true_0d_heart);
            heart = true_0d_heart;
        } else if (type == "Internal") {
            Simple_vertex * sv;
            try {
                sv = map_pop(s_vert_map, mv.value()["vertex"].template get<std::string>());
                //sv = s_vert_map.at(mv.value()["vertex"].template get<std::string>());
            } catch (const std::out_of_range &) {
                throw(GraphConfigError(mv.key() + ": cannot find its vertex among SimpleVertices"));
            }
            std::vector<Edge*> this_vertex_edges;
            for (auto ej: mv.value()["edges"]) {
                Edge * e;
                try {
                    e = edge_map.at(ej.template get<std::string>());
                } catch (const std::out_of_range &) {
                    throw(GraphConfigError(mv.key() + ": cannot find its edges among Edges"));
                }
                if (!e->test_connection(sv))
                    throw(GraphConfigError(mv.key() + ": edge " + e->get_id() + " does not have a required Simple_vertex"));
                this_vertex_edges.push_back(e);
            }
            vertices.push_back(new Internal_vertex(mv.key(), this_vertex_edges, sv));
        } else if (type == "Windkessel_vertex") {
            Edge * e;
            try {
                e = edge_map.at(mv.value()["edge"].template get<std::string>());
            } catch (const std::out_of_range &) {
                throw(GraphConfigError(mv.key() + ": cannot find its edge among Edges"));
            }
            Simple_vertex * sv;
            try {
                sv = map_pop(s_vert_map, mv.value()["vertex"].template get<std::string>());
                //sv = s_vert_map.at(mv.value()["vertex"].template get<std::string>());
            } catch (const std::out_of_range &) {
                throw(GraphConfigError(mv.key() + ": cannot find its vertex among SimpleVertices"));
            }
            if (!e->test_connection(sv))
                throw(GraphConfigError(mv.key() + ": edge " + e->get_id() + " does not have a required Simple_vertex"));

            double Pout = mv.value()["P_out"].template get<double>();
            double R1 = terminalResistanceScaler * mv.value()["R1"].template get<double>();
            double R2 = terminalResistanceScaler * mv.value()["R2"].template get<double>();
            double C = mv.value()["C"].template get<double>();
            auto * wk = new Windkessel_vertex(mv.key(), e, sv, Pout, R1, R2, C);
            wk_vertices.push_back(wk);
            //terminal_vertices.push_back(wk);
            vertices.push_back(wk);
        } else if (type == "TerminalResistanceCoronary") {
            Edge * e;
            try {
                e = edge_map.at(mv.value()["edge"].template get<std::string>());
            } catch (const std::out_of_range &) {
                throw(GraphConfigError(mv.key() + ": cannot find its edge among Edges"));
            }
            Simple_vertex * sv;
            try {
                sv = map_pop(s_vert_map, mv.value()["vertex"].template get<std::string>());
                //sv = s_vert_map.at(mv.value()["vertex"].template get<std::string>());
            } catch (const std::out_of_range &) {
                throw(GraphConfigError(mv.key() + ": cannot find its vertex among SimpleVertices"));
            }
            if (!e->test_connection(sv))
                throw(GraphConfigError(mv.key() + ": edge " + e->get_id() + " does not have a required Simple_vertex"));

            double Pout = mv.value()["P_out"].template get<double>();
            double R = terminalResistanceScaler * mv.value()["R"].template get<double>();
            auto * terminalCoronary = new TerminalResistanceCoronaryArteries(mv.key(), e, sv, Pout, R);
            terminal_vertices.push_back(terminalCoronary);
            vertices.push_back(terminalCoronary);
        } else if (type == "Heart_2CV_Pump") {
            Edge * e0, * e1;
            try {
                e0 = edge_map.at(mv.value()["edge0"].template get<std::string>());
                e1 = edge_map.at(mv.value()["edge1"].template get<std::string>());
            } catch (const std::out_of_range &) {
                throw(GraphConfigError(mv.key() + ": cannot find its edges among Edges"));
            }
            Simple_vertex * sv0, *sv1;
            try {
                sv0 = map_pop(s_vert_map, mv.value()["vertex0"].template get<std::string>());
                sv1 = map_pop(s_vert_map, mv.value()["vertex1"].template get<std::string>());
                //sv0 = s_vert_map.at(mv.value()["vertex0"].template get<std::string>());
                //sv1 = s_vert_map.at(mv.value()["vertex1"].template get<std::string>());
            } catch (const std::out_of_range &) {
                throw(GraphConfigError(mv.key() + ": cannot find its vertices among SimpleVertices"));
            }
            if (!e0->test_connection(sv0))
                throw(GraphConfigError(mv.key() + ": edge " + e0->get_id() + " does not have a required Simple_vertex"));
            if (!e1->test_connection(sv1))
                throw(GraphConfigError(mv.key() + ": edge " + e1->get_id() + " does not have a required Simple_vertex"));

            double pulm_veins_pressure = mv.value()["pulmVeinsPressure"].template get<double>();
            double initLVvolume = mv.value()["LV_initialVolume"].template get<double>();
            double LV_V0 = mv.value()["LV_V0"].template get<double>();
            double LV_EDPVR = mv.value()["LV_EDPVR"].template get<double>();
            double LV_ESPVR = mv.value()["LV_ESPVR"].template get<double>();
            w = mv.value()["ImpellerSpeed0"].template get<double>();

            pump_0d_heart = new H2CV_Pump(mv.key(), e0, e1, sv0, sv1,
                                          pulm_veins_pressure,
                                          initLVvolume,
                                          LV_V0,
                                          LV_EDPVR,
                                          LV_ESPVR);
            heart = true_0d_heart = pump_0d_heart;
            vertices.push_back(pump_0d_heart);
            set_pump_speed(w);
        } else if (type == "SimpleFlow") {
            Edge * e;
            try {
                e = edge_map.at(mv.value()["edge"].template get<std::string>());
            } catch (const std::out_of_range &) {
                throw(GraphConfigError(mv.key() + ": cannot find its edge among Edges"));
            }
            Simple_vertex * sv;
            try {
                sv = map_pop(s_vert_map, mv.value()["vertex"].template get<std::string>());
                //sv = s_vert_map.at(mv.value()["vertex"].template get<std::string>());
            } catch (const std::out_of_range &) {
                throw(GraphConfigError(mv.key() + ": cannot find its vertex among SimpleVertices"));
            }
            if (!e->test_connection(sv))
                throw(GraphConfigError(mv.key() + ": edge " + e->get_id() + " does not have a required Simple_vertex"));

            heart =  new Simple_heart(mv.key(), e, sv,
                                      /*Period*/ 1,
                                      /*Stroke volume*/ mv.value()["Stroke_volume"].template get<double>());
            vertices.push_back(heart);
        } else if (type == "ConstantFlow") {
            Edge * e;
            try {
                e = edge_map.at(mv.value()["edge"].template get<std::string>());
            } catch (const std::out_of_range &) {
                throw(GraphConfigError(mv.key() + ": cannot find its edge among Edges"));
            }
            Simple_vertex * sv;
            try {
                sv = map_pop(s_vert_map, mv.value()["vertex"].template get<std::string>());
                //sv = s_vert_map.at(mv.value()["vertex"].template get<std::string>());
            } catch (const std::out_of_range &) {
                throw(GraphConfigError(mv.key() + ": cannot find its vertex among SimpleVertices"));
            }
            if (!e->test_connection(sv))
                throw(GraphConfigError(mv.key() + ": edge " + e->get_id() + " does not have a required Simple_vertex"));

            heart =  new Constant_flow_heart(mv.key(), e, sv,
                                             /*Flowrate*/ mv.value()["Flowrate"].template get<double>());
            vertices.push_back(heart);
        } else {
            throw(GraphConfigError(mv.key() + ": unsupported type of MetaVertex"));
        }
    }

    void find_max_abs_eigenvalue_div_dx();
    //void find_dt();
    /**
     * @brief run_step runs model for sim_timer seconds
     * @param sim_timer step time (in seconds)
     */
    void run_step(double sim_timer);

    /**
     * @brief set_up
     * @param main_config_path
     * @param heart_additional_config_path
     */
    void set_up(const std::string & main_config_path, const std::string & heart_additional_config_path = std::string());
    void run_full();
    bool is_valid();
    bool has_pump();
    ~Task();

    int get_edge_data_size(const std::string & ename, double & len);
  //  template<typename C>
  //  void get_edge_data(const std::string & ename, value_type vt, C & array, int sz);//array of doubles

    template<typename C, typename Point>
    C get_edge_data(const std::string ename, quantity vt, C array, C end, double dx_add)
    {
        //double t1 = omp_get_wtime();
        Edge *edge = edge_map[ename];
        const double dx = edge->get_dx();
        const auto sz = edge->get_points_num();
        assert(sz > 0);
        switch (vt) {
            case area: {
                for (int j = 0; j < sz && array != end; j++, ++array) {
                    *array = Point(j*dx + dx_add, edge->get_S_unsafe(j));
                }
                break;
            }

            case speed: {
                for (int j = 0; j < sz && array != end; j++, ++array) {
                    *array = Point(j*dx + dx_add, edge->get_U_unsafe(j));
                }
                break;
            }

            case flow: {
                for (int j = 0; j < sz && array != end; j++, ++array) {
                    *array = Point(j*dx + dx_add, edge->get_Q_unsafe(j));
                }
                break;
            }

            case pressure: {// in mm Hg
                for (int j = 0; j < sz && array != end; j++, ++array) {
                    *array = Point(j*dx + dx_add, edge->get_P_unsafe(j) / 1333.22);
                }
                break;
            }

        }
        //eval_time += omp_get_wtime() - t1;
        return array;
    }
    void set_pump_speed(const double &w);
    double get_simTime() const;
    double get_computationTime() const;
    void get_hq(double &q, double &h) const;
    void get_LV_pv(double &p, double &v) const;
    void get_LA_pv(double &p, double &v) const;

    //void set_pump_LV_pressure_mmHg(const double &p);
    double get_strokeVolume();
    double get_aortic_valve();
    double get_mitral_valve();
    //double tprev;
    void generate_report_speed();
    void generate_report_test_LV_ESPVR();
    void generate_report_test_pulm_vein_pressure();
    void generate_report_test_afterload();
    void test_afterload(const std::string & output);
    void generate_report_basic();
    void generate_report(const std::string & outputPrefix, bool & is_pump_backflow,
                         bool & is_av_always_closed, bool & is_mv_always_opened,
                         double & sv_av, double & sv_pump, double & aortic_valve_opened_time,
                         double & mitral_valve_opened_time, double & pressure_pulsality,
                         double & max_pressure, double & min_pressure,
                         double & min_pump_flow, double & lv_work);

    void generate_report_edge_stats();
    void sensitivityAnalysis();
    void generate_report_wk_flows();
};
#endif
