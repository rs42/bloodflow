﻿#ifndef HEART_TWOCOMPVALVES_H
#define HEART_TWOCOMPVALVES_H

#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include <Eigen/Dense>
#include <cmath>

#include "vertex.h"

template<typename Edge,
         template<typename, int, int ...> typename Matrix, typename Scalar, auto Dynamic>
class Heart_2CV:
    public True_0d_heart<Edge, Matrix, Scalar, Dynamic>
{
    typedef True_0d_heart<Edge, Matrix, Scalar, Dynamic> Base;
    using Base::OutgoingCompatibilityCoeffs;
    using Base::T;
    using Base::dt;
    using Base::id;


    typedef Eigen::Matrix<double, 9, 9> Matrix9d;
    typedef Eigen::Matrix<double, 8, 8> Matrix8d;
    typedef Eigen::Matrix<double, 8, 9> Matrix89;

    typedef Eigen::Matrix<double, 9, 1> Vector9d;
    typedef Eigen::Matrix<double, 8, 1> Vector8d;
    typedef Eigen::Matrix<double, 1, 9> RowVector9d;

    enum {v1, v1_d, v4, v4_d, tet51, tet51_d, tet14, tet14_d, s};

    //edge starting from this node
    Edge * e;

    //simple vertex of this node
    Simple_vertex * sv;

    const double cc = 1333.22;

    //compatibility coeffs
    double alfa, beta;

    const double r2d = M_PI/180;

    //flow coefficient of atrium entering, ml/(s*mmHg^0.5)
    double CQ48;

    //flow coefficient of mitral valve, ml/(s*mmHg^0.5)
    double CQ14;

    //flow coefficient of aortic valve, ml/(s*mmHg^0.5)
    double CQ51;

    //pressure in pulmonary veins, mmHg
    double P8;

    //peak of systolic phase for ventricle
    const double Ts1 = 0.35;

    //end of systolic phase for ventricle
    const double Ts2 = 0.4;

    const double Tpb = 0.9;

    const double Tpw = 0.1;

    //dead volume of lv
    const double V1_0;

    //dead volume of la
    const double V4_0;// = 20;

    //elastance of ventricle in systolic phase, mmHg/ml
    //basically it is ESPVR
    double E1s;

    //elastance of ventricle in diastolic phase, mmHg/ml
    //this is EDPVR
    const double E1d;

    //elastance of atrium in systolic phase, mmHg/ml
    const double E4s;// = 1.5;

    //elastance of atrium in diastolic phase, mmHg/ml
    const double E4d;// = 0.2;

    //coefficient of pressure force effect in valve
    const double Kp;// = 10000;//was 5500

    //coefficient of frictional force effect in valve
    const double Kf;// = 50;

    //max and min opening angles of both valves, degrees
    const double thetamax1_n = 75;
    const double thetamin1_n = 0;

    const double thetamax = thetamax1_n*r2d;
    const double thetamin = thetamin1_n*r2d;
    const int thetaPower = 2;
    const double AR0 = std::pow(1-cos(thetamax), thetaPower);

    //duration of heart period, s
    const double Period = 1;

    //coefficient of inertia effect in ventricle
    const double I1;// = 1.e-5;

    //coefficient of resistance of ventricle
    const double R1;// = 0.0015;//4.e-5;

    //coefficient of inertia effect in atrium
    const double I4;// = 1.e-5;

    //cofficient of resistance of atrium
    const double R4;// = 0.0015;// 4.e-5;

    //time normalization
    inline double tnorm (const double & t) {
        return fmod(t, Period);
    }


    //ventricular activation function
    double e10 (const double & tcurr) {

        const double t = tnorm(tcurr);
        assert(t >= 0);

        if (t < Ts1) {

            return 1.0 - cos(t*M_PI/Ts1);

        } else if (t < Ts2) {

            return  1.0 - cos((t-Ts2)*M_PI/(Ts1-Ts2));

        } else {

            return 0;

        }
/*


        //double hill
        double T_max = 0.2 + 0.15;
        double tn = t/T_max;
        return 2*1.55*std::pow(tn/0.7, 1.9)
               / ((1 + std::pow(tn/0.7, 1.9) ) * (1 + std::pow(tn/1.17, 21.9)));
*/
/*
        double T1 = 0.05, T2 = 0.3, T3 = 0.4;
        double res;
        if (t < T1)
            res =  (std::exp(t/T1) - 1)/(M_E - 1)*0.4;
        else if ( t < T2 )
            res =   (1 + (T2-t)/(T2-T1)*std::log(t/T1)) *0.4 + (t-T1)/(T2-T1)*t*3;
        else if ( t < T3)
            res = (1.0 - cos((t - T3)*M_PI/(T2-T3)))/2*1.2962;
        else
            res = 0;
        res = res/1.2962*2;
        return res;
    */
    }

    //its time derivative
    double e10_t (const double & tcurr) {

        const double t = tnorm(tcurr);
        assert(t >= 0);

        if (t < Ts1) {

            return sin(t*M_PI/Ts1)*M_PI/Ts1;

        } else if (t <= Ts2) {

            return sin((t-Ts2)*M_PI/(Ts1-Ts2))*M_PI/(Ts1-Ts2);

        } else {

            return 0;

        }


/*
        //double hill
        double T_max = 0.2 + 0.15;
        double tn = t/T_max;
        double g = ((1 + std::pow(tn/0.7, 1.9) ) * (1 + std::pow(tn/1.17, 21.9)));
        double f =  std::pow(tn/0.7, 1.9);
        double fp = 1.9/std::pow(0.7,1.9)*std::pow(tn,0.9);
        double gp = (1 + std::pow(tn/1.17, 21.9))*fp + (1 + std::pow(tn/0.7, 1.9) ) * 21.9/std::pow(1.17,  21.9) * std::pow(tn, 20.9);
        return 2*1.55/T_max/(g*g)*(g*fp - f*gp);
*/

/*
        double T1 = 0.05, T2 = 0.3, T3 = 0.4;
        double res;
        if (t < T1)
            res =  (std::exp(t/T1)/T1)/(M_E - 1)*0.4;
        else if ( t < T2 )
            res =   ( -std::log(t/T1) + (T2-t)/t ) *0.4/(T2-T1) + (t-T1)/(T2-T1)*3 + t*3/(T2-T1);
        else if ( t < T3)
            res = (sin((t - T3)*M_PI/(T2-T3)))  /2*1.2962 * M_PI/(T2-T3);
        else
            res = 0;
        res = res/1.2962*2;
        return res;
*/
    }

    //atrium activation function

    double e40 (const double & tcurr) {

        const double t = tnorm(tcurr);
        assert(t >= 0);

        if (t < Tpb || t > Tpb + Tpw) {

            return 0;

        } else {

            return 1 - cos((t-Tpb)*2*M_PI/Tpw);

        }
    }

    double e40_t (const double & tcurr) {

        const double t = tnorm(tcurr);
        assert(t >= 0);

        if (t < Tpb || t > Tpb + Tpw) {

            return 0;

        } else {

            return sin((t-Tpb)*2*M_PI/Tpw)*2*M_PI/Tpw;

        }
    }

/*
    double e40 (const double & tcurr) {

        const double t = tnorm(tcurr);
        assert(t >= 0);
        const double t1 = 0.07;
        const double t2 = 0.03;
        if (t < Tpb) {

            return 0;

        } else if (t < Tpb + t1) {

            return 1.0 - cos((t - Tpb) * M_PI / t1);

        } else {

            return 1.0 - cos((Tpb + t1 + t2 - t) * M_PI / t2);

        }
    }

    double e40_t (const double & tcurr) {

        const double t = tnorm(tcurr);
        assert(t >= 0);
        const double t1 = 0.07;
        const double t2 = 0.03;
        if (t < Tpb) {

            return 0;

        } else if (t < Tpb + t1) {

            return sin((t - Tpb) * M_PI / t1)* M_PI / t1;

        } else {

            return  sin((t - Tpb - t1) * M_PI / (t2 - t1))* M_PI / (t2 - t1);

        }
    }
*/
    //time-varying ventricular elastance
    double e1 (const double & t) {

        return E1d + (E1s - E1d) * e10(t) / 2.0;

    }

    double e1_t (const double & t) {

        return  (E1s - E1d) * e10_t(t) / 2.0;

    }

    //time-varying atrium elastance
    double e4 (const double & t) {

        return E4d + (E4s - E4d) * e40(t) / 2.0;

    }

    double e4_t (const double & t) {

        return (E4s-E4d)*e40_t(t)/2.0;

    }


    //valve opening function
    const double veps = 1e-6; //doesnt work with 0

    double AR (const double & y) {

        if (y <= thetamin) {

            return veps;

        } else if (y >= thetamax) {

            return 1;

        } else {

            return (1-veps)*std::pow(1-cos(y), thetaPower)/AR0 + veps;

        }
    }

    double AR_y (const double & y) {

        if (y < thetamax && y > thetamin) {

            return (1-veps)*thetaPower*std::pow(1-cos(y), thetaPower - 1)*sin(y)/AR0;

        } else {

            return 0;

        }
    }


    //coefficient of resistance force in the valve
    const double ec = 1000;
    const double thetaeps = 0;

    double Kr (const double & y) {
        if (y >= thetamax) {
            return expm1(ec*(y-thetamax));
        } else if (y <= thetamin+thetaeps) {
            return -expm1(ec*(thetamin+thetaeps-y));
        } else {
            return 0;
        }
    }

    double Kr_y (const double & y) {
        if (y >= thetamax) {
            return ec*exp(ec*(y-thetamax));
        } else if (y <= thetamin+thetaeps) {
            return ec*exp(ec*(thetamin+thetaeps-y));
        } else {
            return 0;
        }
    }

    double Kr_yy (const double & y) {
        if (y >= thetamax) {
            return ec*ec*exp(ec*(y-thetamax));
        } else if (y <= thetamin+thetaeps) {
            return -ec*ec*exp(ec*(thetamin+thetaeps-y));
        } else {
            return 0;
        }
    }

    /*
    double Kr (const double & y) {

        if (y >= thetamax) {

            return Kr0+1000000*atan(10*(y-thetamax));

        } else if (y <= thetamin) {

            return Kr0-1000000*atan(10*(y-thetamin));

        } else {

            return Kr0;

        }
    }

    double Kr_y (const double & y) {

        if (y >= thetamax) {

            return 10000000/(100*(y-thetamax)*(y-thetamax)+1);

        } else if (y <= thetamin) {

            return -10000000/(100*(y-thetamin)*(y-thetamin)+1);

        } else {

            return 0;

        }
    }

    double Kr_yy (const double & y) {

        if (y >= thetamax) {

            return -(2000000000*(y-thetamax))/
            ((100*(y-thetamax)*(y-thetamax)+1)*(100*(y-thetamax)*(y-thetamax)+1));

        } else if (y <= thetamin) {

            return (2000000*(y-thetamin))/
            ((100*(y-thetamin)*(y-thetamin)+1)*(100*(y-thetamin)*(y-thetamin)+1));

        } else {
            return 0;
        }
    }
    */


    //pressure in aorta
    double P5(const double & Sq) {
        return e->get_pressure(Sq, sv, 0)/cc;
    }

    double P5_d(const double & Sq) {
        return e->get_d_pressure_d_s(Sq, sv, 0)/cc;
    }

    //pressure in left ventricle
    double P1 (const double & Sq, const double & teta) {
        return P5(Sq) + Sq*(alfa*Sq + beta) / (CQ51*AR(teta));
    }

    double P1_s (const double & Sq, const double & teta) {
        return P5_d(Sq) + (2*alfa*Sq + beta) / (CQ51*AR(teta));
    }

    double P1_tet (const double & Sq, const double & teta) {
        return  - AR_y(teta)* Sq * (alfa*Sq + beta) / (CQ51 * pow(AR(teta), 2));
    }

    //pressure in left atrium
    double P4 (const double & V1_D, const double & V4_D, const double & Sq) {
        return P8 - (V1_D + V4_D + Sq*(alfa*Sq + beta))/CQ48;
    }

    double P4_v1_d (const double & V1_D, const double & V4_D, const double & Sq) {
        return - 1/CQ48;
    }

    double P4_v4_d (const double & V1_D, const double & V4_D, const double & Sq) {
        return - 1/CQ48;
    }

    double P4_s (const double & V1_D, const double & V4_D, const double & Sq) {
        return - (2*alfa*Sq + beta)/CQ48;
    }

    //frictional force
    double Ff (const double & y3) {
        return Kf*y3;
    }

    double Ff_tet_d (const double & y3) {
        return Kf;
    }

    double Ff_y3y3 (const double & y3) {
        return 0;
    }

    //resistance force
    double Fr (const double & y2) {
        return Kr(y2);
    }

    double Fr_tet (const double & y2) {
        return Kr_y(y2);
    }

    double Fr_y2y2 (const double & y2) {
        return Kr_yy(y2);
    }

    //pressure force
    double Fp1 (const double & Sq, const double & tet) {
        return Kp*(P1(Sq, tet) - P5(Sq))
                 *cos(tet);
    }

    double Fp1_s (const double & Sq, const double & tet) {
        return Kp*(P1_s(Sq, tet) - P5_d(Sq))
                 *cos(tet);
    }
    double Fp1_tet (const double & Sq, const double & tet) {
        return Kp*(P1(Sq, tet) - P5(Sq))
                 *(-sin(tet)) +
                Kp*P1_tet(Sq, tet)*cos(tet);
    }

    double Fp2 (const double & V1_D, const double & V4_D, const double & Sq, const double & TET51, const double & TET14) {

        return Kp*(P4(V1_D, V4_D, Sq) - P1(Sq, TET51))*cos(TET14);

    }

    double Fp2_v1_d (const double & V1_D, const double & V4_D, const double & Sq, const double & TET51, const double & TET14) {

        return Kp*P4_v1_d(V1_D, V4_D, Sq)*cos(TET14);

    }

    double Fp2_v4_d(const double & V1_D, const double & V4_D, const double & Sq, const double & TET51, const double & TET14) {

        return Kp*P4_v4_d(V1_D, V4_D, Sq)*cos(TET14);

    }

    double Fp2_s(const double & V1_D, const double & V4_D, const double & Sq, const double & TET51, const double & TET14) {

        return Kp*(P4_s(V1_D, V4_D, Sq) - P1_s(Sq, TET51))*cos(TET14);

    }

    double Fp2_tet51(const double & V1_D, const double & V4_D, double Sq, double TET51, double TET14) {

        return Kp*(- P1_tet(Sq, TET51))*cos(TET14);

    }

    double Fp2_tet14(const double & V1_D, const double & V4_D,
                     const double & Sq, const double & TET51, const double & TET14) {

        return Kp*(P4(V1_D, V4_D, Sq) - P1(Sq, TET51))*(-sin(TET14));

    }



    //right part of system
    Vector8d F (const Vector9d & y, const double & t) {
        Vector8d func;

        func(0) = y(v1_d);

        const double P1c = P1(y(s), y(tet51));
        func(1) = -R1 * P1c * y(v1_d) / I1 - e1(t) * (y(v1) - V1_0) / I1 + P1c / I1;

        //without dynamic resistance
        //func(1) = -R1*y(v1_d)/I1 - e1(t)*(y(v1) - V1_0)/I1 + P1(y(s), y(tet51))/I1;

        func(2) = y(v4_d);

        //without dynamic resistance
        //func(3) = -R4*y(v4_d)/I4 - e4(t)*(y(v4) - V4_0)/I4 + P4(y(v1_d), y(v4_d), y(s))/I4;

        const double P4c = P4(y(v1_d), y(v4_d), y(s));
        func(3) = -R4 * P4c * y(v4_d) / I4 - e4(t) * (y(v4) - V4_0) / I4
                  + P4c / I4;

        func(4) = y(tet51_d);

        func(5) = -Fr(y(tet51)) - Ff(y(tet51_d)) + Fp1(y(s), y(tet51));

        func(6) = y(tet14_d);

        func(7) = -Fr(y(tet14)) - Ff(y(tet14_d)) + Fp2(y(v1_d), y(v4_d), y(s), y(tet51), y(tet14));

        return func;
    }

    Matrix89 F_d(const Vector9d & y, const double & t) {
        Matrix89 res;
        res.setZero();

        res(0, v1_d) = 1;

        res(1, v1_d) = -R1*P1(y(s), y(tet51))/I1;
        res(1, v1) = - e1(t)/I1;

        res(1, tet51) = (-R1 * y(v1_d) + 1) * P1_tet(y(s), y(tet51))/I1;
        res(1, s) = (-R1 * y(v1_d) + 1) * P1_s(y(s), y(tet51))/I1;

        res(2, v4_d) = 1;

        const double P4_v4_d_c = P4_v4_d(y(v1_d), y(v4_d), y(s));
        res(3, v4_d) = -R4*P4(y(v1_d), y(v4_d), y(s))/I4 - R4*P4_v4_d_c*y(v4_d)/I4 + P4_v4_d_c/I4;
        res(3, v4) = - e4(t)/I4;
        const double P4_v1_d_c = P4_v1_d(y(v1_d), y(v4_d), y(s));
        res(3, v1_d) = -R4 * P4_v1_d_c * y(v4_d) / I4 + P4_v1_d_c / I4;
        const double P4_s_c = P4_s(y(v1_d), y(v4_d), y(s));
        res(3, s) = -R4 * P4_s_c * y(v4_d) / I4 + P4_s_c / I4;

        res(4, tet51_d) = 1;

        res(5, tet51) = -Fr_tet(y(tet51)) + Fp1_tet(y(s), y(tet51));
        res(5, tet51_d) = - Ff_tet_d(y(tet51_d));
        res(5, s) = Fp1_s(y(s), y(tet51));

        res(6, tet14_d) = 1;

        res(7, tet14) = -Fr_tet(y(tet14)) + Fp2_tet14(y(v1_d), y(v4_d), y(s), y(tet51), y(tet14));
        res(7, tet14_d) = - Ff_tet_d(y(tet14_d));
        res(7, tet51) = Fp2_tet51(y(v1_d), y(v4_d), y(s), y(tet51), y(tet14));
        res(7, s) = Fp2_s(y(v1_d), y(v4_d), y(s), y(tet51), y(tet14));
        res(7, v1_d) = Fp2_v1_d(y(v1_d), y(v4_d), y(s), y(tet51), y(tet14));
        res(7, v4_d) = Fp2_v4_d(y(v1_d), y(v4_d), y(s), y(tet51), y(tet14));

        return res;
    }


    double f(const Vector9d & y)
    {
        const double coef14 = CQ14*AR(y(tet14));
        const double coef51 = CQ51*AR(y(tet51));

        const double A = (coef14 + coef51)*CQ48 + coef14*coef51;
        const double G = coef14*coef51*CQ48;
        const double L = coef14*coef51;
        const double M = L + CQ48*coef51;

        return A*y(s)*(alfa*y(s) + beta) + G*(P5(y(s)) - P8)
               + L*y(v4_d) + M*y(v1_d);

    }
    RowVector9d f_d(const Vector9d & y)
    {
        RowVector9d res;

        const double coef14 = CQ14*AR(y(tet14));
        const double coef14_d14 = CQ14*AR_y(y(tet14));
        const double coef51 = CQ51*AR(y(tet51));
        const double coef51_d51 = CQ51*AR_y(y(tet51));

        const double A = (coef14 + coef51)*CQ48 + coef14*coef51;
        const double A_d14 = CQ48*coef14_d14 + coef51*coef14_d14;
        const double A_d51 = CQ48*coef51_d51 + coef14*coef51_d51;

        const double L = coef14*coef51;
        const double L_d14 = coef51*coef14_d14;
        const double L_d51 = coef14*coef51_d51;

        const double G = L*CQ48;
        const double G_d14 = CQ48*L_d14;
        const double G_d51 = CQ48*L_d51;

        const double M = L + CQ48*coef51;
        const double M_d14 = L_d14;
        const double M_d51 = L_d51 + CQ48*coef51_d51;

        res(v1) = 0;
        res(v1_d) = M;
        res(v4) = 0;
        res(v4_d) = L;
        res(tet51) = A_d51*y(s)*(alfa*y(s) + beta) + G_d51*(P5(y(s)) - P8)
                + L_d51*y(v4_d) + M_d51*y(v1_d);
        res(tet51_d) = 0;
        res(tet14) = A_d14*y(s)*(alfa*y(s) + beta) + G_d14*(P5(y(s)) - P8)
                + L_d14*y(v4_d) + M_d14*y(v1_d);
        res(tet14_d) = 0;
        res(s) = A*(2*alfa*y(s) + beta) + G*P5_d(y(s));

        return res;
    }

    //flows
    double Q51 (const Vector9d & y) {
        return y(s)*(alfa*y(s) + beta);
    }

    double Q14 (const Vector9d & y) {
        return y(v1_d) + Q51(y);
    }


    Vector9d y0, yn, R, y2prev;
    Matrix9d B;

    double LV_P, LV_V;
    double LA_P, LA_V;
    double aortic_valve, mitral_valve;
public:
    int ff;
    double get_aortic_root_pressure()
    {
        return e->get_pressure(y0(s), sv, 0)/cc;
    }
    double get_LV_P()
    {
        return LV_P;
    }
    double get_LV_V()
    {
        return LV_V;
    }
    double get_LA_P()
    {
        return LA_P;
    }
    double get_LA_V()
    {
        return LA_V;
    }
    double get_aortic_valve()
    {
        return aortic_valve;
    }
    double get_mitral_valve()
    {
        return mitral_valve;
    }
    double get_flow_av()
    {
        return y0(s)* (alfa*y0(s) + beta);
    }
    double get_ESPVR()
    {
        return E1s;
    }
    void set_ESPVR(double new_ESPVR)
    {
        E1s = new_ESPVR;
    }
    double get_PveinPressure()
    {
        return P8;
    }
    void set_PveinPressure(double new_PveinPressure)
    {
        P8 = new_PveinPressure;
    }
    Heart_2CV(const std::string & id, Edge * e, Simple_vertex * sv,
              double atriumEnteringCoeff,
              double mitralValveCoeff,
              double aorticValveCoeff,
              double pulmVeinsPressure,
              double initLVvolume,
              double LV_V0, double LA_V0,
              double LV_ESPVR, double LV_EDPVR,
              double LA_ESPVR, double LA_EDPVR,
              double valvePressureForceCoeff,
              double valveFrictionalForceCoeff,
              double LV_inertiaCoeff,
              double LV_dynamicResistanceCoeff,
              double LA_inertiaCoeff,
              double LA_dynamicResistanceCoeff
              )
    : Base(id), e(e), sv(sv), CQ48(atriumEnteringCoeff), CQ14(mitralValveCoeff), CQ51(aorticValveCoeff),
      P8(pulmVeinsPressure), V1_0(LV_V0), V4_0(LA_V0),
      E1s(LV_ESPVR),
      E1d(LV_EDPVR),
      E4s(LA_ESPVR),
      E4d(LA_EDPVR),
      Kp(valvePressureForceCoeff),
      Kf(valveFrictionalForceCoeff),
      I1(LV_inertiaCoeff),
      R1(LV_dynamicResistanceCoeff),
      I4(LA_inertiaCoeff),
      R4(LA_dynamicResistanceCoeff)
    {
        //CQ51 = CQ48/3*3.5; //according to the relative sizes of valves
        //CQ14 = CQ48/3*5;

        //see config try initLVvolume = 130 for healthy heart
        ff = 0;
        y0 << initLVvolume, 0, 80, 0, 0.01, 0, 0.01, 0, 7.9;
        y2prev = y0;
        B.setZero();
        R.setZero();
        yn.setZero();
    }
protected:
    void update()
    {
        OutgoingCompatibilityCoeffs(e, sv, alfa, beta);

        double ts = T-dt;
        double tau = dt;
        while (tau > 1e-15) {

            double t = ts + tau;
            yn = y0 + (y0 - y2prev);

            int j = 0;
            double R0_norm = 0;

            int MaxIt = 20;
            while (1) {
                //Newton
                if (j > MaxIt) {
                    j = 0;
                    //yn = y0;
                    tau/=2;
                    t = ts + tau;

                    //std::cout << "!" << std::endl;
                    if (tau < 1e-6) {
                        std::cout << "LOL" << std::endl;
                        std::cout << y0 << std::endl;
                        std::cout << yn << std::endl;
                        std::cout << "Res error: " << R.lpNorm<Eigen::Infinity>() << std::endl;

                        //break;
                        throw("Newton from Heart with valves failed");
                     }
                }

                R.head<8>() = yn.head<8>() - y0.head<8>() - tau * F(yn, t);
                R(8) = f(yn);

                if (R0_norm == 0) {
                    R0_norm = R.lpNorm<Eigen::Infinity>();
                }

                if (R.lpNorm<Eigen::Infinity>() < 1e-5 || R.lpNorm<Eigen::Infinity>() < 1e-12*R0_norm) {
                   // std::cout << R.lpNorm<Eigen::Infinity>() << std::endl;

                    break;
                }
                B.setZero();
                B.topLeftCorner<8, 8>().setIdentity();
                B.topLeftCorner<8, 9>() -= tau * F_d(yn, t);
                B.row(8) = f_d(yn);

                yn -= B.colPivHouseholderQr().solve(R);
                j++;
            }
            y2prev = y0;
            y0 = yn;
    /*
            const auto f_yynt = f_y(yn,t);
            const auto eigenvalues = f_yynt.eigenvalues();
            for (int i = 0; i < 8; i++) {
                if (eigenvalues(i).real() > 1)
                    cout << t << " " << eigenvalues(i) << endl;
            }
    */
            ts += tau;
            tau = T - ts;
        }
/*
        if (T >= 6 && T <= 7) {
            ff++;
            if (ff % 6 == 0)
                std::cout << y0(v1) << " " << P1(y0(s), y0(tet51)) << std::endl;
        }
        */
        if (y0(v1) < 0 || y0(v4) < 0) {
            std::cout << "time: " << T << std::endl;
            std::cout << "LV vol/pressure: " << y0(v1) << " " << P1(y0(s), y0(tet51)) << std::endl;
            std::cout << "LA vol/pressure: " << y0(v4) << " " << P4(y0(v1_d), y0(v4_d), y0(s)) << std::endl;

            throw("OMG");
        }
        //save results
        LV_P = P1(y0(s), y0(tet51));
        LV_V = y0(v1);
        LA_P = P4(y0(v1_d), y0(v4_d), y0(s));
        LA_V = y0(v4);
        aortic_valve = y0(tet51)/r2d;
        mitral_valve = y0(tet14)/r2d;
        const double S = y0(s);
        e->set_V_s(sv, 0, S);
        e->set_V_u(sv, 0, alfa*S + beta);
    }
};



#endif // HEART_TWOCOMPVALVES_H
