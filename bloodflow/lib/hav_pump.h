#ifndef HAV_PUMP_H
#define HAV_PUMP_H

#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include <Eigen/Dense>
#include <cmath>

#include "vertex.h"
#include "heart_advanced_valves.h"
#include "pump_part.h"


template<typename Edge,
         template<typename, int, int ...> typename Matrix,
         typename Scalar, auto Dynamic>
class Heart_AV_pump:
    public Pump_0d_heart<Edge, Matrix, Scalar, Dynamic>,
    public Heart_part,
    public Pump
{
    typedef Pump_0d_heart<Edge, Matrix, Scalar, Dynamic> Base;
    using Base::OutgoingCompatibilityCoeffs;
    using Base::T;
    using Base::dt;
    using Base::id;

    using Base::get_pressure;
    using Base::get_eos_deriv;
    using Base::setSU;


    typedef Eigen::Matrix<double, 19, 19> Matrix19d;
    typedef Eigen::Matrix<double, 12, 19> Matrix12x19;

    typedef Eigen::Matrix<double, 7, 1> Vector7d;
    typedef Eigen::Matrix<double, 19, 1> Vector19d;
    typedef Eigen::Matrix<double, 12, 1> Vector12d;
    typedef Eigen::Matrix<double, 7, 19> Matrix7x19;

    enum {v_v, v_v_d, v_a, v_a_d, ao_valve, ao_valve_d, mi_valve, mi_valve_d,
          q_av, q_mi, q_pu, q_PUMP, a_aorta, p_vent, p_aort, p_atri,
          p_PUMP_END, S1, S2};

    Edge * edge1v; //edge with a valve
    Edge * edge2p; //two aortic edges adjacent to pump tube
    Edge * edge3p; //two aortic edges adjacent to pump tube

    Simple_vertex * heart_v;
    Simple_vertex * pump_connect_v;


    //compatibility coeffs
    double alfa0, beta0; //flow from valve to pump
    double alfa1, beta1; //flow from pump to valve
    double alfa2, beta2; //flow from pump to the end of the ascending aorta


public:
        double get_H()
        {
            return y0(p_PUMP_END) - y0(p_vent);
        }
        double get_Q()
        {
            return y0(q_PUMP);
        }
private:
    double density;

    double viscosity;

    //right side of ODE system
    Vector12d F (const Vector19d & y, const double & t) {
        Vector12d d;

        d(v_v) = y(v_v_d);

        d(v_v_d) = (-R1 * y(p_vent) * y(v_v_d) - e1(t) * (y(v_v) - V1_0) + y(p_vent) )/ I1;

        d(v_a) = y(v_a_d);

        d(v_a_d) = (-R4 * y(p_atri) * y(v_a_d) - e4(t) * (y(v_a) - V4_0) + y(p_atri)) / I4;


        d(ao_valve) = y(ao_valve_d);

        d(ao_valve_d) = -Fr(y(ao_valve)) - Ff(y(ao_valve_d)) + Fp(y(p_vent), y(p_aort), y(ao_valve));

        d(mi_valve) = y(mi_valve_d);

        d(mi_valve_d) = -Fr(y(mi_valve)) - Ff(y(mi_valve_d)) + Fp(y(p_atri), y(p_vent), y(mi_valve));


        d(q_av) = L_av_inverse(y(a_aorta)) * (y(p_vent) - y(p_aort) - R_av(y(ao_valve)) * y(q_av)
                                         - B_av(y(ao_valve), y(a_aorta)) * y(q_av) * fabs(y(q_av)));


        d(q_mi) = L_mi_inverse() * (y(p_atri) - y(p_vent) - R_av(y(mi_valve)) * y(q_mi)
                                              - B_mi(y(mi_valve)) * y(q_mi) * fabs(y(q_mi)));

        d(q_pu) = L_pu_inverse() * (P_pulmonary - y(p_atri) - R_pu() * y(q_pu)
                                                - B_pu() * y(q_pu) * fabs(y(q_pu)) );

        d(q_PUMP) = pump_eq(y(p_vent), y(p_PUMP_END), y(q_PUMP));

        return d;
    }

    Matrix12x19 F_d(const Vector19d & y, const double & t) {
        Matrix12x19 d;
        d.setZero();

        d(v_v, v_v_d) = 1;

        d(v_v_d, p_vent) = (-R1 * y(v_v_d) + 1 )/ I1;
        d(v_v_d, v_v_d) = (-R1 * y(p_vent))/ I1;
        d(v_v_d, v_v) = (- e1(t) )/ I1;

        d(v_a, v_a_d) = 1;

        d(v_a_d, p_atri) = (-R4 * y(v_a_d) + 1) / I4;
        d(v_a_d, v_a_d) = (-R4 * y(p_atri) ) / I4;
        d(v_a_d, v_a) = ( - e4(t) ) / I4;

        d(ao_valve, ao_valve_d) = 1;

        d(ao_valve_d, ao_valve) = -Fr_d(y(ao_valve)) + Fp_dTet(y(p_vent), y(p_aort), y(ao_valve));
        d(ao_valve_d, ao_valve_d) = - Ff_tet_d(y(ao_valve_d));
        d(ao_valve_d, p_vent) = Fp_dPfrom(y(p_vent), y(p_aort), y(ao_valve));
        d(ao_valve_d, p_aort) = Fp_dPto(y(p_vent), y(p_aort), y(ao_valve));

        d(mi_valve, mi_valve_d) = 1;

        d(mi_valve_d, mi_valve) = -Fr_d(y(mi_valve)) + Fp_dTet(y(p_atri), y(p_vent), y(mi_valve));
        d(mi_valve_d, mi_valve_d) = - Ff_tet_d(y(mi_valve_d)) ;
        d(mi_valve_d, p_atri) = Fp_dPfrom(y(p_atri), y(p_vent), y(mi_valve));
        d(mi_valve_d, p_vent) = Fp_dPto(y(p_atri), y(p_vent), y(mi_valve));



        d(q_av, a_aorta) = L_av_inverse_d(y(a_aorta)) * (y(p_vent) - y(p_aort) - R_av(y(ao_valve)) * y(q_av)
                                         - B_av(y(ao_valve), y(a_aorta)) * y(q_av) * fabs(y(q_av)))

                           + L_av_inverse(y(a_aorta)) * (- B_av_dAorta_area(y(ao_valve), y(a_aorta)) * y(q_av) * fabs(y(q_av)));

        d(q_av, p_vent) = L_av_inverse(y(a_aorta));
        d(q_av, p_aort) = L_av_inverse(y(a_aorta)) * ( -1);
        d(q_av, ao_valve) = L_av_inverse(y(a_aorta)) * ( - R_av_d(y(ao_valve)) * y(q_av)
                                         - B_av_dAv_state(y(ao_valve), y(a_aorta)) * y(q_av) * fabs(y(q_av)));

        d(q_av, q_av) = L_av_inverse(y(a_aorta)) * (- R_av(y(ao_valve))
                                         - B_av(y(ao_valve), y(a_aorta)) * 2 * fabs(y(q_av)));



        d(q_mi, p_atri) = L_mi_inverse();
        d(q_mi, p_vent) = L_mi_inverse() * (-1);
        d(q_mi, mi_valve) = L_mi_inverse() * ( - R_av_d(y(mi_valve)) * y(q_mi)
                                              - B_mi_dMi_state(y(mi_valve)) * y(q_mi) * fabs(y(q_mi)));

        d(q_mi, q_mi) = L_mi_inverse() * ( - R_av(y(mi_valve))
                                          - B_mi(y(mi_valve)) * 2 * fabs(y(q_mi)));



        d(q_pu, p_atri) = L_pu_inverse() * (-1);
        d(q_pu, q_pu) = L_pu_inverse() * (- R_pu() - B_pu() * 2 * fabs(y(q_pu)) );

        d(q_PUMP, p_vent) = pump_eq_d_lvp(y(p_vent), y(p_PUMP_END), y(q_PUMP));
        d(q_PUMP, p_PUMP_END) = pump_eq_d_ap(y(p_vent), y(p_PUMP_END), y(q_PUMP));
        d(q_PUMP, q_PUMP) = pump_eq_d_pflow(y(p_vent), y(p_PUMP_END), y(q_PUMP));


        return d;
    }


    //algebraic part
    Vector7d f(const Vector19d & y)
    {
        Vector7d ff;
        ff(0) = y(v_v_d) - y(q_mi) + y(q_av) + y(q_PUMP);
        ff(1) = y(v_a_d) - y(q_pu) + y(q_mi);
        ff(2) = y(p_aort) - edge1v->get_pressure(y(a_aorta), heart_v, 0)/cc;
        ff(3) = y(q_av) - y(a_aorta) * (alfa0 * y(a_aorta) + beta0);

        ff(4) = y(q_PUMP) - y(S1)*(alfa1 * y(S1) + beta1) - y(S2)*(alfa2 * y(S2) + beta2);
        ff(5) = edge2p->get_pressure(y(S1), pump_connect_v, 0) +
                density/2 * std::pow((alfa1 * y(S1) + beta1), 2) -
                (y(p_PUMP_END) * cc + density/2 * std::pow(y(q_PUMP)/S_pump, 2));

        ff(6) = edge3p->get_pressure(y(S2), pump_connect_v, 0) +
                density/2 * std::pow((alfa2 * y(S2) + beta2), 2) -
                (y(p_PUMP_END) * cc + density/2 * std::pow(y(q_PUMP)/S_pump, 2));

        return ff;
    }
    Matrix7x19 f_d(const Vector19d & y)
    {
        Matrix7x19 ff;
        ff.setZero();

        ff(0, v_v_d) = 1;
        ff(0, q_mi) = -1;
        ff(0, q_av) = 1;
        ff(0, q_PUMP) = 1;

        ff(1, v_a_d) = 1;
        ff(1, q_pu) = -1;
        ff(1, q_mi) = 1;

        ff(2, p_aort) = 1;
        ff(2, a_aorta) = - edge1v->get_d_pressure_d_s(y(a_aorta), heart_v, 0)/cc;

        ff(3, q_av) = 1;
        ff(3, a_aorta) = - (2 * alfa0 * y(a_aorta) + beta0);


        ff(4, q_PUMP) = 1;
        ff(4, S1) = - (2 * alfa1 * y(S1) + beta1);
        ff(4, S2) = - (2 * alfa2 * y(S2) + beta2);


        ff(5, S1) = edge2p->get_d_pressure_d_s(y(S1), pump_connect_v, 0) +
                density * (alfa1 * y(S1) + beta1)  * alfa1;
        ff(5, p_PUMP_END) =  -1*cc;

        ff(5, q_PUMP) = - density * y(q_PUMP) /(S_pump*S_pump);



        ff(6, S2) = edge3p->get_d_pressure_d_s(y(S2), pump_connect_v, 0) +
                density * (alfa2 * y(S2) + beta2) * alfa2;

        ff(6, p_PUMP_END) =  -1*cc;

        ff(6, q_PUMP) = - density * y(q_PUMP) /(S_pump*S_pump);

        return ff;
    }



    Vector19d y0, yn, R, y2prev;
    Matrix19d B;

    double LV_P, LV_V;
    double LA_P, LA_V;
    double aortic_valve, mitral_valve;
public:
    void set_w(const double & ww)
    {
        Pump::set_w(ww);
    }
    double get_ESPVR()
    {
        return Heart_part::get_ESPVR();
    }
    void set_ESPVR(double new_ESPVR)
    {
        Heart_part::set_ESPVR(new_ESPVR);
    }
    double get_PveinPressure()
    {
        return Heart_part::get_PveinPressure();
    }
    void set_PveinPressure(double new_PveinPressure)
    {
        Heart_part::set_PveinPressure(new_PveinPressure);
    }
    double get_LV_P()
    {
        return LV_P;
    }
    double get_LV_V()
    {
        return LV_V;
    }
    double get_LA_P()
    {
        return LA_P;
    }
    double get_LA_V()
    {
        return LA_V;
    }
    double get_aortic_valve()
    {
        return aortic_valve;
    }
    double get_mitral_valve()
    {
        return mitral_valve;
    }
    double get_flow_av()
    {
        return y0(q_av);
    }
    double get_flow_total()
    {
        return y0(S2) * (alfa2 * y0(S2) + beta2);
    }
    double get_aortic_root_pressure()
    {
        return y0(p_aort);
    }
    Heart_AV_pump(const std::string & id, Edge * edge1, Edge * edge2, Edge * edge3,
                  Simple_vertex * heart, Simple_vertex * pump_connect,
              double density,
              double viscosity,

              double L_av,
              double L_pu,
              double L_mi,
              double B_pu_const,
              double B_av_denomin,
              double B_mi_denomin,

              double pulmVeinsPressure,
              double initLVvolume,
              double LV_V0, double LA_V0,
              double LV_ESPVR, double LV_EDPVR,
              double LA_ESPVR, double LA_EDPVR,
              double valvePressureForceCoeff,
              double valveFrictionalForceCoeff,
              double LV_inertiaCoeff,
              double LV_dynamicResistanceCoeff,
              double LA_inertiaCoeff,
              double LA_dynamicResistanceCoeff,
              const Pump & pump
              )
    : Base(id),  Heart_part(
                     density,
                     viscosity,
                     L_av,
                     L_pu,
                     L_mi,
                     B_pu_const,
                     B_av_denomin,
                     B_mi_denomin,
                     pulmVeinsPressure,
                     LV_V0,  LA_V0,
                     LV_ESPVR, LV_EDPVR,
                     LA_ESPVR,  LA_EDPVR,
                     valvePressureForceCoeff,
                     valveFrictionalForceCoeff,
                     LV_inertiaCoeff,
                     LV_dynamicResistanceCoeff,
                     LA_inertiaCoeff,
                     LA_dynamicResistanceCoeff
                     ),
                   Pump(pump),
      edge1v(edge1), edge2p(edge2), edge3p(edge3),
      heart_v(heart), pump_connect_v(pump_connect),
      density(density), viscosity(viscosity)
    {
        std::cout << this->a << std::endl;
        std::cout << this->b << std::endl;
        std::cout << this->c << std::endl;
        std::cout << this->d << std::endl;
        std::cout << this->e << std::endl;
        std::cout << this->R_rec << std::endl;
        std::cout << this->L_per << std::endl;
        std::cout << this->R_per <<std::endl;
        std::cout << this->S_pump << std::endl << std::endl;
        //see config try initLVvolume = 130 for healthy heart
        y0.setZero();
        y0(v_v) = initLVvolume;
        y0(v_a) = 10;
        y0(ao_valve) = 0.01;
        y0(mi_valve) = 0.01;
        y0(q_pu) = 1;
        y0(S1) = y0(S2) = y0(a_aorta) = 8;
        y0(p_vent) = 20;
        y0(p_PUMP_END) = y0(p_aort) = 80;
        y0(p_atri) = P_pulmonary;
        y0(q_PUMP) = 0;

        y2prev = y0;
        B.setZero();
        R.setZero();
        yn.setZero();
        w = 0;
    }
protected:
    void update()
    {
        OutgoingCompatibilityCoeffs(edge1v, heart_v, alfa0, beta0);
        OutgoingCompatibilityCoeffs(edge2p, pump_connect_v, alfa1, beta1);
        OutgoingCompatibilityCoeffs(edge3p, pump_connect_v, alfa2, beta2);

        double ts = T-dt;
        double tau = dt;
        while (tau > 1e-12) {

            double t = ts + tau;
            yn = y0;// + (y0 - y2prev);

            int j = 0;
            double R0_norm = 0;

            int MaxIt = 40;
            while (1) {
                //Newton
                if (j > MaxIt) {
                    j = 0;
                    //yn = y0;
                    tau/=2;
                    t = ts + tau;

                    //std::cout << "!" << std::endl;
                    if (tau < 1e-12) {
                        std::cout << "TIME: " << T << std::endl<< std::endl;

                        std::cout << y0 << std::endl << std::endl;

                        std::cout << yn << std::endl;
                        std::cout << "Residual:" << std::endl;
                        std::cout << R << std::endl;
                        std::cout << "Residual norm: " << R.lpNorm<Eigen::Infinity>() << std::endl;
                        std::cout << "Relative error: " << R.lpNorm<Eigen::Infinity>()/R0_norm << std::endl;
                        //break;

                        throw("Newton from Heart_AV PUMP failed");
                     }
                }

                R.head<12>() = yn.head<12>() - y0.head<12>() - tau * F(yn, t);
                R.tail<7>() = f(yn);
                if (R0_norm == 0) {
                    R0_norm = R.lpNorm<Eigen::Infinity>();
                }
                if (R.lpNorm<Eigen::Infinity>() < 1e-6 || R.lpNorm<Eigen::Infinity>() < 1e-12*R0_norm) {
                   // std::cout << R.lpNorm<Eigen::Infinity>() << std::endl;

                    break;
                }
                B.setZero();
                B.topLeftCorner<12, 12>().setIdentity();
                B.topRows<12>() -= tau * F_d(yn, t);
                B.bottomRows<7>() = f_d(yn);

                yn -= B.colPivHouseholderQr().solve(R);
                j++;
            }
            y2prev = y0;
            y0 = yn;
    /*
            const auto f_yynt = f_y(yn,t);
            const auto eigenvalues = f_yynt.eigenvalues();
            for (int i = 0; i < 8; i++) {
                if (eigenvalues(i).real() > 1)
                    cout << t << " " << eigenvalues(i) << endl;
            }
    */
            ts += tau;
            tau = T - ts;
        }
/*
        if (T >= 6 && T <= 7) {
            ff++;
            if (ff % 6 == 0)
                std::cout << y0(v1) << " " << P1(y0(s), y0(tet51)) << std::endl;
        }
        */
        if (y0(v_v) < 0 || y0(v_a) < 0) {
            std::cout << "time: " << T << std::endl;
            std::cout << "LV vol/pressure: " << y0(v_v) << " " << y0(p_vent) << std::endl;
            std::cout << "LA vol/pressure: " << y0(v_a) << " " << y0(p_atri) << std::endl;

            throw("OMG");
        }
        //save results
        LV_P = y0(p_vent);
        LV_V = y0(v_v);
        LA_P = y0(p_atri);
        LA_V = y0(v_a);
        aortic_valve = y0(ao_valve)/r2d;
        mitral_valve = y0(mi_valve)/r2d;


        //save results
        setSU(edge1v, heart_v, y0(a_aorta), alfa0*y0(a_aorta) + beta0);
        setSU(edge2p, pump_connect_v, y0(S1), alfa1*y0(S1) + beta1);
        setSU(edge3p, pump_connect_v, y0(S2), alfa2*y0(S2) + beta2);

        if (y0(q_pu) < -1) {
            std::cout << y0(q_pu) << std::endl;
        }
    }
};

#endif // HAV_PUMP_H
