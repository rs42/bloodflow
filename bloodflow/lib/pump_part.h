#ifndef PUMP_PART_H
#define PUMP_PART_H
#include <cmath>
class Pump
{
protected:

    const double cpp = 1333.22;
    //Sputnik-D
    /*
        const double S_pump = 1.1;//test it! pump area
        const double ce1 = -8.5e-1 * pcc * 0.0036;
        const double ce2 = -7.3e-4 * pcc * 0.06;
        const double ce3 = 7.5e-7 * pcc;
        const double ce4 = -9.89e+1 * pcc * 6e-4
                +  15*pcc/1000 ; //plus periphial
        const double ce = 1.174422e-4 * 1000/60;
        const double ancf = 6.62775 * pcc * 0.0036;
        const double Rper = 2e-1 * pcc/278; //plus periphial
      */


        //HM2
    double a, b, c, d, e, R_rec, L_per, R_per;
    double S_pump;//pump area
   // const double ce1 = -8.6e-1 * pcc * 0.0036;
  //  const double ce2 = 3.2e-4 * pcc * 0.06;
   // const double ce3 = 9.5e-7 * pcc;
   // const double ce4 = -22.97 * pcc * 1e-3
   //         +  15*pcc/1000 ; //plus periphial

            //   +  20*pcc/1000 ; //plus periphial
  //  const double ce = 3.593e-4 * 1000/60;
   // const double ancf = 3.07 * pcc * 0.0036;
    //const double Rper = 3.8e-1 * pcc/278; //plus periphial
    //const double Rper = 2e-1 * pcc/278; //plus periphial

        double w;

        double pump_eq(const double & lv_pressure,
                       const double & aortic_pressure,
                       const double & p_flow);

        double pump_eq_d_lvp(const double & lv_pressure,
                       const double & aortic_pressure,
                       const double & p_flow);
        double pump_eq_d_ap(const double & lv_pressure,
                       const double & aortic_pressure,
                       const double & p_flow);

        double pump_eq_d_pflow(const double & lv_pressure,
                       const double & aortic_pressure,
                       const double & p_flow);
public:
        Pump(double a, double b, double c, double d,
             double e, double R_rec, double L_per, double R_per, double S);
        void set_w(const double & ww);
};

#endif // PUMP_PART_H
