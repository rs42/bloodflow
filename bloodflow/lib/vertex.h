#ifndef VERTEX_H
#define VERTEX_H

#include <iostream>
#include <cassert>
#include <cmath>
#include <Eigen/Dense>

#include "graph.h"
#include "edge.h"
#include "calculator.h"

typedef std::pair<std::string, double *> Parameter;
typedef std::vector<Parameter> ParameterBlock;
#define PAIR(x) Parameter((#x), &(x))

class Meta_vertex
{
public:
    std::string id;
    Meta_vertex(const std::string & id);
    //virtual void update_boundary() = 0;
    virtual ~Meta_vertex();
    std::string get_id();

    std::vector<ParameterBlock> parameterBlocks;
};


template<typename Edge,
         template<typename, int, int ...> typename Matrix, typename Scalar, auto Dynamic>
class Vertex:
    public Meta_vertex
{
protected:
    typedef Meta_vertex Base;
    std::vector<std::reference_wrapper<BaseTimeCalculator>> timeCalculators;

    enum Vector_index {i_Sq, i_U};//square of cross-section, velocity
    const static int N = 2;

    typedef Matrix<Scalar, Dynamic, Dynamic> MatrixDD;
    typedef Matrix<Scalar, N, Dynamic> MatrixND;
    typedef Matrix<Scalar, N, N> MatrixNN;
    typedef Matrix<Scalar, N, 1> VectorN;
    typedef Matrix<Scalar, Dynamic, 1> VectorD;
    typedef Matrix<Scalar, 1, Dynamic> RowVectorD;

    double dt;
    double T;//overall time
public:
    Vertex(const std::string & idd)
    : Base(idd)
    {
        dt = 0;
        T = 0;
    }

    void set_dt(double d)
    {
        dt = d;
    }
    void set_T(double d)
    {
        T = d;
    }
protected:
    void OCNFO2(const Edge * e, const Simple_vertex * sv, VectorN & OC, VectorN & OF, VectorN & OFF) const
    {
        OC(0) = e->get_Vp_s(sv, 0);
        OC(1) = e->get_Vp_u(sv, 0);
        OF(0) = e->get_Vp_s(sv, 1);
        OF(1) = e->get_Vp_u(sv, 1);
        OFF(0) = e->get_Vp_s(sv, 2);
        OFF(1) = e->get_Vp_u(sv, 2);
    }
    void OCNFO2_2nd(const Edge * e, const Simple_vertex * sv, VectorN & OC, VectorN & NC, VectorN & NF, VectorN & NFF) const
    {
        OC(0) = e->get_Vp_s(sv, 0);
        OC(1) = e->get_Vp_u(sv, 0);
        NC(0) = e->get_V_s(sv, 0);
        NC(1) = e->get_V_u(sv, 0);
        NF(0) = e->get_V_s(sv, 1);
        NF(1) = e->get_V_u(sv, 1);
        NFF(0) = e->get_V_s(sv, 2);
        NFF(1) = e->get_V_u(sv, 2);
    }
    /*
    void OutgoingCompatibilityCoeffs_old(const Edge * e, const Simple_vertex * sv, double & a, double & b) const
    {
        // Коэффициенты условия совместности (U = a * S + b) в начале i-ой ветви исходящей из узла
	    double sigm;
        double P, Ps, sound;
	    VectorN U, V, S, FPR;
	    VectorN OC, OF, OFF, NC, NF;
	    SqMatrixNN SL, SP, OM, OMO;

	    //Call OCNFO(br, Z, OC, OF, NC, NF)
        OCNFO2(e, OC, OF, OFF);
	
    	V = OC;
	
        e->Equation_of_state(V, sound, P, Ps);
	    e->Eigenvalues(V, S, SL, SP, sound, P);
        e->OMEGAB(V, OM, OMO, sound, P);
        FPR = e->get_FPRCH(sv, V, 0);
        const double dx = e->get_dx();

        sigm = S(0) * dt / dx;

    	a = - OM(0,0) / OM(0,1);
    
        //2nd order explicit without rhs
        //b = (1.0+3.0*sigm/2.0)*(-a*OC(0) + OC(1)) -
        //    2.0*sigm*(-a*OF(0) + OF(1)) + 0.5*sigm*(-a*OFF(0) + OFF(1));

        //with rhs
        b = (1+3*sigm/2)*(-a*OC(0) + OC(1)) -
            2*sigm*(-a*OF(0) + OF(1)) + 0.5*sigm*(-a*OFF(0) + OFF(1)) - (FPR(0)*a - FPR(1))*dt;
    }
*/
    void OutgoingCompatibilityCoeffs(const Edge * e, const Simple_vertex * sv, double & a, double & b) const
    {
        // Outgoing compatibility coefficients a, b for (U = a * S + b) at the beginning of Edge e
        double sigm;
        double P, Ps, sound;
        VectorN U, V, S, FPR;
        VectorN OC, NFF, NC, NF;
        MatrixNN SL, SP, OM, OMO;

        OCNFO2_2nd(e, sv, OC, NC, NF, NFF);
        //OC = NC;
        //V = NF;
        V = OC;

        e->get_pressure(V(0), sound, P, Ps, sv, 0);
        e->get_eigenvalues(V, S, SL, SP, sound, P);
        e->get_big_omega(V, OM, OMO, sound, P);
        FPR = e->get_right_hand_side(sv, V, 0); //not really implicit way
        const double dx = e->get_dx();
        sigm = S(0) * dt / dx;

        //implicit, actually not lol
        a = - OM(0,0) / OM(0,1);
        //b = (a * (sigm * NF(0) - OC(0)) + OC(1) - sigm * NF(1) + (FPR(1) - a * FPR(0)) * dt) / (1 - sigm);

        //b = (-a * NC(0)  + a * sigm * (NF(0) - NC(0)) + NC(1) - sigm * (NF(1) - NC(1)) + dt * (FPR(1) - a * FPR(0)));

         b = (a * (sigm * (2 * NF(0) - 0.5 * NFF(0)) - OC(0)) -
            (sigm * (2 * NF(1) - 0.5 * NFF(1)) - OC(1))
              + dt * (FPR(1) - a * FPR(0)))
                 / (1 - 3 * sigm / 2);

    }
    void setSU(Edge * e, const Simple_vertex * sv, const double & S, const double & U)
    {
        e->set_V_s(sv, 0, S);
        e->set_V_u(sv, 0, U);
    }
    double get_eos_deriv(const Edge * e, const double & S)
    {
        return e->Equation_of_state_deriv(S);
    }
    double get_pressure(const Edge * e, const double & S) const
    {
        return e->Equation_of_state_optimized(S);
    }
    virtual void update() = 0;
public:
    void update_boundary()
    {
        update();
        for (BaseTimeCalculator & calc: timeCalculators)
            calc.update(dt);
    }//need to think about the architecture

};


template<typename Edge,
         template<typename, int, int ...> typename Matrix, typename Scalar, auto Dynamic>
class Heart:
    public Vertex<Edge, Matrix, Scalar, Dynamic>
{
    typedef Vertex<Edge, Matrix, Scalar, Dynamic> Base;

    IntegralCalculator strokeVOL;
protected:
    using Base::timeCalculators;
public:
    Heart(const std::string & id)
    : Base(id), strokeVOL(std::bind(&Heart::get_flow_av, this), 1)
    {
        //strokeVOL = IntegralCalculator(std::bind(&Heart::get_flow_av, this), 1);
        timeCalculators.push_back(strokeVOL);
    }
public:
    double get_stroke_volume_av()
    {
        return strokeVOL.value();
        //return stroke_volume_av;
    }
    virtual double get_stroke_volume_total()
    {
        return get_stroke_volume_av();
    }
    virtual double get_flow_av() = 0;
};

template<typename Edge,
         template<typename, int, int ...> typename Matrix, typename Scalar, auto Dynamic>
class True_0d_heart:
    public Heart<Edge, Matrix, Scalar, Dynamic>
{
    typedef Heart<Edge, Matrix, Scalar, Dynamic> Base;

    IntegralCalculator aortic_valve_time_opened, mitral_valve_time_opened;
    BinaryCalculator max_aortic_pressure, min_aortic_pressure;
    AreaCalculator left_ventricular_work_pv;
    double integrate_aortic_valve_time_opened()
    {
        return (get_aortic_valve() > 65);
    }
    double integrate_mitral_valve_time_opened()
    {
        return (get_mitral_valve() > 65);
    }
protected:
    using Base::timeCalculators;
public:
    True_0d_heart(const std::string & id)
        : Base(id),
          aortic_valve_time_opened(std::bind(&True_0d_heart::integrate_aortic_valve_time_opened, this), 1),
          mitral_valve_time_opened(std::bind(&True_0d_heart::integrate_mitral_valve_time_opened, this), 1),
          max_aortic_pressure(static_cast<double const&(*)(double const&, double const&)>(std::max), std::bind(&True_0d_heart::get_aortic_root_pressure, this), 0, 1),
          min_aortic_pressure(static_cast<double const&(*)(double const&, double const&)>(std::min), std::bind(&True_0d_heart::get_aortic_root_pressure, this), 9999, 1),
          left_ventricular_work_pv(std::bind(&True_0d_heart::get_LV_V, this), std::bind(&True_0d_heart::get_LV_P, this), 1)
    {
        timeCalculators.push_back(aortic_valve_time_opened);
        timeCalculators.push_back(mitral_valve_time_opened);
        timeCalculators.push_back(max_aortic_pressure);
        timeCalculators.push_back(min_aortic_pressure);
        timeCalculators.push_back(left_ventricular_work_pv);
    }
    virtual double get_aortic_root_pressure() = 0;
    virtual double get_LV_P() = 0;
    virtual double get_LV_V() = 0;
    virtual double get_LA_P() = 0;
    virtual double get_LA_V() = 0;
    virtual double get_aortic_valve() = 0;
    double get_LV_work()
    {
        return left_ventricular_work_pv.value();
    }
    double get_time_aortic_valve_opened()
    {
        return aortic_valve_time_opened.value();
    }
    virtual double get_mitral_valve() = 0;
    double get_time_mitral_valve_opened()
    {
        return mitral_valve_time_opened.value();
    }
    double get_pulsePressure()
    {
        return max_aortic_pressure.value() - min_aortic_pressure.value();
    }
    double get_max_aortic_pressure()
    {
        return max_aortic_pressure.value();
    }
    double get_min_aortic_pressure()
    {
        return min_aortic_pressure.value();
    }
    virtual double get_ESPVR() = 0;
    virtual void set_ESPVR(double new_ESPVR) = 0;
    virtual double get_PveinPressure() = 0;
    virtual void set_PveinPressure(double new_PveinPressure) = 0;
};

template<typename Edge,
         template<typename, int, int ...> typename Matrix, typename Scalar, auto Dynamic>
class Pump_0d_heart:
    public True_0d_heart<Edge, Matrix, Scalar, Dynamic>
{
    typedef True_0d_heart<Edge, Matrix, Scalar, Dynamic> Base;
protected:
    using Base::timeCalculators;
    using Base::get_stroke_volume_av;
    IntegralCalculator stroke_volume_pump;
    IntegralCalculator stroke_volume_total;
    BinaryCalculator min_pump_flow;
public:
    Pump_0d_heart(const std::string & id)
        : Base(id), stroke_volume_pump(std::bind(&Pump_0d_heart::get_Q, this), 1),
          stroke_volume_total(std::bind(&Pump_0d_heart::get_flow_total, this), 1),
          min_pump_flow(static_cast<double const&(*)(double const&, double const&)>(std::min), std::bind(&Pump_0d_heart::get_Q, this), 999999, 1)
    {
        timeCalculators.push_back(stroke_volume_pump);
        timeCalculators.push_back(stroke_volume_total);
        timeCalculators.push_back(min_pump_flow);
    }
    virtual void set_w(const double &ww) = 0;
    virtual double get_H() = 0;
    virtual double get_Q() = 0;
    virtual double get_flow_total() = 0;
    double get_min_pump_flow()
    {
        return min_pump_flow.value();
    }
    double get_stroke_volume_pump()
    {
        return stroke_volume_pump.value();
    }
    virtual double get_stroke_volume_total() override
    {
        //return stroke_volume_total.value();
        return get_stroke_volume_pump() + get_stroke_volume_av();
    }
};

template<typename Edge,
         template<typename, int, int ...> typename Matrix, typename Scalar, auto Dynamic>
class Internal_vertex:
    public Vertex<Edge, Matrix, Scalar, Dynamic>
{
    typedef Vertex<Edge, Matrix, Scalar, Dynamic> Base;

    using Base::OutgoingCompatibilityCoeffs;
    using Base::OCNFO2;
    using typename Base::VectorN;
    using typename Base::VectorD;
    using typename Base::MatrixDD;

    int K;
    std::vector<Edge*> edges;
    Simple_vertex * sv;
    double density;
public:
    Internal_vertex(const std::string & id, const std::vector<Edge*> & edges, Simple_vertex * sv)
    : Base(id), edges(edges), sv(sv)
    {
        K = edges.size();
        density = edges[0]->get_density();
    }
private:
    /*
    void MakeRMatrix(MatrixDD & RMatrix, double & RDet) const
    {
        VectorD Resist(K);
        
        for (unsigned i = 0; i < K; i++) {
            Resist(i) = edges[i]->get_resistance();
        }
        // ! Подсчет определителя матрицы сопротивлений (было: "матрицы системы" ??)
        RDet = 0;

        for (unsigned i = 0; i < K; i++) {
            double W = 1;
            for (unsigned j = 0; j < K; j++) {
                if (j == i) continue;
                W *= Resist(j);
            }
            RDet += W;
        }

        // ! Подсчет диагональных элементов матрицы в системе для F
	    RMatrix.setZero();

        for (unsigned i = 0; i < K; i++) {
            double A = 0;
            for (unsigned j = 0; j < K; j++) {
                double W = 1;
                if (i == j) continue;
                for (unsigned m = 0; m < K; m++) {
                    if (m == i || m == j) continue;
                    W *= Resist(m);
                }
                A += W;
            }
            RMatrix(i, i) = -A;
        }

        // ! Подсчет недиагональных элементов матрицы в системе для F
        for (unsigned i = 0; i < K; i++) {
            for (unsigned j = 0; j < K; j++) {
                double W = 1;
                if (i == j) continue;
                for (unsigned m = 0; m < K; m++) {
                    if (m != i && m != j)
                        W *= Resist(m);
                }
                RMatrix(i, j) = W;
            }
        }
    }
*/
    void findSpeed(const VectorD & alf, const VectorD & bet, const VectorD & Xs, VectorD & Xu) const
    {
        for (int i = 0; i < K; i++) {
            Xu(i) = alf(i) * Xs(i) + bet(i);
        }
    }

    void find_F_Yac_Bern(VectorD & F, MatrixDD & YAC, const VectorD & Xs,
                         const VectorD & Xu, const VectorD & alf,
                         const VectorD & bet)
    {
        VectorD P(K), Ps(K);
        double sound;

        for (int i = 0; i < K; i++) {
          //  VectorN V;
           // V(0) = Xs(i);
            //V(1) = Xu(i);
            edges[i]->get_pressure(Xs(i), sound, P(i), Ps(i), sv, 0);
        }

        //find F
        F.setZero();
        F(0) =  Xu(0) * Xs(0);

        for (int i = 1; i < K; i++) {
            F(i) = P(i-1) - P(i) +
                    0.5 * density * (std::pow(Xu(i-1), 2) - std::pow(Xu(i), 2));
            F(0) += Xu(i) * Xs(i);
        }
        //F found


        //find YAC
        YAC.setZero();

        for (int i = 0; i < K; i++) {
            YAC(0,i) = 2 * alf(i) * Xs(i) + bet(i);
        }
        for (int i = 1; i < K; i++) {
            YAC(i,i-1) =  Ps(i-1) + alf(i-1) * density * Xu(i-1);
            YAC(i,i)   = -Ps(i) - alf(i) * density * Xu(i);
        }
        //done
    }

/*
    void FUZ(VectorD & F, VectorD & Xs, VectorD & Xu)
    {
        //	Подпрограмма вычисляет значение векторной функции определяющей 
        //	нелинейную систему в узле
	    VectorD P(K), Ps(K), alf(K), bet(K);
	    MatrixDD RM(K, K);
	    VectorN V;
	    double RDet, sound;

        for (unsigned i = 0; i < K; i++) {
            OutgoingCompatibilityCoeffs(edges[i], sv, alf(i), bet(i));
            V(0) = Xs(i);
		    V(1) = Xu(i);
            edges[i]->Equation_of_state(V, sound, P(i), Ps(i));
        }
        MakeRMatrix(RM, RDet);

        // ! Окончательное выражение для F

	    F.setZero();
	
        for (unsigned i = 0; i < K; i++) {
            for (unsigned j = 0; j < K; j++) {
			    if (i != j)
				    F(i) += RM(i,j) * P(j);
		    }
		    F(i) += RM(i,i) * P(i) - RDet * (alf(i) * Xs(i) + bet(i)) * Xs(i);
	    }
    }

    void TDYacobian(MatrixDD & YAC, const VectorD & Xs, const VectorD & Xu)
    {
        //	Подпрограмма вычисляет значение якобиана нелинейной системы в точке ветвления
	    VectorD P(K), Ps(K), alf(K), bet(K);
	    MatrixDD RM(K, K);
	    VectorN V;
	    double RDet, sound;
//this is the same code as in FUZ!!!!
        for (unsigned i = 0; i < K; i++) {
            OutgoingCompatibilityCoeffs(edges[i], sv, alf(i), bet(i));
            V(0) = Xs(i);
		    V(1) = Xu(i);
            edges[i]->Equation_of_state(V, sound, P(i), Ps(i));
        }
        MakeRMatrix(RM, RDet);
//end
        
        //! Подсчет элементов матрицы якобиана
	    YAC.setZero();
        for (unsigned i = 0; i < K; i++) {
	    	YAC(i,i) = -RDet * (2.0 * alf(i) * Xs(i) + bet(i)) + RM(i,i) * Ps(i);
            for (unsigned j = 0; j < K; j++) {
		    	if (i != j) YAC(i,j) = RM(i,j) * Ps(j);
            }
        }
    }
    */

public:
    void update() final
    {
        K = edges.size();
        MatrixDD YAC(K,K);
        VectorD F(K), Xs(K), Xu(K);
        //s - square
        //u - speed

        VectorD alf(K), bet(K); //compatibility coeffs

        for (int i = 0; i < K; i++) {
            OutgoingCompatibilityCoeffs(edges[i], sv, alf(i), bet(i));
            const Edge *e = edges[i];
            Xs(i) = e->get_Vp_s(sv, 0);
            Xu(i) = e->get_Vp_u(sv, 0);
        }

        //Solve system to find Xs with Netwons method
        const int Nmax = 20;//limit of iterations
        double F_norm = 1, F_normZero = 0;
        const double atol = 1e-6, rtol = 1e-13;
        for (int i = 0; F_norm > atol && F_norm > rtol*F_normZero; i++) {
	        if (i > Nmax) {
                for (int i = 0; i < K; i++) {
                    Edge *e = edges[i];
                    std::cout << "edge " << i << " " << e->get_id() << std::endl;
                }

                std::cout << "Xs: " << std::endl << Xs << std::endl;
                std::cout << "F: " << std::endl << F << std::endl;
                std::cout << sv->get_id() << std::endl;
                std::cout << "abs: " <<  F_norm << " rel: " << F_norm / F_normZero << std::endl;
                std::cout << "internalVertex Newton failed" << std::endl;
                throw("InternalVertex Newton failed");
            }

            find_F_Yac_Bern(F, YAC, Xs, Xu, alf, bet);

            //for large K dont use .inverse(), inverse implicitly!
            Xs -= YAC.inverse() * F;

            F_norm = F.template lpNorm<Eigen::Infinity>();
            if (i == 0)
                F_normZero = F_norm;

            findSpeed(alf, bet, Xs, Xu);
        }
        //done

        //send results to edges
        for (int i = 0; i < K; i++) {
#ifndef NDEBUG
         //   std::cout << Base::id << ": " << Xs(i) << " " << Xu(i) << " "<< Xs(i)*Xu(i) <<std::endl;
#endif
            Edge *e = edges[i];
            e->set_V_s(sv, 0, Xs(i));
            e->set_V_u(sv, 0, Xu(i));
        }
    }
};

/*

template<typename Edge, typename MatrixDD, typename VectorD, typename MatrixND,
         typename SqMatrixNN, typename VectorN, int N>
class Vertex_with_pump:
    public Vertex<Edge, MatrixDD, VectorD, MatrixND, SqMatrixNN, VectorN, N>
{
    typedef Vertex<MatrixDD, VectorD, MatrixND, SqMatrixNN, VectorN, N> Base;
    using typename Base::Edge;
    using Base::edges;
    using Base::dt;
    using Base::OutgoingCompatibilityCoeffs;
    using Base::T;
    using Base::K;

    double w;
    double w_prev;
    VectorN X_prev;
    VectorN X;
    double LV_pressure;
    const double cc = 1333.22;
    const double a1 = 1.2 * cc * 0.0036;
    const double a2 = -2.2e-3 * cc * 0.06;
    const double a3 = 8.9e-7 * cc;
    const double a4 = -1e+2 * cc * 3.6;
    const double a5 = 2.4e-1 * cc;
    double pump_eq(const double & Q) const
    {
        return a1*Q*Q + a2*w*Q + a3*w*w;// + a4*(Q - X_prev(1))/dt
                //+ a5*(w - w_prev)/dt;
    }
    double pump_eq_deriv(const double & Q) const
    {
        return 2*a1*Q + a2*w;// + a4/dt;
    }
    double mass_conserv(const double & P, VectorD &alpha,  VectorD &beta) const
    {
        double res = 0;
        for (unsigned i = 0; i < edges.size(); i++) {
            Edge *e = static_cast<Edge *>(edges[i]);
            const double ip = e->inverse_eos(P);
            res += ip * (alpha(i) * ip + beta(i));
        }
        return res;
    }
    double mass_conserv_deriv(const double & P, VectorD &alpha, VectorD &beta) const
    {
        double res = 0;
        for (unsigned i = 0; i < edges.size(); i++) {
            Edge *e = static_cast<Edge *>(edges[i]);
            const double ip = e->inverse_eos(P);
            const double ipd = e->inverse_eos_deriv(P);
            res += ipd * (alpha(i) * ip + beta(i)) + alpha(i) * ipd * ip;
        }
        return res;
    }
public:
    void set_w(double ww)
    {
        w = ww;
    }
    Vertex_with_pump(const std::string & id)
    : Base(id)
    {
        w = w_prev = 0;
        LV_pressure = 100000;// appr. 75 mmHg
    }
    void set_up()
    {
        K = edges.size();
        Edge *e0 = static_cast<Edge *>(edges[0]);
        X(0) = e0->Equation_of_state_optimized(e0->get_Vp_s(this, 0));
        X(1) = 0;
        X_prev = X;
    }
    void update_boundary()
    {//pump has incoming flow
     //all vessel edges have outcoming flow
     //assume VectorN is (2,1) so X = (P in vertex, Q pump)
        LV_pressure = T > 1? 100000: T*100000;
        X = X_prev;

        VectorN X0;
        SqMatrixNN Yac;
        VectorN F;

        VectorD alpha(K);
        VectorD beta(K);

        for (unsigned i = 0; i < K; i++) {
            OutgoingCompatibilityCoeffs(i, alpha(i), beta(i));
        }

        double X0_norm = 1, F_norm = 1;
        const int Nmax = 20;
        const double eps = 1e-8;
        for (int i = 0; X0_norm > eps || F_norm > eps; i++) {
            if (i > Nmax) {
                if (X0_norm > 1e-6 || F_norm > 1e-6) {
                    std::cout << "pump Newton failed: "  << X0_norm << " " << F_norm << std::endl;
                    throw 0;
                } else {
                    break;
                }
            }

            F(0) = X(0) - LV_pressure - pump_eq(X(1));
            F(1) = X(1) - mass_conserv(X(0), alpha, beta);

            Yac(0,0) = Yac(1,1) = 1;
            Yac(1, 0) = - mass_conserv_deriv(X(0), alpha, beta);
            Yac(0, 1) = - pump_eq_deriv(X(1));
           // std::cout << F(0) << " " << F(1)  << " " << invYac(0,1)  << " " << invYac(1,0) << std::endl;
            X0 = - Yac.inverse()*F;
            X += X0;
            X0_norm = X0.template lpNorm<Eigen::Infinity>();
            F_norm = F.template lpNorm<Eigen::Infinity>();

            //std::cout << "iter: " << i << "res: " << X0_norm << std::endl;
        }

        const double P = X(0);
        for (unsigned i = 0; i < edges.size(); i++) {
            //find S U from P
            Edge *e = static_cast<Edge *>(edges[i]);
            const double S = e->inverse_eos(P);
            const double U = alpha(i)*S + beta(i);
            e->set_V_s(this, 0, S);
            e->set_V_u(this, 0, U);
        }
//#ifndef NDEBUG
        std::cout << "pump: P: " << X(0) << " Q: " << X(1) << std::endl;
//#endif
        w_prev = w;
        X_prev = X;
    }
};
*/

/*
template<typename MatrixDD, typename VectorD, typename MatrixND,
         typename SqMatrixNN, typename VectorN, int N>
class Vertex_with_pump_Bern:
    public Vertex<MatrixDD, VectorD, MatrixND, SqMatrixNN, VectorN, N>
{
    typedef Vertex<MatrixDD, VectorD, MatrixND, SqMatrixNN, VectorN, N> Base;
    using typename Base::Edge;
    using Base::edges;
    using Base::dt;
    using Base::OutgoingCompatibilityCoeffs;
    using Base::T;
    using Base::K;

    double w;
    double w_prev;

    double Q_prev;
    double H_prev;
    double LV_pressure;
    double S_p; //pump area
    bool is_first_time;
    bool is_real_flow;
    const double cc = 1333.22;
    //pump 1 (dont use it)
    const double a1 = 1.2 * cc * 0.0036;
    const double a2 = -2.2e-3 * cc * 0.06;
    const double a3 = 8.9e-7 * cc;
    const double a4 = -1e+2 * cc * 6e-4;
    const double a5 = 2.4e-1 * cc / 100;
    //pump 2
    const double b1 = -1.15e+1 * cc * 0.06;
    const double b2 = 8e-7 * cc;
    const double b3 = -1.055e+2 * cc * 6e-4;
    //pump 3
    const double d1 = -1.75 * cc * 0.0036;
    const double d2 = -8.22e-4 * cc * 0.06;
    const double d3 = 8e-7 * cc;
    const double d4 = -1.037e+2 * cc * 6e-4;
    //pump 4
    const double e1 = -8.5e-1 * cc * 0.0036;
    const double e2 = -7.3e-4 * cc * 0.06;
    const double e3 = 7.5e-7 * cc;
    const double e4 = -9.89e+1 * cc * 6e-4;
    const double e = 1.174422e-4 * 1000/60;
    const double f = 6.62775 * cc * 0.0036;


    double pump_eq(const double & Q) const
    {
        //pump1
        //return a1*Q*Q + a2*w*Q + a3*w*w + a4*(Q - Q_prev)/dt
        //        + a5*(w - w_prev)/dt;

        //pump 2
        //return b1*Q + b2*w*w + b3*(Q - Q_prev)/dt;
        //pump 3
        //return d1*Q*Q + d2*w*Q + d3*w*w + d4*(Q - Q_prev)/dt;
        //pump 4
        const double bf = Q - e*w;
        return e1*Q*Q + e2*w*Q + e3*w*w + e4*(Q - Q_prev)/dt + (bf < 0)*f*bf*bf;
    }
    double pump_eq_deriv(const double & Q) const
    {
        //pump1
        //return 2*a1*Q + a2*w + a4/dt;

        //pump 2
        //return b1 + b3/dt;
        //pump 3
        //return 2*d1*Q + d2*w + d4/dt;
        //pump 4
        const double bf = Q - e*w;
        return 2*e1*Q + e2*w + e4/dt + (bf < 0)*f*2*bf;
    }

public:
    bool get_is_real_flow() const
    {
        return is_real_flow;
    }
    void set_w(double ww)
    {
        w = ww;
    }
    double get_h() const
    {
        return H_prev;
    }
    double get_q() const
    {
        return Q_prev;
    }
    void set_LV_pressure_mmHg(double p)
    {
        LV_pressure = cc*p;
    }
    Vertex_with_pump_Bern(const std::string & id)
    : Base(id)
    {
        is_first_time = 1;
        S_p = M_PI;
        w = w_prev = 0;
        Q_prev = 0;
        LV_pressure = 0;//66660;//100000;// appr. 75 mmHg
        //50 000 newton works when w = 10 000 or more
        //10 000 until when w = 500 or more
    }
    VectorN findF(const VectorN & alpha, const VectorN & beta, const VectorN & X) const
    {
        VectorN F;
        double Qf = 0;
        for (unsigned j = 0; j < K; j++) {
            Qf += X(j) * (alpha(j) * X(j) + beta(j));
        }

        //fill F
        const double Common = LV_pressure + pump_eq(Qf) + std::pow(Qf/S_p, 2)/2;
        for (unsigned j = 0; j < K; j++) {
            Edge *e = static_cast<Edge *>(edges[j]);
            F(j) = e->Equation_of_state_optimized(X(j)) + std::pow(alpha(j) * X(j) + beta(j), 2) / 2 - Common;
        }
        return F;
    }
    double findFnorm(const VectorD & alpha, const VectorD & beta, const VectorD & X) const
    {
        return findF(alpha, beta, X).template lpNorm<Eigen::Infinity>();
    }

    void update_boundary()
    {//pump has incoming flow
     //all vessel edges have outcoming flow
     //Solve Newton's method for S_i
        //LV_pressure = T > 1? 120000 : 120000*T;

        VectorD X(K);
        MatrixDD Yac(K, K);
        for (unsigned i = 0; i < K; i++) {
            Edge *e = static_cast<Edge *>(edges[i]);
            X(i) = e->get_Vp_s(this, 0);
        }

        VectorD X0(K);
        VectorD F(K);

        VectorD alpha(K), beta(K);

        for (unsigned i = 0; i < K; i++) {
            OutgoingCompatibilityCoeffs(i, alpha(i), beta(i));
        }
        //std::cout << "COEFFS: alpha: " << alpha(0) << " Beta: " << beta(0) << std::endl;
        int Nmax = 20;
        const double eps = 1e-8;
        double X0_norm = 1e10, F_norm = 1e10;
        double relax = 1;
        for (int i = 0; X0_norm > eps || F_norm > eps; i++) {
            if (i > Nmax) {
                if (X0_norm < 1e-6 && F_norm < 1e-5)
                    break;//fine
                if (relax > 0.00001) {
                    relax -= 0.1;
                    i = 0;
                    Nmax+=20;
                    continue;
                }
                if (X0_norm > 1e-6 || F_norm > 1e-4) {
                    std::cout << "Bern pump Newton failed: "  << X0_norm  << " " << F_norm << std::endl;
                    throw 0;
                } else {
                    break;
                }
            }

            double Qf = 0;
            for (unsigned j = 0; j < K; j++) {
                Qf += X(j) * (alpha(j) * X(j) + beta(j));
            }
            if (is_first_time) {
                Q_prev = Qf;
                is_first_time = 0;
            }
            //fill F
            const double Common = LV_pressure + pump_eq(Qf) + std::pow(Qf/S_p, 2)/2;
            for (unsigned j = 0; j < K; j++) {
                Edge *e = static_cast<Edge *>(edges[j]);
                F(j) = e->Equation_of_state_optimized(X(j)) + std::pow(alpha(j) * X(j) + beta(j), 2) / 2 - Common;
            }
            F_norm = F.template lpNorm<Eigen::Infinity>();

            //std::cout << "iter: " << i << "F_norm: " << F_norm << std::endl;

            //now fill Yac
            const double Com2 = pump_eq_deriv(Qf) + Qf/pow(S_p, 2);
            for (unsigned j = 0; j < K; j++) {
                const double phi = Com2 * (2 * alpha(j) * X(j) + beta(j));
                Edge *e = static_cast<Edge *>(edges[j]);
                for (unsigned m = 0; m < K; m++) {
                    if (m == j) {
                        Yac(m, j) = alpha(j) * (alpha(j) * X(j) + beta(j)) + e->Equation_of_state_deriv(X(j)) - phi;
                    } else {
                        Yac(m, j) = -phi;
                    }
                }
            }
            //std::cout << "DET: " << Yac.determinant() << std::endl;
            X0 = -Yac.inverse()*F;
            X0_norm = X0.template lpNorm<Eigen::Infinity>();
            X += relax*X0;
            //if (relax < 1)
              //  std::cout << "iter: " << i <<" relax: " << relax << " x: " << X0_norm << " res: " << F_norm << std::endl;
        }

        double Q = 0;//pump flow
        for (unsigned j = 0; j < K; j++) {
            Q += X(j) * (alpha(j) * X(j) + beta(j));
        }

        for (unsigned i = 0; i < K; i++) {
            //find U from S
            Edge *e = static_cast<Edge *>(edges[i]);
            const double U = alpha(i)*X(i) + beta(i);
            e->set_V_s(this, 0, X(i));
            e->set_V_u(this, 0, U);
        }
        H_prev = pump_eq(Q);
        if (Q > 0)
            is_real_flow = 1;
        else
            is_real_flow = 0;
#ifndef NDEBUG
        const double P = LV_pressure + H_prev; //pressure at the end of pump
        std::cout << "pump: w: " << w << " P: " << P << " Q: " << Q << " Xnorm: " << X0_norm << " Fnorm: " << F_norm << std::endl;
#endif
        Q_prev = Q;
        w_prev = w;
    }
};
*/


template<typename Edge,
         template<typename, int, int ...> typename Matrix, typename Scalar, auto Dynamic>
class Simple_heart:
    public Heart<Edge, Matrix, Scalar, Dynamic>
{
    typedef Heart<Edge, Matrix, Scalar, Dynamic> Base;
    using Base::OutgoingCompatibilityCoeffs;
    using Base::T;
    using Base::dt;
    using Base::setSU;

    Edge *e;
    Simple_vertex * sv;
    double Tc;
    double stroke_volume;
    double Q;
    void FlowToSU(double Qi, double & S, double & U)
    {
        double alfa, beta;
        OutgoingCompatibilityCoeffs(e, sv, alfa, beta);
        U = ( beta + std::sqrt(beta * beta + 4.0 * alfa * Qi) ) / 2;
        S = (-beta + std::sqrt(beta * beta + 4.0 * alfa * Qi) ) / (2 * alfa);
    }
public:
    Simple_heart(const std::string & id, Edge * e, Simple_vertex * sv, double period, double stroke_volume)
    : Base(id), e(e), sv(sv), Tc(period), stroke_volume(stroke_volume)
    {}
    double get_flow_av()
    {
        return Q;
    }
private:
    void update()
    {
        double S, U;
        Q = stroke_volume / 58.75845 * (1.0/Tc) * 285 * (0.20617 + 0.37759*sin(2*M_PI*T/Tc + 0.59605) +
              0.2804*sin(4*M_PI*T/Tc - 0.35859) +  0.15337*sin(6*M_PI*T/Tc - 1.2509) -
              0.049889*sin(8*M_PI*T/Tc + 1.3921) +  0.038107*sin(10*M_PI*T/Tc - 1.1068) -
              0.041699*sin(12*M_PI*T/Tc + 1.3985) -  0.020754*sin(14*M_PI*T/Tc + 0.72921) +
              0.013367*sin(16*M_PI*T/Tc - 1.5394) -   0.021983*sin(18*M_PI*T/Tc + 0.95617) -
              0.013072*sin(20*M_PI*T/Tc - 0.022417) +   0.0037028*sin(22*M_PI*T/Tc - 1.4146) -
              0.013973*sin(24*M_PI*T/Tc + 0.77416) -   0.012423*sin(26*M_PI*T/Tc - 0.46511) +
              0.0040098*sin(28*M_PI*T/Tc + 0.95145) -   0.0059704*sin(30*M_PI*T/Tc + 0.86369) -
              0.0073439*sin(32*M_PI*T/Tc - 0.64769) +   0.0037006*sin(34*M_PI*T/Tc + 0.74663) -
              0.0032069*sin(36*M_PI*T/Tc + 0.85926) -   0.0048171*sin(38*M_PI*T/Tc - 1.0306) +
              0.0040403*sin(40*M_PI*T/Tc + 0.28009) -   0.0032409*sin(42*M_PI*T/Tc + 1.202) -
              0.0032517*sin(44*M_PI*T/Tc - 0.93316) +   0.0029112*sin(46*M_PI*T/Tc + 0.21405) -
              0.0022708*sin(48*M_PI*T/Tc + 1.1869) -  0.0021566*sin(50*M_PI*T/Tc - 1.1574) +
              0.0025511*sin(52*M_PI*T/Tc - 0.12915) -   0.0024448*sin(54*M_PI*T/Tc + 1.1185) -
              0.0019032*sin(56*M_PI*T/Tc - 0.99244) +   0.0019476*sin(58*M_PI*T/Tc - 0.059885) -
              0.0019477*sin(60*M_PI*T/Tc + 1.1655) -   0.0014545*sin(62*M_PI*T/Tc - 0.85829) +
              0.0013979*sin(64*M_PI*T/Tc + 0.042912) -   0.0014305*sin(66*M_PI*T/Tc + 1.2439) -
              0.0010775*sin(68*M_PI*T/Tc - 0.79464) +   0.0010368*sin(70*M_PI*T/Tc - 0.0043058) -
              0.0012162*sin(72*M_PI*T/Tc + 1.211) -   0.00095707*sin(74*M_PI*T/Tc - 0.66203) +
              0.00077733*sin(76*M_PI*T/Tc + 0.25642) -   0.00092407*sin(78*M_PI*T/Tc + 1.3954) -
              0.00079585*sin(80*M_PI*T/Tc - 0.49973));

        FlowToSU(Q, S, U);
        setSU(e, sv, S, U);
    }
};



template<typename Edge,
         template<typename, int, int ...> typename Matrix, typename Scalar, auto Dynamic>
class Constant_flow_heart:
    public Heart<Edge, Matrix, Scalar, Dynamic>
{
    typedef Heart<Edge, Matrix, Scalar, Dynamic> Base;
    using Base::OutgoingCompatibilityCoeffs;
    using Base::T;
    using Base::dt;
    using Base::setSU;

    Edge *e;
    Simple_vertex * sv;
    double flowrate;
    void FlowToSU(double Q, double & S, double & U)
    {
        double alfa, beta;
        OutgoingCompatibilityCoeffs(e, sv, alfa, beta);
        U = ( beta + std::sqrt(beta * beta + 4.0 * alfa * Q) ) / 2;
        S = (-beta + std::sqrt(beta * beta + 4.0 * alfa * Q) ) / (2 * alfa);
    }
public:
    Constant_flow_heart(const std::string & id, Edge * e, Simple_vertex * sv, double flowrate)
    : Base(id), e(e), sv(sv), flowrate(flowrate)
    {}

    double get_flow_av()
    {
        return flowrate;
    }
protected:
    void update()
    {
        double S, U;
        FlowToSU(flowrate, S, U);
        setSU(e, sv, S, U);
    }
};


/* need to fix its compatibility condition coefficients to respect right hand side
template<typename Edge,
         template<typename, int, int ...> typename Matrix, typename Scalar, auto Dynamic>
class Free_flow_vertex:
    public Vertex<Edge, Matrix, Scalar, Dynamic>
{
    typedef Vertex<Edge, Matrix, Scalar, Dynamic> Base;
    using typename Base::VectorN;
    using typename Base::MatrixNN;
    using Base::OutgoingCompatibilityCoeffs;
    using Base::dt;
    using Base::id;
    using Base::OCNFO2;

    Edge *e;
    Simple_vertex * sv;

    void UnreflectingConditions(double & Sq, double & U)
    {
	    double P, Ps, sound, sigm, a1, b1, a2, b2;

	    VectorN	V, S, FPR;
	    VectorN OC, OF, OFF, NC, NF;
        MatrixNN SL, SP, OM, OMO;

        OCNFO2(e, sv, OC, OF, OFF);

    	V = OC;

        e->Equation_of_state(V, sound, P, Ps);
	    e->Eigenvalues(V, S, SL, SP, sound, P);
        e->OMEGAB(V, OM, OMO, sound, P);
        FPR = e->get_FPRCH(sv, V, 0);
        const double dx = e->get_dx();
	    
    	sigm = S(1) * dt / dx;
		
	    a1 = - OM(1,0) / OM(1,1);

	    //!b = (OC(2)+sigm*NF(2)-a*(OC(1)+sigm*NF(1))+FPR(2)*dt-a*FPR(1)*dt)/(1+sigm)
	
	    //!2nd order explicit
	    b1 = (1.0-3.0*sigm/2)*(-a1*OC(0) + OC(1)) +
             2.0*sigm*(-a1*OF(0) + OF(1)) - 0.5*sigm*(-a1*OFF(0) + OFF(1));
	
	    sigm = S(0) * dt / dx;

	    a2 = - OM(0,0) / OM(0,1);

        //!b = (a*(sigm*NF(1)-OC(1))+OC(2)-sigm*NF(2)+FPR(2)*dt-a*FPR(1 )*dt)/(1-sigm)
    
        //!2nd order explicit
        b2 = (1.0+3.0*sigm/2)*(-a2*OC(0) + OC(1)) -
             2.0*sigm*(-a2*OF(0) + OF(1)) + 0.5*sigm*(-a2*OFF(0) + OFF(1));

        Sq = -(b2 - b1)/(a2 - a1);
        U =  (b2*a1 - b1*a2)/(a1-a2);
    }

public:
    Free_flow_vertex(const std::string & id, Edge * e, Simple_vertex * sv)
    : Base(id), e(e), sv(sv)
    {}
    void update_boundary()
    {
        double S, U;
        UnreflectingConditions(S, U);
        e->set_V_s(sv, 0, S);
        e->set_V_u(sv, 0, U);
#ifndef NDEBUG
       // std::cout << id << ": " << S << " " << U << " " << S*U << std::endl;
#endif
    }
};
*/


//A bit more advanced heart model
//old version code
/*
template<typename MatrixDD, typename VectorD, typename MatrixND,
         typename SqMatrixNN, typename VectorN, int N>
class A_heart:
    public Vertex<MatrixDD, VectorD, MatrixND, SqMatrixNN, VectorN, N>
{
    typedef Vertex<MatrixDD, VectorD, MatrixND, SqMatrixNN, VectorN, N> Base;
    using Base::OutgoingCompatibilityCoeffs;
    using Base::T;
    using typename Base::Edge;
    using Base::edges;
    using Base::dt;

    const double cc = 1333.22;

    const double delta = 1;//1 -- healthy heart, 0.25 -- failure
    const double Emax = cc*2, Emin = cc*0.06;//looks fine
    double E()
    {
        const double tc = 1;
        const double Tmax = 0.2+0.15*tc;
        const double tn = std::fmod(T, 1.0)/Tmax;

        const double Ee = 1.55*std::pow(tn/0.7, 1.9)/(1 + std::pow(tn/0.7, 1.9)) / (1 + std::pow(tn/1.17, 21.9));
        return ((Emax - Emin)*delta*Ee + Emin);//orig: delta*((Emax - Emin)*Ee + Emin);
    }


    const double V0 = 10;//test it, 10 for normal, 70 failure
    double R_av = cc*0.001;

    const int vn = 4; //pressure in lv, volume of lv, S, U
    VectorD X;
    MatrixDD Yac;
    double V_lv_prev;
    double Qfill()
    {//TODO
        //return 80;//just testing constant filling, 80 for normal is fine
        const double tn = std::fmod(T, 1.0);
        const double Rmv = 0.005*cc;
        double lap = 5;
        if (tn < 0.5)
            lap += 30*tn;
        lap*=cc;//dont forget
        const double Pdiff = lap - X(0);
        std::cout << "Qfill: " << Pdiff/Rmv << std::endl;
        if (Pdiff > 0)
            return Pdiff/Rmv;// > 200 ? 200:Pdiff/Rmv ;
        else
            return 0;
    }
public:
    A_heart(const std::string & id)
    : Base(id), X(vn), Yac(vn, vn)
    {
        X(0) = 5*cc;
        V_lv_prev = X(1) = V0 + X(0)/E();
        X(2) = M_PI;
        X(3) = 0;
    }
    void update_boundary()
    {
        V_lv_prev = X(1);
        VectorD X0(vn);
        VectorD F(vn);
        double alfa, beta;
        //начало ветви
        OutgoingCompatibilityCoeffs(0, alfa, beta);
        double cE = E();
        double cQfill = Qfill();
        //set const elements of Yac
        Yac.setZero();
        Yac(0, 0) = 1;
        Yac(0, 1) = - cE;
        Yac(2, 1) = 1;
        Yac(3, 2) = - alfa;
        Yac(3, 3) = 1;

        Edge *e = static_cast<Edge *>(edges[0]);

        int Nmax = 20;
        const double eps = 1e-8;
        double X0_norm = 1e10, F_norm = 1e10;
        double relax = 1;
        for (int i = 0; X0_norm > eps || F_norm > eps; i++) {
            if (i > Nmax) {
                if (X0_norm < 1e-6 && F_norm < 1e-5)
                    break;//fine
                if (relax > 0.00001) {
                    relax -= 0.1;
                    i = 0;
                    Nmax+=20;
                    continue;
                }
                if (X0_norm > 1e-6 || F_norm > 1e-4) {
                    std::cout << "A_heart Newton failed: "  << X0_norm  << " " << F_norm << std::endl;
                    throw 0;
                } else {
                    break;
                }
            }

            double & P_lv = X(0);
            double & V_lv = X(1);
            double & S_av = X(2);
            double & U_av = X(3);

            //fill F
            F(0) = P_lv - cE * (V_lv - V0);
            const double P_av = e->Equation_of_state_optimized(S_av);
            if (P_lv >= P_av) {
                F(1) = R_av*S_av*U_av - P_lv + P_av;
            } else {
                F(1) = R_av*S_av*U_av;
            }
            F(2) = V_lv - V_lv_prev - dt*(cQfill - S_av*U_av);
            F(3) = U_av - alfa*S_av - beta;
            F_norm = F.template lpNorm<Eigen::Infinity>();

            //std::cout << "iter: " << i << "F_norm: " << F_norm << std::endl;

            //now complete Yac
            if (P_lv >= P_av) {
                Yac(1, 0) = -1;
                Yac(1, 2) = R_av*U_av + e->Equation_of_state_deriv(S_av);
            } else {
                Yac(1, 0) = 0;
                Yac(1, 2) = R_av*U_av;
            }
            Yac(1, 3) = R_av*S_av;
            Yac(2, 2) = dt*U_av;
            Yac(2, 3) = dt*S_av;
            //std::cout << "DET: " << Yac.determinant() << std::endl;

            X0 = -Yac.inverse()*F;
            X0_norm = X0.template lpNorm<Eigen::Infinity>();
            X += relax*X0;
            //if (relax < 1)
              //  std::cout << "iter: " << i <<" relax: " << relax << " x: " << X0_norm << " res: " << F_norm << std::endl;
        }


        //now just send info
        const double S = X(2);
        const double U = X(3);
        e->set_V_s(this, 0, S);
        e->set_V_u(this, 0, U);
        std::cout << X(0)/cc  << " " << X(1)  << " " << S*U << std::endl;
#ifndef NDEBUG
        std::cout << this->id << ": " << S << " " << U  << " " << " " << S*U << std::endl;
#endif
    }
};



template<typename Edge, typename MatrixDD, typename VectorD, typename MatrixND,
         typename SqMatrixNN, typename VectorN, int N>
class A_heart:
    public Vertex<Edge, MatrixDD, VectorD, MatrixND, SqMatrixNN, VectorN, N>
{
    typedef Vertex<Edge, MatrixDD, VectorD, MatrixND, SqMatrixNN, VectorN, N> Base;
    using Base::OutgoingCompatibilityCoeffs;
    using Base::T;
    using Base::dt;
    using Base::id;

    Edge * e;
    Simple_vertex * sv;
    const double cc = 1333.22;

    const double delta = 1;//1 -- healthy heart, 0.25 -- failure
    const double V0 = 10;//test it, 10 for normal, 70 failure

    const double Emax = cc*2, Emin = cc*0.06;//looks fine
    double E()
    {
        const double tc = 1;
        const double Tmax = 0.2+0.15*tc;
        const double tn = std::fmod(T, 1.0)/Tmax;

        const double Ee = 1.55*std::pow(tn/0.7, 1.9)/(1 + std::pow(tn/0.7, 1.9)) / (1 + std::pow(tn/1.17, 21.9));
        return ((Emax - Emin)*delta*Ee + Emin);
        //return delta*((Emax - Emin)*Ee + Emin);
    }


    double R_av = cc*0.001;

    const int vn = 4; //pressure in lv, volume of lv, S, U
    VectorD X;
    MatrixDD Yac;
    double V_lv_prev;
    const double Rmv = 0.005*cc;
    double LAP()
    {//TODO
        //return 80;//just testing constant filling, 80 for normal is fine
        const double tn = std::fmod(T, 1.0);
        double lap = 5;
        if (tn < 0.5)
            lap += 30*tn;
        lap*=cc;//dont forget
        return lap;
    }
public:
    A_heart(const std::string & id, Edge * e, Simple_vertex * sv)
    : Base(id), e(e), sv(sv), X(vn), Yac(vn, vn)
    {
        X(0) = 5*cc;
        V_lv_prev = X(1) = V0 + X(0)/E();
        X(2) = M_PI;
        X(3) = 0;
    }
    void update_boundary()
    {
        V_lv_prev = X(1);
        VectorD X0(vn);
        VectorD F(vn);
        double alfa, beta;
        //начало ветви
        OutgoingCompatibilityCoeffs(e, sv, alfa, beta);
        double cE = E();
        double lap = LAP();
        //set const elements of Yac
        Yac.setZero();
        Yac(0, 0) = 1;
        Yac(0, 1) = - cE;
        Yac(2, 1) = 1;
        Yac(3, 2) = - alfa;
        Yac(3, 3) = 1;

        int Nmax = 20;
        const double eps = 1e-8;
        double X0_norm = 1e10, F_norm = 1e10;
        double relax = 1;
        for (int i = 0; X0_norm > eps || F_norm > eps; i++) {
            if (i > Nmax) {
                if (X0_norm < 1e-6 && F_norm < 1e-5)
                    break;//fine
                if (relax > 0.00001) {
                    relax -= 0.1;
                    i = 0;
                    Nmax+=20;
                    continue;
                }
                if (X0_norm > 1e-6 || F_norm > 1e-4) {
                    std::cout << "A_heart Newton failed: "  << X0_norm  << " " << F_norm << std::endl;
                    throw 0;
                } else {
                    break;
                }
            }

            double & P_lv = X(0);
            double & V_lv = X(1);
            double & S_av = X(2);
            double & U_av = X(3);

            //fill F
            F(0) = P_lv - cE * (V_lv - V0);
            const double P_av = e->Equation_of_state_optimized(S_av);
            if (P_lv >= P_av) {
                F(1) = R_av*S_av*U_av - P_lv + P_av;
            } else {
                F(1) = R_av*S_av*U_av;
            }
            if (lap >= P_lv)
                F(2) = V_lv - V_lv_prev - dt*((lap - P_lv)/Rmv - S_av*U_av);
            else
                F(2) = V_lv - V_lv_prev - dt*(0 - S_av*U_av);


            F(3) = U_av - alfa*S_av - beta;
            F_norm = F.template lpNorm<Eigen::Infinity>();

            //std::cout << "iter: " << i << "F_norm: " << F_norm << std::endl;

            //now complete Yac
            if (P_lv >= P_av) {
                Yac(1, 0) = -1;
                Yac(1, 2) = R_av*U_av + e->Equation_of_state_deriv(S_av);
            } else {
                Yac(1, 0) = 0;
                Yac(1, 2) = R_av*U_av;
            }
            Yac(1, 3) = R_av*S_av;

            if (lap >= P_lv)
                Yac(2, 0) = dt/Rmv;
            else
                Yac(2, 0) = 0;

            Yac(2, 2) = dt*U_av;
            Yac(2, 3) = dt*S_av;
            //std::cout << "DET: " << Yac.determinant() << std::endl;

            X0 = -Yac.inverse()*F;
            X0_norm = X0.template lpNorm<Eigen::Infinity>();
            X += relax*X0;
            //if (relax < 1)
              //  std::cout << "iter: " << i <<" relax: " << relax << " x: " << X0_norm << " res: " << F_norm << std::endl;
        }


        //now just send info
        const double S = X(2);
        const double U = X(3);
        e->set_V_s(sv, 0, S);
        e->set_V_u(sv, 0, U);
        //std::cout <<  X(1)  << " " << X(0)/cc << std::endl;
#ifndef NDEBUG
        //std::cout << id << ": " << S << " " << U  << " " << " " << S*U << std::endl;
#endif
    }
};


//Metavertex -- heart + lvad
template<typename Edge, typename MatrixDD, typename VectorD, typename MatrixND,
         typename SqMatrixNN, typename VectorN, int N>
class HeartPlusPumpMeta:
    public Vertex<Edge, MatrixDD, VectorD, MatrixND, SqMatrixNN, VectorN, N>
{
    typedef Vertex<Edge, MatrixDD, VectorD, MatrixND, SqMatrixNN, VectorN, N> Base;
    using Base::OutgoingCompatibilityCoeffs;
    using Base::get_pressure;
    using Base::get_eos_deriv;
    using Base::setSU;
    using Base::T;
    using Base::id;
    using Base::dt;

    const double cc = 1333.22;

    Edge * edge1; //from valve to the pump
    Edge * edge2; //from the pump to the end
    Simple_vertex * heart_v;
    Simple_vertex * pump_connect_v;

    const double delta = 0.25;//1 -- healthy heart, 0.25 -- failure
    const double V0 = 70;//test it, 10 for normal, 70 failure
    const double Emax = cc*2, Emin = cc*0.06;//looks fine

    double E()
    {
        const double tc = 1;
        const double Tmax = 0.2+0.15*tc;
        const double tn = std::fmod(T, 1.0)/Tmax;

        const double Ee = 1.55*std::pow(tn/0.7, 1.9)/(1 + std::pow(tn/0.7, 1.9)) / (1 + std::pow(tn/1.17, 21.9));
        return ((Emax - Emin)*delta*Ee + Emin);//orig: delta*((Emax - Emin)*Ee + Emin);
    }


    double R_av = cc*0.001;

    const int vn = 8; //P(lv), P(aort), U(pump), V(lv), S(av),
                      //u(av), S1, S2
    VectorD X;
    MatrixDD Yac;
    double V_lv_prev;
    double u_pump_prev;
    const double Rmv = 0.005*cc;
    double LAP()
    {//TODO
        //return 80;//just testing constant filling, 80 for normal is fine
        const double tn = std::fmod(T, 1.0);
        double lap = 5;
        if (tn < 0.5)
            lap += 30*tn;
        lap*=cc;//dont forget
        return lap;
    }

//pump stuff
    bool is_first_time;
    const double S_p = M_PI/4;//test it! pump area
    const bool is_real_flow = 0; //dont need it
    const double e1 = -8.5e-1 * cc * 0.0036;
    const double e2 = -7.3e-4 * cc * 0.06;
    const double e3 = 7.5e-7 * cc;
    const double e4 = -9.89e+1 * cc * 6e-4  +  15*cc/1000 ; //plus periphial
    const double e = 1.174422e-4 * 1000/60;
    const double f = 6.62775 * cc * 0.0036;
    const double Rper = 2*e-1 * cc/278; //plus periphial
    double w;



    //pump 2
    const double b1 = -1.15e+1 * cc * 0.06;
    const double b2 = 8e-7 * cc;
    const double b3 = -1.055e+2 * cc * 6e-4;

    double pump_eq(const double & Q) const
    {//TODO add perif resistance
        //pump1
        //return a1*Q*Q + a2*w*Q + a3*w*w + a4*(Q - Q_prev)/dt
        //        + a5*(w - w_prev)/dt;


        const double Q_prev = S_p*u_pump_prev;

        //pump 2
        //return b1*Q + b2*w*w + b3*(Q - Q_prev)/dt;

        //pump 3
        //return d1*Q*Q + d2*w*Q + d3*w*w + d4*(Q - Q_prev)/dt;

        //pump 4
        const double bf = Q - e*w;
        return e1*Q*Q + e2*w*Q + e3*w*w + e4*(Q - Q_prev)/dt + (bf < 0)*f*bf*bf
                - Rper*Q*Q*(Q > 0? 1 : -1);
    }
    double pump_eq_deriv(const double & Q) const
    {//TODO add perif resistance
        //pump1
        //return 2*a1*Q + a2*w + a4/dt;

        //pump 2
        //return b1 + b3/dt;
        //pump 3
        //return 2*d1*Q + d2*w + d4/dt;
        //pump 4
        const double bf = Q - e*w;
        return 2*e1*Q + e2*w + e4/dt + (bf < 0)*f*2*bf
                - 2*Rper*Q*(Q > 0? 1 : -1);

    }


//end pump stuff

public:
    bool get_is_real_flow() const
    {
        return is_real_flow;
    }
    void set_w(double ww)
    {
        w = ww;
    }
    HeartPlusPumpMeta(const std::string & id, Edge * edge1, Edge * edge2, Simple_vertex * heart, Simple_vertex * pump_connect)
    : Base(id), edge1(edge1), edge2(edge2), heart_v(heart), pump_connect_v(pump_connect), X(vn), Yac(vn, vn)
    {
        X(0) = 5*cc;
        X(1) = 5*cc;
        X(2) = 0;
        V_lv_prev = X(3) = V0 + X(0)/E();
        X(4) = M_PI;
        X(5) = 0;
        X(7) = X(6) = 4.9;
        u_pump_prev = 0;
        w = 0;
        is_first_time = 1;
    }
    ~HeartPlusPumpMeta()
    {}

    void update_boundary()
    {
        //lets try to control w!!!
        //calm control

//        const double tn = std::fmod(T+0.1, 1.0);
//        if (tn < 0.1 || tn > 0.45)
//            w = 10000;
//        else if (tn > 0.1 && tn < 0.2)
//            w = 10000 + 3000*(tn-0.1)/0.1;
//        else if (tn > 0.2 && tn < 0.3)
//            w = 13000;
//        else //tn is between 0.3 and 0.45
//            w = 13000 - 3000*(tn-0.3)/0.15;



//        const double tn = std::fmod(T, 1.0);
//        const double t0 = 0.001, ts = 0.1, t1 = 0.35;
//        if (tn > t1 || T < 1)
//            w = 7000;
//        else if (tn < t0)
//            w = 7000 + 9000*tn/t0;
//        else if (tn > t0 && tn < ts)
//            w = 16000;
//        else //tn is between ts and t1
//            w = 16000 - 9000*(tn-ts)/(t1-ts);


         //superfast


//        const double tn = std::fmod(T, 1.0);
//        if (T < 1)
//            w = 8000;
//        else if (tn > 0.15)
//            w = 8000;
//        else
//           w = 26000 - 18000*tn/0.15;


        const double & P_lv = X(0);
        const double & P_aorta = X(1);
        const double & u_pump = X(2);
        const double & V_lv = X(3);
        const double & S_av = X(4);
        const double & u_av = X(5);
        const double & s1 = X(6);
        const double & s2 = X(7);

        V_lv_prev = V_lv;
        u_pump_prev = u_pump;

        VectorD X0(vn);
        VectorD F(vn);

        double alfa_av, beta_av;
        double alfa1, beta1, alfa2, beta2;
        OutgoingCompatibilityCoeffs(edge1, heart_v, alfa_av, beta_av);
        OutgoingCompatibilityCoeffs(edge1, pump_connect_v, alfa1, beta1);
        OutgoingCompatibilityCoeffs(edge2, pump_connect_v, alfa2, beta2);

        if (is_first_time) {
            is_first_time = 0;
            u_pump_prev = (4.9*(alfa1*4.9 + beta1)
                         + 4.9*(alfa2*4.9 + beta2)) / S_p;
        }
        const double cE = E();
        const double lap = LAP();
        //set const elements of Yac
        Yac.setZero();
        Yac(0, 0) = 1;
        Yac(0, 1) = -1;

        Yac(1,0) = 1;
        Yac(1, 3) = - cE;

        Yac(3, 4) = -alfa_av;
        Yac(3, 5) = 1;

        Yac(4, 2) = dt*S_p;
        Yac(4, 3) = 1;

        Yac(5, 2) = S_p;

        Yac(6, 1) = -1;
        Yac(7, 1) = -1;


//        Edge *e = static_cast<Edge *>(edges[0]);

        int Nmax = 20;
        const double eps = 1e-6;
        double X0_norm = 1e10, F_norm = 1e10;
        double relax = 1;
        for (int i = 0; X0_norm > eps || F_norm > eps; i++) {
            if (i > Nmax) {
                if (X0_norm < 1e-6 && F_norm < 1e-5)
                    break;//fine
                if (relax > 0.00001) {
                    relax -= 0.1;
                    i = 0;
                    Nmax+=20;
                    continue;
                }
                if (X0_norm > 1e-6 || F_norm > 1e-4) {
                    std::cout << "HeartPump Newton failed: "  << X0_norm  << " " << F_norm << std::endl;
                    throw 0;
                } else {
                    std::cout << "Nearly failed" << std::endl;
                    break;
                }
            }



            //fill F
            F(0) = P_lv - P_aorta + pump_eq(u_pump*S_p);
            F(1) = P_lv - cE * (V_lv - V0);

            const double P_av = get_pressure(edge1, S_av);
            if (P_lv >= P_av) {
                F(2) = R_av*S_av*u_av - P_lv + P_av;
            } else {
                F(2) = R_av*S_av*u_av;
            }
            F(3) = u_av - alfa_av*S_av - beta_av;

            if (lap >= P_lv)
                F(4) = V_lv - V_lv_prev - dt*((lap - P_lv)/Rmv - S_av*u_av - S_p*u_pump);
            else
                F(4) = V_lv - V_lv_prev - dt*(0 - S_av*u_av - S_p*u_pump);


            const double u1 = alfa1 * s1 + beta1;
            const double u2 = alfa2 * s2 + beta2;
            F(5) = S_p*u_pump - s1*u1 - s2*u2;

            const double P1 = get_pressure(edge1, s1);
            const double P2 = get_pressure(edge2, s2);
            F(6) = P1 + u1*u1/2 - P_aorta - u_pump*u_pump/2;
            F(7) = P2 + u2*u2/2 - P_aorta - u_pump*u_pump/2;
            //F complete

            F_norm = F.template lpNorm<Eigen::Infinity>();
            //std::cout << "iter: " << i << "F_norm: " << F_norm << std::endl;

            //now complete Yac
            Yac(0, 2) = S_p*pump_eq_deriv(S_p*u_pump);

            if (P_lv >= P_av) {
                Yac(2, 0) = -1;
                Yac(2, 4) = R_av*u_av + get_eos_deriv(edge1, S_av);
            } else {
                Yac(2, 0) = 0;
                Yac(2, 4) = R_av*u_av;
            }
            Yac(2, 5) = R_av*S_av;



            if (lap >= P_lv)
                Yac(4, 0) = dt/Rmv;
            else
                Yac(4, 0) = 0;


            Yac(4, 4) = dt*u_av;
            Yac(4, 5) = dt*S_av;

            Yac(5, 6) = -2*alfa1*s1 - beta1;
            Yac(5, 7) = -2*alfa2*s2 - beta2;

            Yac(6, 2) = - u_pump;
            Yac(6, 6) = get_eos_deriv(edge1, s1) + alfa1*u1;

            Yac(7, 2) = - u_pump;
            Yac(7, 7) = get_eos_deriv(edge2, s2) + alfa2*u2;


            //std::cout << "DET: " << Yac.determinant() << std::endl;
            //std::cout << "Yac.frob: " << Yac.norm() << std::endl;

            X0 = -Yac.inverse()*F;
            X0_norm = X0.template lpNorm<Eigen::Infinity>();
            X += relax*X0;
           // if (relax < 1)
           //     std::cout << "iter: " << i <<" relax: " << relax << " x: " << X0_norm << " res: " << F_norm << std::endl;
        }


        //now just send info
        setSU(edge1, heart_v, S_av, u_av);
        setSU(edge1, pump_connect_v, s1, alfa1*s1+beta1);
        setSU(edge2, pump_connect_v, s2, alfa2*s2+beta2);
        //std::cout << X << std::endl << std::endl;
        //std::cout << u_pump*S_p << " " << s1*(alfa1*s1+beta1) << " " << s2*(alfa2*s2+beta2) << std::endl;
       // std::cout << "P_lv: " << X(0)/cc  << " V_lv: " << X(3)  << " Q_p: " << S_p*u_pump << std::endl;
       // std::cout << X(3) << " " << X(0)/cc << std::endl;
#ifndef NDEBUG
     //   std::cout << id << ": " << S << " " << U  << " " << " " << S*U << std::endl;
#endif
    }
};
*/

#endif
