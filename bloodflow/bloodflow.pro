#-------------------------------------------------
#
# Project created by QtCreator 2020-02-29T14:47:17
#
#-------------------------------------------------

QT       -= core gui

TARGET = bloodflow
TEMPLATE = lib

DEFINES += BLOODFLOW_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += \
        lib/ \
        ext/

unix:!macx {
    INCLUDEPATH += \
        /usr/include/eigen3
    QMAKE_CXXFLAGS_RELEASE += -march=native
}

macx: {
    INCLUDEPATH += \
        /opt/homebrew/Cellar/eigen/3.3.9/include/eigen3/
    LIBS += -L /opt/homebrew/lib
}

CONFIG += c++17

QMAKE_LFLAGS += -Xpreprocessor -fopenmp -lomp
QMAKE_CXXFLAGS += -Wall -Xpreprocessor -fopenmp

macx: {
    QMAKE_CXXFLAGS += -I/opt/homebrew/include
}

QMAKE_CXXFLAGS_RELEASE += -O3

SOURCES += \
        src/calculator.cpp \
        src/edge.cpp \
        src/graph.cpp \
        src/heart_part.cpp \
        src/pump_part.cpp \
        src/task.cpp \
        src/vertex.cpp

HEADERS += \
        ext/json.hpp \
        lib/calculator.h \
        lib/edge.h \
        lib/graph.h \
        lib/hav_pump.h \
        lib/heart_advanced_valves.h \
        lib/heart_part.h \
        lib/heart_twocompvalves.h \
        lib/htcnpump.h \
        lib/pump_part.h \
        lib/rcrwindkessel.h \
        lib/task.h \
        lib/vertex.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
