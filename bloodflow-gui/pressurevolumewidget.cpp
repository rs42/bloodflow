#include "pressurevolumewidget.h"

PressureVolumeWidget::PressureVolumeWidget(QWidget *parent)
    : QChartView(parent)
{
    m_taskWrapper = nullptr;
    m_pv_pos = 0;

    m_LV_pv_data.resize(pv_sz);
    m_LA_pv_data.resize(pv_sz);

    pv_chart.set_x_axis(50, 300);
    pv_chart.set_y_axis(0, 150);
    pv_chart.set_x_axis_title("Volume, ml");
    pv_chart.set_y_axis_title("Pressure, mmhg");
    setChart(&pv_chart);
    //setRenderHint(QPainter::HighQualityAntialiasing);//QPainter::Antialiasing);
    setRenderHint(QPainter::Antialiasing);
    setRubberBand(QChartView::RectangleRubberBand);
}
void PressureVolumeWidget::initialize(TaskGUIWrapper *p_taskWrapper)
{
    m_taskWrapper = p_taskWrapper;
}

void PressureVolumeWidget::update()
{
    //QReadLocker locker(m_taskWrapper->p_lock);

    double lv_p, lv_v, la_p, la_v;
    m_taskWrapper->p_task->get_LV_pv(lv_p, lv_v);
    m_taskWrapper->p_task->get_LA_pv(la_p, la_v);
    //std::cout << p /1333.22  << " " << v  << std::endl;
    lastLV_PVpoint = QPointF(lv_v, lv_p);
    lastLA_PVpoint = QPointF(la_v, la_p);
    m_LA_pv_data[m_pv_pos] = lastLA_PVpoint;
    m_LV_pv_data[m_pv_pos] = lastLV_PVpoint;//QPointF(q*0.06, h/1333.22); // L/min mmHg
    m_pv_pos = (m_pv_pos+1)%m_LA_pv_data.size();


    pv_chart.update_chart(get_LV_pv(), get_lastLV_PVpoint(),
                          get_LA_pv(), get_lastLA_PVpoint());
}

QVector<QPointF> & PressureVolumeWidget::get_LV_pv()
{
    return m_LV_pv_data;
}

QVector<QPointF> & PressureVolumeWidget::get_LA_pv()
{
    return m_LA_pv_data;
}

QPointF & PressureVolumeWidget::get_lastLV_PVpoint()
{
    return lastLV_PVpoint;
}

QPointF & PressureVolumeWidget::get_lastLA_PVpoint()
{
    return lastLA_PVpoint;
}
