#ifndef TASKGUIWRAPPER_H
#define TASKGUIWRAPPER_H

#include <QTime>
#include <QVector>
#include <QPointF>
#include <QString>
#include <QReadWriteLock>


#include "task.h"
#include "graph.h"

class TaskGUIWrapper
{


public:
    QReadWriteLock * p_lock;

    TaskGUIWrapper();
    ~TaskGUIWrapper();
    std::vector<Simple_edge *> & get_edges();
    std::vector<Simple_vertex *> & get_vertices();

    void generate_report_afterload();
    void generate_report_windkessel_sv();
    void generate_report_edge_stats();
    void generate_report_speed();
    void generate_report_test_LV_ESPVR();
    void generate_report_test_pulm_vein_pressure();
    void generate_report_basic();
    QTime computationTime() const;
    //void setRealTime(const QTime &value);
    void run_step(double timer);
    void setPumpSpeed(int value);
   // int num() const;
   // void setNum(const int &num);

    QTime simTime() const;
    QVector<QPointF> & data(quantity vt);
    QPointF data(quantity vt, int pos);
    QPointF lv_p();
    QPointF la_p();



    void setup_data();
    double get_total_len();
    int get_total_sz();
    double getStrokeVol();
    double get_aortic_valve();
    double get_mitral_valve();
    Simple_edge * selected_edge;
    void change_selected_edge(Simple_edge * se);
public:
    void set_new_task(const QString & graph, const QString & add_heart = QString());
    QTime from_sec(double t) const;
    void sensitivityAnalysis();
    double total_len;
    QVector<QPointF> m_data;

    Task * p_task;
    Simple_edge * get_selected_edge();
};

#endif // TASKGUIWRAPPER_H
