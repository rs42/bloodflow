#ifndef PRESSUREVOLUMEWIDGET_H
#define PRESSUREVOLUMEWIDGET_H

#include <QChartView>
#include "taskguiwrapper.h"
#include "chart.h"

class PressureVolumeWidget
: public QChartView
{
    Q_OBJECT

public:
    PressureVolumeWidget(QWidget *parent = nullptr);
public slots:
    void initialize(TaskGUIWrapper * p_taskWrapper);
    void update();
private:
    Chart pv_chart;
    TaskGUIWrapper * m_taskWrapper;

    const int pv_sz = 300;


    QVector<QPointF> m_LV_pv_data;
    QVector<QPointF> m_LA_pv_data;

    QPointF lastLV_PVpoint;
    QPointF lastLA_PVpoint;

    int m_pv_pos;

    QVector<QPointF> & get_LV_pv();
    QVector<QPointF> & get_LA_pv();
    QPointF & get_lastLV_PVpoint();
    QPointF & get_lastLA_PVpoint();
};

#endif // PRESSUREVOLUMEWIDGET_H
