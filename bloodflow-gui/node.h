#ifndef NODE_H
#define NODE_H

#include <QGraphicsItem>
#include <QList>
#include <vector>
#include "graph.h"

class GraphWidget;
class GEdge;
QT_BEGIN_NAMESPACE
class QGraphicsSceneMouseEvent;
QT_END_NAMESPACE

class Node : public QGraphicsItem
{
public:
    Node(GraphWidget *graphWidget, Simple_vertex * sv);

    enum { Type = UserType + 1 };
    int type() const override { return Type; }

    QRectF boundingRect() const override;
    QPainterPath shape() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
    void add_gedge(GEdge * e);
protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;
    QVariant itemChange(GraphicsItemChange change, const QVariant &value) override;

private:
    GraphWidget *graph;
    Simple_vertex * sv;
    std::vector<GEdge*> gedges;
};

#endif // NODE_H
