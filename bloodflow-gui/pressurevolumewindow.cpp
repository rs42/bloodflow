#include "pressurevolumewindow.h"
#include "ui_pressurevolumewindow.h"

PressureVolumeWindow::PressureVolumeWindow(QWidget *parent) :
    QWidget(parent, Qt::Window),
    ui(new Ui::PressureVolumeWindow)
{
    ui->setupUi(this);
}

PressureVolumeWindow::~PressureVolumeWindow()
{
    delete ui;
}

PressureVolumeWidget *PressureVolumeWindow::get_pv_widget()
{
    return ui->pv_widget;
}
