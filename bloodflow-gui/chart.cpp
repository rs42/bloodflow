#include "chart.h"
#include <QtCharts/QSplineSeries>
#include <QtCharts/QLineSeries>
#include <QtCharts/QScatterSeries>
#include <QtCharts/QValueAxis>

#include <iostream>
Chart::Chart(QGraphicsItem *parent, Qt::WindowFlags wFlags)
    : QChart(QChart::ChartTypeCartesian, parent, wFlags),
      m_series(nullptr), scat_series(nullptr), series2(nullptr), scat2(nullptr)
{

    m_series = new QLineSeries(this);
    m_series->setUseOpenGL(true);
    QPen pen(Qt::red);
    pen.setWidth(3);
    m_series->setPen(pen);
    addSeries(m_series);


    scat_series = new QScatterSeries(this);
    scat_series->setUseOpenGL(true);
    scat_series->setMarkerShape(QScatterSeries::MarkerShapeCircle);
    scat_series->setMarkerSize(30.0);
    scat_series->setColor(Qt::red);
    addSeries(scat_series);

    x_num = 20;
    min_x = -3;
    max_x = 6;

    legend()->hide();
    axisX = new QValueAxis;
    axisX->setTickCount(x_num);
    axisX->setRange(min_x, max_x);
    axisX->setTitleText("x");
    addAxis(axisX, Qt::AlignBottom);

    axisY = new QValueAxis;
    addAxis(axisY, Qt::AlignLeft);
    m_series->attachAxis(axisX);
    m_series->attachAxis(axisY);
    scat_series->attachAxis(axisX);
    scat_series->attachAxis(axisY);
    axisY->setTickCount(21);


    series2 = new QLineSeries(this);
    series2->setUseOpenGL(true);
    QPen pen2(Qt::blue);
    pen2.setWidth(3);
    series2->setPen(pen2);
    addSeries(series2);
    series2->attachAxis(axisX);
    series2->attachAxis(axisY);

    scat2 = new QScatterSeries(this);
    scat2->setUseOpenGL(true);
    scat2->setMarkerShape(QScatterSeries::MarkerShapeCircle);
    scat2->setMarkerSize(30.0);
    scat2->setColor(Qt::blue);
    addSeries(scat2);
    scat2->attachAxis(axisX);
    scat2->attachAxis(axisY);


}

Chart::~Chart()
{
    delete scat_series;
    delete m_series;
    delete scat2;
    delete series2;
    delete axisX;
    delete axisY;

}
void Chart::set_x_axis(double len)
{
    min_x = 0;
    max_x = len;
    axisX->setRange(min_x, max_x);
}
void Chart::set_x_axis(double min, double max)
{
    min_x = min;
    max_x = max;
    axisX->setRange(min_x, max_x);
}

void Chart::set_y_axis(double min, double max)
{
    min_y = min;
    max_y = max;
    axisY->setRange(min_y, max_y);
}

void Chart::set_x_axis_title(const char *title)
{
    axisX->setTitleText(title);
}

void Chart::set_y_axis_title(const char *title)
{
    axisY->setTitleText(title);
}
void Chart::update_chart(QVector<QPointF> & points)
{
    m_series->replace(points);
}
void Chart::update_chart(QVector<QPointF> & points, QPointF & tip)
{
    m_series->replace(points);
    if (scat_series->points().size() == 0)
        scat_series->append(tip);
    else
        scat_series->replace(0, tip);
}

void Chart::update_chart(QVector<QPointF> & points, QPointF & tip, QVector<QPointF> & points2, QPointF & tip2)
{
    m_series->replace(points);
    if (scat_series->points().size() == 0)
        scat_series->append(tip);
    else
        scat_series->replace(0, tip);

    series2->replace(points2);
    if (scat2->points().size() == 0)
        scat2->append(tip2);
    else
        scat2->replace(0, tip2);
}


ChartDynamic::ChartDynamic(QGraphicsItem *parent, Qt::WindowFlags wFlags):
    QChart(QChart::ChartTypeCartesian, parent, wFlags),
    m_series(0), series2(0), series3(0)
{
    setAnimationOptions(QChart::SeriesAnimations);
    m_series = new /*QSplineSeries*/QLineSeries(this);
    m_series->setUseOpenGL(true);
    //setAnimationOptions(QChart::AllAnimations);
    QPen pen1(Qt::red);
    pen1.setWidth(3);
    m_series->setPen(pen1);

    legend()->hide();
    axisX = new QValueAxis;
    axisX->setTickCount(10);
    axisX->setRange(-1.8, 0.2);
    axisX->setTitleText("t");
    addAxis(axisX, Qt::AlignBottom);

    addSeries(m_series);
    m_series->append(0,0);


    axisY = new QValueAxis;
    addAxis(axisY, Qt::AlignLeft);
    m_series->attachAxis(axisX);
    m_series->attachAxis(axisY);
    axisY->setTickCount(21);

    axisY->setRange(0, 140);


    series2 = new /*QSplineSeries*/QLineSeries(this);
    series2->setUseOpenGL(true);
    QPen pen2(Qt::blue);
    pen2.setWidth(3);
    series2->setPen(pen2);
    addSeries(series2);
    series2->attachAxis(axisX);
    series2->attachAxis(axisY);

    series3 = new /*QSplineSeries*/QLineSeries(this);
    series3->setUseOpenGL(true);
    QPen pen3(Qt::green);
    pen3.setWidth(3);
    series2->setPen(pen3);
    addSeries(series3);
    series3->attachAxis(axisX);
    series3->attachAxis(axisY);


}

ChartDynamic::~ChartDynamic()
{
    delete m_series;
    delete series2;
    delete series3;
    delete axisX;
    delete axisY;
}

void ChartDynamic::reset_chart()
{
    m_series->clear();
    m_series->append(0,0);
    series2->clear();
    series3->clear();
    axisX->setRange(-1.8, 0.2);

}

void ChartDynamic::update_chart(const QPointF & p)
{
    const double st = p.x() - m_series->at(m_series->count() - 1).x();
    //assert(st);
    scroll(st*plotArea().width()/2, 0);
    //std::cout << st*plotArea().width()/2 << std::endl;
    m_series->append(p);
    //axisX->setRange(-0.8+p.x(), 0.2+p.x());
}

void ChartDynamic::update_chart(const QPointF & p, const QPointF & s2, const QPointF & s3)
{
    const double st = p.x() - m_series->at(m_series->count() - 1).x();
    //assert(st);
    scroll(st*plotArea().width()/2, 0);
    //std::cout << st*plotArea().width()/2 << std::endl;
    m_series->append(p);
    //axisX->setRange(-0.8+p.x(), 0.2+p.x());
    series2->append(s2);
    series3->append(s3);
}
