#include "taskguiwrapper.h"
#include <iostream>
#include <QVector>
#include <QString>


TaskGUIWrapper::TaskGUIWrapper()
{
    p_lock = new QReadWriteLock();//QReadWriteLock::Recursive);
    p_task = nullptr;

}

TaskGUIWrapper::~TaskGUIWrapper()
{
    delete p_lock;
    if (p_task)
        delete p_task;
}


std::vector<Simple_edge *> &TaskGUIWrapper::get_edges()
{
    assert(p_task);
    return p_task->s_edges;
}

std::vector<Simple_vertex *> &TaskGUIWrapper::get_vertices()
{
    assert(p_task);
    return p_task->s_vertices;
}

void TaskGUIWrapper::generate_report_afterload()
{
    QWriteLocker locker(p_lock);
    assert(p_task);
    p_task->generate_report_test_afterload();
}

void TaskGUIWrapper::generate_report_windkessel_sv()
{
    QWriteLocker locker(p_lock);
    assert(p_task);
    p_task->generate_report_wk_flows();
}


void TaskGUIWrapper::generate_report_speed()
{
    QWriteLocker locker(p_lock);
    assert(p_task);
    p_task->generate_report_speed();
}

void TaskGUIWrapper::generate_report_edge_stats()
{
    QWriteLocker locker(p_lock);
    assert(p_task);
    p_task->generate_report_edge_stats();
}
void TaskGUIWrapper::generate_report_test_LV_ESPVR()
{
    QWriteLocker locker(p_lock);
    assert(p_task);
    p_task->generate_report_test_LV_ESPVR();
}
void TaskGUIWrapper::generate_report_test_pulm_vein_pressure()
{
    QWriteLocker locker(p_lock);
    assert(p_task);
    p_task->generate_report_test_pulm_vein_pressure();
}
void TaskGUIWrapper::generate_report_basic()
{
    QWriteLocker locker(p_lock);
    assert(p_task);
    p_task->generate_report_basic();
}

QTime TaskGUIWrapper::computationTime() const
{
    return from_sec(p_task->get_computationTime());
    //return  from_sec(double(real_ms)/1000);
}





double TaskGUIWrapper::getStrokeVol()
{//return stroke volume in ml
    return p_task->get_strokeVolume();
}

double TaskGUIWrapper::get_aortic_valve()
{
    return p_task->get_aortic_valve();
}

double TaskGUIWrapper::get_mitral_valve()
{
    return p_task->get_mitral_valve();
}

void TaskGUIWrapper::change_selected_edge(Simple_edge *se)
{
    selected_edge = se;
    setup_data();
}

void TaskGUIWrapper::set_new_task(const QString & graph, const QString & add_heart)
{
    QWriteLocker locker(p_lock);

    Task * pN_task = new Task;
    try {
        if (add_heart.isEmpty())
            pN_task->set_up(graph.toStdString());
        else
            pN_task->set_up(graph.toStdString(), add_heart.toStdString());
    } catch (const std::exception & exc) {
        delete pN_task;
        throw;
    }

    if (p_task)
        delete p_task;
    p_task = pN_task;
    selected_edge = get_edges()[0];
    setup_data();
}

void TaskGUIWrapper::run_step(double timer)
{
    QWriteLocker locker(p_lock);

    p_task->run_step(timer);
}

void TaskGUIWrapper::setPumpSpeed(int value)
{
    QWriteLocker locker(p_lock);

    p_task->set_pump_speed(value);
}


QTime TaskGUIWrapper::simTime() const
{
    return from_sec(p_task->get_simTime());
}

void TaskGUIWrapper::setup_data()
{
    total_len = 0;
    int total_sz = 0;
    double len;
    const int sz = p_task->get_edge_data_size(selected_edge->get_id().c_str(), len);
    total_len+=len;
    total_sz += sz;

    if (m_data.size() != total_sz)
        m_data.resize(total_sz);
}

double TaskGUIWrapper::get_total_len()
{
    return total_len;
}

int TaskGUIWrapper::get_total_sz()
{
    return m_data.size();
}

QVector<QPointF> &TaskGUIWrapper::data(quantity vt)
{
    double dx_add = 0;
    QVector<QPointF>::iterator iter = m_data.begin();

    iter = p_task->get_edge_data<QVector<QPointF>::iterator, QPointF>(selected_edge->get_id().c_str(), vt, iter, m_data.end(), dx_add);
        dx_add = (iter-1)->x();
    return m_data;
}

QPointF TaskGUIWrapper::data(quantity vt, int pos)
{
    return QPointF(p_task->get_simTime(), m_data[pos].ry());
}

QPointF TaskGUIWrapper::lv_p()
{
    double p, v;
    p_task->get_LV_pv(p, v);
    return QPointF(p_task->get_simTime(), p);
}

QPointF TaskGUIWrapper::la_p()
{
    double p, v;
    p_task->get_LA_pv(p, v);
    return QPointF(p_task->get_simTime(), p);
}




QTime TaskGUIWrapper::from_sec(double t) const
{
    return QTime(int(t/3600)%24, int(t/60)%60, int(t)%60, int(t*1000)%1000);
}

void TaskGUIWrapper::sensitivityAnalysis()
{
    QWriteLocker locker(p_lock);
    p_task->sensitivityAnalysis();
}

Simple_edge *TaskGUIWrapper::get_selected_edge()
{
    return selected_edge;
}
