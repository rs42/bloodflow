#include "edgelabel.h"

#include <QPainter>

EdgeLabel::EdgeLabel(QString label)
    : label(label)
{
    setZValue(1);
}

void EdgeLabel::set_position(QPointF new_source)
{
    source = new_source;
}

QRectF EdgeLabel::boundingRect() const
{
    int fontSize = 10;
    QFont font;
    //font.setBold(true);
    font.setPointSize(fontSize);

    QFontMetrics fm(font);
    int pixelsWide = fm.horizontalAdvance(label);
    int pixelsHigh = fm.height();

    QRectF textRect = QRectF(source, QSizeF(pixelsWide, pixelsHigh));
    return textRect;
}

void EdgeLabel::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    int fontSize = 10;
    QFont font = painter->font();
    //font.setBold(true);
    font.setPointSize(fontSize);

    QFontMetrics fm(font);
    int pixelsWide = fm.horizontalAdvance(label);
    int pixelsHigh = fm.height();

    QRectF textRect = QRectF(source, QSizeF(pixelsWide, pixelsHigh));

    painter->setPen(QPen(Qt::green, 1));
    painter->drawRect(textRect);
    painter->fillRect(textRect, QColor(Qt::lightGray).lighter(120));

    painter->setFont(font);
   // painter->setPen(Qt::lightGray);
   // painter->drawText(textRect.translated(2, 2), edge_name);
    painter->setPen(Qt::black);
    painter->drawText(textRect, label);
}
