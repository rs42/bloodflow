#ifndef WORKER_H
#define WORKER_H

#include <QObject>
#include <QElapsedTimer>
#include <QtCore/QTimer>
#include "taskguiwrapper.h"

class Worker : public QObject
{
    Q_OBJECT

    TaskGUIWrapper * taskwrapper;
public:
    explicit Worker(QObject *parent = nullptr);
    ~Worker();
signals:
    void read_access_signal();
public slots:
    void initialize(TaskGUIWrapper * p_taskwrapper);
    void initialize_in_thread();
    void run_step(double timer);
};

#endif // WORKER_H
