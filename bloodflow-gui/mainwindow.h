#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtCharts/QChart>

#include "taskguiwrapper.h"
#include "chart.h"
#include "worker.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
protected:
    void closeEvent(QCloseEvent *event) override;
public:
    explicit MainWindow(Worker * worker, QWidget *parent = nullptr);
    ~MainWindow() override;
signals:
    void call_worker(double timer);
    void newTaskGUIWrapper(TaskGUIWrapper * p_taskWrapper);
    void read_access_signal();
public slots:
    void update();

    void handleRunPauseButton();

    void handleReport_Afterload();
    void handleWindkessel_SV();
    void handleEdge_stats();
    void handleReport_speed();
    void handleReport_test_LV_ESPVR();
    void handleReport_test_pulm_vein_pressure();
    void handleReport_basic();
    void handleSensitivityAnalysis();

    void setArea(bool st);
    void setPressure(bool st);
    void setSpeed(bool st);
    void setFlow(bool st);
    void selected_edge(Simple_edge * se);
    void loadGraph();
    void loadGraphAndHeart();
    void setPumpSpeed(int value);
private:
    Worker * worker;
    quantity cur_value;

    Chart m_chart;
    ChartDynamic m_val_time_chart;

    enum State {running, stopped, report} state;

    Ui::MainWindow * ui;
    TaskGUIWrapper * m_taskWrapper;
    void setupConnections();
    void stop();
    void afterLoad();
};

#endif // MAINWINDOW_H
