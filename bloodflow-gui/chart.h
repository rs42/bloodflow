#ifndef CHART_H
#define CHART_H


#include <QtCharts/QChart>
/*
QT_CHARTS_BEGIN_NAMESPACE
class QSplineSeries;
class QValueAxis;
class QLineSeries;
class QScatterSeries;
QT_CHARTS_END_NAMESPACE

QT_CHARTS_USE_NAMESPACE
*/

#include <QtCharts/QLineSeries>
#include <QtCharts/QValueAxis>
#include <QtCharts/QScatterSeries>
#include <QtCharts/QSplineSeries>

class Chart : public QChart
{
    Q_OBJECT
public:
    Chart(QGraphicsItem *parent = nullptr, Qt::WindowFlags wFlags = Qt::WindowFlags());
    virtual ~Chart();
    void update_chart(QVector<QPointF> & points);
    void update_chart(QVector<QPointF> & points, QPointF & tip);
    void update_chart(QVector<QPointF> & points, QPointF & tip, QVector<QPointF> & points2, QPointF & tip2);

    void set_x_axis(double len);
    void set_x_axis(double min, double max);
    void set_y_axis(double min, double max);
    void set_x_axis_title(const char * title);
    void set_y_axis_title(const char * title);
private:
    QValueAxis *axisX;
    QValueAxis *axisY;
    QLineSeries /*QSplineSeries*/ *m_series;
    QScatterSeries *scat_series;
    qreal min_x, max_x, min_y, max_y;
    qreal x_step;
    int x_num;

    QLineSeries *series2;
    QScatterSeries *scat2;
};



class ChartDynamic: public QChart
{
    Q_OBJECT
public:
    ChartDynamic(QGraphicsItem *parent = nullptr, Qt::WindowFlags wFlags = Qt::WindowFlags());
    virtual ~ChartDynamic();

    void update_chart(const QPointF & point);
    void update_chart(const QPointF & p, const QPointF & s2, const QPointF & s3);

    void reset_chart();
//public slots:
  //  void handleTimeout();


private:
    /*QSplineSeries*/QLineSeries *m_series;
    QStringList m_titles;
    QValueAxis *axisX;
    QValueAxis *axisY;

    QLineSeries *series2;
    QLineSeries *series3;
};

#endif // CHART_H
