#ifndef GEDGE_H
#define GEDGE_H

#include <QGraphicsItem>

#include "graph.h"
#include "edgelabel.h"

class Node;

class GraphWidget;

class GEdge : public QGraphicsItem
{
public:
    GEdge(GraphWidget *graphWidget, Simple_edge *se, Node * n1, Node * n2, int is_selected);

    enum { Type = UserType + 2 };
    int type() const override { return Type; }
    QRectF boundingRect() const override;
    QPainterPath shape() const override;
    EdgeLabel * getLabel() const;

    QPolygonF get_shapePolygon() const;
    void unselect();
    void select();
    Simple_edge * se;
    void get_color_angle(Node *n, QColor & color, double & rad_angle);
    void adjust();
protected:
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
    void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event) override;
    void hoverEnterEvent(QGraphicsSceneHoverEvent *event) override;
    void hoverLeaveEvent(QGraphicsSceneHoverEvent *event) override;

private:


    Node * n1, * n2;
    int is_selected;
    QPointF sourcePoint;
    QPointF destPoint;
    double length;
    qreal nx, ny; //normal to edge
    qreal penWidth;
    EdgeLabel * label;
    QPolygonF shapePolygon;
    GraphWidget *graphWidget;
    QColor color_n1;
    QColor color_n2;
    double angle_n1;
    double angle_n2;
};


#endif // GEDGE_H
