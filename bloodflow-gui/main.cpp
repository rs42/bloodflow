#include "mainwindow.h"
#include "pressurevolumewindow.h"
#include "graphwindow.h"
#include "worker.h"
#include <QThread>
#include <QtWidgets/QApplication>
#include <QMessageBox>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QThread worker_thread;
    Worker * worker = new Worker();

    MainWindow main_window(worker);

    worker->moveToThread(&worker_thread);
    QObject::connect(&worker_thread, SIGNAL(started()), worker, SLOT(initialize_in_thread()));
    QObject::connect(&worker_thread, &QThread::finished, worker, &QObject::deleteLater);

    worker_thread.start();
    main_window.show();

    return a.exec();
}
