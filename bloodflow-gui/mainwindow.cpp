#include <QLCDNumber>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QSlider>
#include <QCheckBox>
#include <QReadWriteLock>
#include <QFileDialog>
#include <QMessageBox>
#include <QThread>

MainWindow::MainWindow(Worker * worker, QWidget *parent) :
    QMainWindow(parent),
    worker(worker),
    ui(new Ui::MainWindow)
{
    state = stopped;
    m_taskWrapper = new TaskGUIWrapper;
    ui->setupUi(this);

    setupConnections();
    cur_value = pressure;
    ui->chartview->setChart(&m_chart);
    //ui->chartview->setRenderHint(QPainter::HighQualityAntialiasing);//QPainter::Antialiasing);
    ui->chartview->setRenderHint(QPainter::Antialiasing);

    ui->chartview->setRubberBand(QChartView::VerticalRubberBand);
   // taskWrapper->setup_data({"aortic_arch_IV", "thoracic_aorta_I"});
   // taskWrapper->setup_data({"aortic_arch_I", "aortic_arch_II"});

    //taskWrapper->setup_data({"edge0", "edge1"});

    /*
    m_chart.set_x_axis(m_taskWrapper->get_total_len());


    ui->PointSlider->setMinimum(0);
    ui->PointSlider->setMaximum(m_taskWrapper->get_total_sz() - 1);
    */


    ui->TimeValueview->setChart(&m_val_time_chart);
    //ui->TimeValueview->setRenderHint(QPainter::HighQualityAntialiasing);//QPainter::Antialiasing);
    ui->TimeValueview->setRenderHint(QPainter::Antialiasing);

    ui->TimeValueview->setRubberBand(QChartView::VerticalRubberBand);
    //m_val_time_chart.setAnimationOptions(QChart::AllAnimations);

}

MainWindow::~MainWindow()
{
    delete ui;
    delete m_taskWrapper;
}
void MainWindow::closeEvent (QCloseEvent *event)
{
    event->accept();
    QThread * w_t = worker->thread();
    w_t->quit();
    w_t->wait();
}
void MainWindow::update()
{
    if (state != running || !isVisible())
        return;

    {

    QReadLocker locker(m_taskWrapper->p_lock);

    emit read_access_signal();
    m_chart.update_chart(m_taskWrapper->data(cur_value));
    if (cur_value != pressure)
        m_val_time_chart.update_chart(m_taskWrapper->data(cur_value, ui->PointSlider->value()));
    else
        m_val_time_chart.update_chart(m_taskWrapper->data(cur_value, ui->PointSlider->value()), m_taskWrapper->lv_p(), m_taskWrapper->la_p());
    ui->realtime->display(m_taskWrapper->computationTime().toString("mm:ss:zzz"));
    ui->simtime->display(m_taskWrapper->simTime().toString("mm:ss:zzz"));

    ui->StrokeVol->setText(QString::number(int(m_taskWrapper->getStrokeVol())));
    ui->aorticValve->setText(QString::number(int(m_taskWrapper->get_aortic_valve()), 'g', 2));
    ui->mitralValve->setText(QString::number(int(m_taskWrapper->get_mitral_valve()), 'g', 2));

    }
    emit call_worker(0.02);
}

void MainWindow::handleRunPauseButton()
{
    if (ui->runpause->isChecked()) {
        state = running;
        ui->runpause->setText("Pause");
        update();
    } else {
        stop();
    }
}

void MainWindow::handleReport_Afterload()
{
    stop();
    m_taskWrapper->generate_report_afterload();
}

void MainWindow::handleWindkessel_SV()
{
    stop();
    m_taskWrapper->generate_report_windkessel_sv();
}

void MainWindow::handleEdge_stats()
{
    stop();
    m_taskWrapper->generate_report_edge_stats();
}

void MainWindow::handleReport_speed()
{
    stop();
    m_taskWrapper->generate_report_speed();
}

void MainWindow::handleReport_basic()
{
    stop();
    m_taskWrapper->generate_report_basic();
}
void MainWindow::handleReport_test_LV_ESPVR()
{
    stop();
    m_taskWrapper->generate_report_test_LV_ESPVR();
}
void MainWindow::handleReport_test_pulm_vein_pressure()
{
    stop();
    m_taskWrapper->generate_report_test_pulm_vein_pressure();
}

void MainWindow::setArea(bool st)
{
    if (st) {
        cur_value = area;
        m_val_time_chart.reset_chart();
    }
}

void MainWindow::setPressure(bool st)
{
    if (st) {
        cur_value = pressure;
        m_val_time_chart.reset_chart();
    }
}

void MainWindow::setSpeed(bool st)
{
    if (st) {
        cur_value = speed;
        m_val_time_chart.reset_chart();
    }
}

void MainWindow::setFlow(bool st)
{
    if (st) {
        cur_value = flow;
        m_val_time_chart.reset_chart();
    }
}

void MainWindow::selected_edge(Simple_edge * se)
{
    m_taskWrapper->change_selected_edge(se);
    m_chart.set_x_axis(m_taskWrapper->get_total_len());
    ui->selected_edge->setText((se->get_id() + " selected").c_str());
    m_chart.set_x_axis(m_taskWrapper->get_total_len());

}

void MainWindow::stop()
{
    state = stopped;
    ui->runpause->setText("Run");
    ui->runpause->setChecked(false);
}
void MainWindow::afterLoad()
{
    ui->menuTests->setEnabled(true);
    ui->runpause->setEnabled(true);
    if (m_taskWrapper->p_task->has_pump())
        ui->speedslider->setEnabled(true);
    else
        ui->speedslider->setEnabled(false);

    ui->selected_edge->setText((m_taskWrapper->get_selected_edge()->get_id() + " selected").c_str());
    m_chart.set_x_axis(m_taskWrapper->get_total_len());

    m_val_time_chart.reset_chart();


    emit newTaskGUIWrapper(m_taskWrapper);
}
void MainWindow::loadGraph()
{
    stop();

    QString filename = QFileDialog::getOpenFileName(this,
        "Load graph", ".", "Graph File (*.json)");

    if (filename.isNull())
        return;

#ifndef NDEBUG
    std::cout << "Opening " << filename.toStdString() << std::endl;
#endif
    try {
        m_taskWrapper->set_new_task(filename);
    } catch (const std::exception & exc) {
        QMessageBox msgBox;
        QString msg = QString(filename) +
                      QString("\nIncorrect graph file.");
        msgBox.setText(msg);
        msgBox.setInformativeText(exc.what());
        msgBox.exec();
        return;
    }

    afterLoad();
}

void MainWindow::loadGraphAndHeart()
{
    stop();

    QString graph_filename = QFileDialog::getOpenFileName(this,
        "Load graph", ".", "Graph File (*.json)");

    if (graph_filename.isNull())
        return;

    QString heart_filename = QFileDialog::getOpenFileName(this,
        "Load heart", ".", "Graph File (*.json)");

    if (heart_filename.isNull())
        return;

#ifndef NDEBUG
    std::cout << "Opening " << graph_filename.toStdString() << std::endl;
    std::cout << "Opening " <<  heart_filename.toStdString() << std::endl;
#endif
    try {
        m_taskWrapper->set_new_task(graph_filename, heart_filename);
    } catch (const std::exception & exc) {
        QMessageBox msgBox;
        QString msg = QString(graph_filename) + QString("\n") + QString(heart_filename) +
                      QString("\nIncorrect graph files.");
        msgBox.setText(msg);
        msgBox.setInformativeText(exc.what());
        msgBox.exec();
        return;
    }


    afterLoad();
}

void MainWindow::handleSensitivityAnalysis()
{
    stop();
    m_taskWrapper->sensitivityAnalysis();
}

void MainWindow::setPumpSpeed(int value)
{
    m_taskWrapper->setPumpSpeed(value);
}

void MainWindow::setupConnections()
{
    QObject::connect(ui->speedslider, &QSlider::sliderMoved, ui->label, QOverload<int>::of(&QLabel::setNum));

    QObject::connect(this, &MainWindow::call_worker, worker, &Worker::run_step);
    QObject::connect(worker, &Worker::read_access_signal, this, &MainWindow::update);

    QObject::connect(ui->speedslider, &QSlider::sliderMoved, this, &MainWindow::setPumpSpeed);
    QObject::connect(this, &MainWindow::newTaskGUIWrapper, worker, &Worker::initialize);


    QObject::connect(ui->runpause, &QPushButton::clicked, this, &MainWindow::handleRunPauseButton);


    QObject::connect(ui->Speed, &QRadioButton::toggled, this, &MainWindow::setSpeed);
    QObject::connect(ui->Flow, &QRadioButton::toggled, this, &MainWindow::setFlow);
    QObject::connect(ui->Pressure, &QRadioButton::toggled, this, &MainWindow::setPressure);
    QObject::connect(ui->Area, &QRadioButton::toggled, this, &MainWindow::setArea);

    QObject::connect(ui->actionLoad_graph, &QAction::triggered, this, &MainWindow::loadGraph);
    QObject::connect(ui->actionLoad_graph_and_heart_from_separate_files, &QAction::triggered, this, &MainWindow::loadGraphAndHeart);


    QObject::connect(ui->actionTest_afterload, &QAction::triggered, this, &MainWindow::handleReport_Afterload);
    QObject::connect(ui->actionWindkessel_sv, &QAction::triggered, this, &MainWindow::handleWindkessel_SV);
    QObject::connect(ui->actionEdge_stats, &QAction::triggered, this, &MainWindow::handleEdge_stats);
    QObject::connect(ui->actionSensitivity_Analysis, &QAction::triggered, this, &MainWindow::handleSensitivityAnalysis);
    QObject::connect(ui->actionTest_speed, &QAction::triggered, this, &MainWindow::handleReport_speed);
    QObject::connect(ui->actionTest_basics, &QAction::triggered, this, &MainWindow::handleReport_basic);
    QObject::connect(ui->actionTest_LV_ESPVR, &QAction::triggered, this, &MainWindow::handleReport_test_LV_ESPVR);
    QObject::connect(ui->actionTest_PV_Pressure, &QAction::triggered, this, &MainWindow::handleReport_test_pulm_vein_pressure);


    QObject::connect(this, &MainWindow::read_access_signal, ui->pvDiag, &PressureVolumeWidget::update);
    QObject::connect(this, &MainWindow::newTaskGUIWrapper, ui->pvDiag, &PressureVolumeWidget::initialize);

    QObject::connect(this, &MainWindow::read_access_signal, ui->graphWidget, &GraphWidget::update);
    QObject::connect(ui->graphWidget, &GraphWidget::selected_edge, this, &MainWindow::selected_edge);
    QObject::connect(this, &MainWindow::newTaskGUIWrapper, ui->graphWidget, &GraphWidget::initialize);
}
