#include "graphwidget.h"
#include "gedge.h"
#include "node.h"

#include <cmath>

#include <QKeyEvent>
#include <QOpenGLWidget>

GraphWidget::GraphWidget( QWidget *parent)
    : QGraphicsView(parent)
{
    m_taskguiwrapper = nullptr;

    m_scene = new QGraphicsScene(this);
    //m_scene->setItemIndexMethod(QGraphicsScene::NoIndex);
    //m_scene->setSceneRect(-200, -200, 400, 400);
    setScene(m_scene);
    //setCacheMode(CacheBackground);
    //setViewportUpdateMode(BoundingRectViewportUpdate);
   // setRenderHint(QPainter::Antialiasing);
    setTransformationAnchor(AnchorUnderMouse);
    scale(qreal(0.8), qreal(0.8));
    setMinimumSize(400, 400);
    setWindowTitle("Vascular Network");
    //setRenderHint(QPainter::HighQualityAntialiasing);
    setRenderHint(QPainter::Antialiasing);
}

GraphWidget::~GraphWidget()
{
    delete m_scene;
}

void GraphWidget::select_me(GEdge *se)
{
    if (se != current_selected_edge) {
        current_selected_edge->unselect();
        current_selected_edge = se;
        emit selected_edge(se->se);
    }
}

void GraphWidget::set(Simple_edge * selected_edge, std::vector<Simple_edge*> & s_edges, std::vector<Simple_vertex*> & s_vertices)
{
    m_scene->clear();

    std::unordered_map<Simple_vertex*, Node*> s_vert_map;

    for (auto & sv: s_vertices) {
        Node *node = new Node(this, sv);
        node->setPos(sv->get_x(), sv->get_y());
        m_scene->addItem(node);
        s_vert_map[sv] = node;
    }
    for (auto & e: s_edges) {
        Node *n1 = s_vert_map.at(e->get_first_vertex());
        Node *n2 = s_vert_map.at(e->get_last_vertex());

        GEdge *edge = new GEdge(this, e, n1, n2, 0);
        n1->add_gedge(edge);
        n2->add_gedge(edge);
        m_scene->addItem(edge);
        m_scene->addItem(edge->getLabel());
        if (e == selected_edge) {
            current_selected_edge = edge;
            edge->select();
        }
    }
    QRectF rect = m_scene->itemsBoundingRect();
    m_scene->setSceneRect(rect.adjusted(-4, -4, 4, 4));
}

void GraphWidget::keyPressEvent(QKeyEvent *event)
{
    switch (event->key()) {
    case Qt::Key_Plus:
        zoomIn();
        break;
    case Qt::Key_Minus:
        zoomOut();
        break;
    default:
        QGraphicsView::keyPressEvent(event);
    }
}


#if QT_CONFIG(wheelevent)

void GraphWidget::wheelEvent(QWheelEvent *event)
{
    //TODO
    //scaleView(std::pow(2.0, -event->pixelDelta() / 240.0));
}

#endif


void GraphWidget::drawBackground(QPainter *painter, const QRectF &rect)
{
    Q_UNUSED(rect)

    // Shadow
    QRectF sceneRect = this->sceneRect();
    QRectF rightShadow(sceneRect.right(), sceneRect.top() + 5, 5, sceneRect.height());
    QRectF bottomShadow(sceneRect.left() + 5, sceneRect.bottom(), sceneRect.width(), 5);
    if (rightShadow.intersects(rect) || rightShadow.contains(rect))
        painter->fillRect(rightShadow, Qt::darkGray);
    if (bottomShadow.intersects(rect) || bottomShadow.contains(rect))
        painter->fillRect(bottomShadow, Qt::darkGray);

    // Fill
    QLinearGradient gradient(sceneRect.topLeft(), sceneRect.bottomRight());
    gradient.setColorAt(0, Qt::white);
    gradient.setColorAt(1, Qt::lightGray);
    painter->fillRect(rect.intersected(sceneRect), gradient);
    painter->setBrush(Qt::NoBrush);
    painter->drawRect(sceneRect);

    // Text
    QRectF textRect(sceneRect.right() - 20, sceneRect.top() + 4,
                    sceneRect.width() - 4, sceneRect.height() - 4);
    QString message(tr("L"));

    QFont font = painter->font();
    font.setBold(true);
    font.setPointSize(20);
    painter->setFont(font);
    painter->setPen(Qt::lightGray);
    painter->drawText(textRect.translated(2, 2), message);
    painter->setPen(Qt::black);
    painter->drawText(textRect, message);
}



void GraphWidget::scaleView(qreal scaleFactor)
{
    qreal factor = transform().scale(scaleFactor, scaleFactor).mapRect(QRectF(0, 0, 1, 1)).width();
    if (factor < 0.07 || factor > 100)
        return;

    scale(scaleFactor, scaleFactor);
}

void GraphWidget::zoomIn()
{
    scaleView(qreal(1.2));
}

void GraphWidget::zoomOut()
{
    scaleView(1 / qreal(1.2));
}
void GraphWidget::initialize(TaskGUIWrapper *taskguiwrapper)
{
    m_taskguiwrapper = taskguiwrapper;
    set(m_taskguiwrapper->selected_edge, m_taskguiwrapper->get_edges(), m_taskguiwrapper->get_vertices());
}

void GraphWidget::update()
{
    //QReadLocker locker(m_taskguiwrapper->p_lock);
    scene()->update();
}
