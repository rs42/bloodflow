#include "gedge.h"
#include "node.h"
#include "graphwidget.h"

#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QPainter>
#include <QStyleOption>


Node::Node(GraphWidget *graphWidget, Simple_vertex * sv)
    : graph(graphWidget), sv(sv)
{
    setFlag(ItemIsMovable);
    setFlag(ItemSendsGeometryChanges);
    //setCacheMode(DeviceCoordinateCache);

    setZValue(1);
}


QRectF Node::boundingRect() const
{
    qreal adjust = 2;
    return QRectF( -10 - adjust, -10 - adjust, 23 + adjust, 23 + adjust);
}


QPainterPath Node::shape() const
{
    QPainterPath path;
    path.addEllipse(-10, -10, 20, 20);
    return path;
}


void Node::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *)
{
    /*
    painter->setPen(Qt::NoPen);
    painter->setBrush(Qt::darkGray);
    painter->drawEllipse(-7, -7, 20, 20);

    QRadialGradient gradient(-3, -3, 10);
    if (option->state & QStyle::State_Sunken) {
        gradient.setCenter(3, 3);
        gradient.setFocalPoint(3, 3);
        gradient.setColorAt(1, QColor(Qt::red).lighter(120));
        gradient.setColorAt(0, QColor(Qt::darkRed).lighter(120));
    } else {
        gradient.setColorAt(0, Qt::red);
        gradient.setColorAt(1, Qt::darkRed);
    }
    */
    QColor main_color;
    double main_rad_angle = 0;
    gedges[0]->get_color_angle(this, main_color, main_rad_angle);
    QConicalGradient conicalGrad(0, 0, main_rad_angle / M_PI * 180);
    conicalGrad.setColorAt(1, main_color);
    conicalGrad.setColorAt(0, main_color);
    for (auto &e: gedges) {
        QColor color;
        double rad_angle;
        e->get_color_angle(this, color, rad_angle);
        double coord = (rad_angle - main_rad_angle) / (2 * M_PI);
        if (coord < 0)
            coord = 1 + coord;
        conicalGrad.setColorAt(coord, color);
    }
    painter->setBrush(conicalGrad);

    painter->setPen(Qt::NoPen);
    //painter->setPen(QPen(Qt::black, 0));
    painter->drawEllipse(-10, -10, 20, 20);
}

void Node::add_gedge(GEdge *e)
{
    gedges.push_back(e);
}

void Node::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    update();
    QGraphicsItem::mousePressEvent(event);
}

void Node::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    update();
    QGraphicsItem::mouseReleaseEvent(event);
}
QVariant Node::itemChange(GraphicsItemChange change, const QVariant &value)
{
    switch (change) {
    case ItemPositionHasChanged:
        for (auto &edge: gedges)
            edge->adjust();
        graph->update();
        std::cout << sv->get_id() << " x: "  << pos().x() << " y: " << pos().y() << std::endl;
        break;
    default:
        break;
    }

    return QGraphicsItem::itemChange(change, value);
}
