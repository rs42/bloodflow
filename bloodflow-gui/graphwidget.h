#ifndef GRAPHWIDGET_H
#define GRAPHWIDGET_H

#include <QGraphicsView>
#include "graph.h"
#include "taskguiwrapper.h"

class Node;
class GEdge;

class GraphWidget : public QGraphicsView
{
    Q_OBJECT

public:
    GraphWidget(QWidget *parent = nullptr);
    void set(Simple_edge * selected_edge, std::vector<Simple_edge*> & s_edges, std::vector<Simple_vertex*> & s_vertices);
    ~GraphWidget() override;
    void select_me(GEdge * se);
public slots:
    void zoomIn();
    void zoomOut();
    void initialize(TaskGUIWrapper * taskguiwrapper);
    void update();
signals:
    void selected_edge(Simple_edge * se);
protected:
    void keyPressEvent(QKeyEvent *event) override;
#if QT_CONFIG(wheelevent)
    void wheelEvent(QWheelEvent *event) override;
#endif
    void drawBackground(QPainter *painter, const QRectF &rect) override;

    void scaleView(qreal scaleFactor);
private:
    TaskGUIWrapper * m_taskguiwrapper;
    QGraphicsScene * m_scene;
    GEdge * current_selected_edge;
};

#endif // GRAPHWIDGET_H
