#include "worker.h"

Worker::Worker(QObject *parent)
    :  QObject(parent), taskwrapper(nullptr)
{
}

Worker::~Worker()
{
}

void Worker::initialize(TaskGUIWrapper * p_taskwrapper)
{
    taskwrapper = p_taskwrapper;
}

void Worker::initialize_in_thread()
{
}

void Worker::run_step(double timer)
{
    taskwrapper->run_step(timer);
    emit read_access_signal();
}
