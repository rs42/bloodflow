#ifndef EDGELABEL_H
#define EDGELABEL_H

#include <QGraphicsItem>


class EdgeLabel : public QGraphicsItem
{
    QPointF source;
    QString label;

public:
    EdgeLabel(QString label);
    void set_position(QPointF source);
    enum { Type = UserType + 3};
    int type() const override { return Type; }
    QRectF boundingRect() const override;
protected:
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
};

#endif // EDGELABEL_H
