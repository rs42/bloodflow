#ifndef PRESSUREVOLUMEWINDOW_H
#define PRESSUREVOLUMEWINDOW_H

#include <QWidget>
#include <pressurevolumewidget.h>

namespace Ui {
class PressureVolumeWindow;
}

class PressureVolumeWindow : public QWidget
{
    Q_OBJECT

public:
    explicit PressureVolumeWindow(QWidget *parent = nullptr);
    ~PressureVolumeWindow();
    PressureVolumeWidget * get_pv_widget();
private:
    Ui::PressureVolumeWindow *ui;

};

#endif // PRESSUREVOLUMEWINDOW_H
