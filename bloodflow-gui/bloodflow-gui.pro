#-------------------------------------------------
#
# Project created by QtCreator 2020-02-29T16:08:13
#
#-------------------------------------------------

QT       += core gui charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = BloodFlow-GUI
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS


# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++17

macx: {
    INCLUDEPATH += \
        /opt/homebrew/Cellar/eigen/3.3.9/include/eigen3/
}

unix:!macx {
    INCLUDEPATH += \
            /usr/include/eigen3
    QMAKE_CXXFLAGS_RELEASE += -march=native
}

SOURCES += \
        chart.cpp \
        edgelabel.cpp \
        gedge.cpp \
        graphwidget.cpp \
        graphwindow.cpp \
        main.cpp \
        mainwindow.cpp \
        node.cpp \
        pressurevolumewidget.cpp \
        pressurevolumewindow.cpp \
        taskguiwrapper.cpp \
        worker.cpp


HEADERS += \
    chart.h \
    edgelabel.h \
    gedge.h \
    graphwidget.h \
    graphwindow.h \
    mainwindow.h \
    node.h \
    pressurevolumewidget.h \
    pressurevolumewindow.h \
    taskguiwrapper.h \
    worker.h


FORMS += \
    graphwindow.ui \
    mainwindow.ui \
    pressurevolumewindow.ui

QMAKE_CXXFLAGS += -Wall -Xpreprocessor -fopenmp

macx: {
    QMAKE_CXXFLAGS += -I/opt/homebrew/include
}

QMAKE_CXXFLAGS_RELEASE += -O3

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../build-bloodflow-Desktop-Release/ -lbloodflow
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../build-bloodflow-Desktop-Debug/ -lbloodflow
else:macx: CONFIG(release, debug|release): LIBS += -L$$PWD/../build-bloodflow-Qt_6_2_0_for_macOS-Release/ -lbloodflow
else:macx:CONFIG(debug, debug|release): LIBS += -L$$PWD/../build-bloodflow-Qt_6_2_0_for_macOS-Debug/ -lbloodflow
else:unix: CONFIG(release, debug|release): LIBS += -L$$PWD/../build-bloodflow-Desktop-Release/ -lbloodflow
else:unix:CONFIG(debug, debug|release): LIBS += -L$$PWD/../build-bloodflow-Desktop-Debug/ -lbloodflow

INCLUDEPATH += $$PWD/../bloodflow/lib
DEPENDPATH += $$PWD/../bloodflow/lib
