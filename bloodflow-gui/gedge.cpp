#include <iostream>
#include "gedge.h"
#include "node.h"
#include "graphwidget.h"
#include <qmath.h>
#include <QPainter>


GEdge::GEdge(GraphWidget *graphWidget, Simple_edge *se, Node * n1, Node * n2, int is_selected)
    : se(se), n1(n1), n2(n2), is_selected(is_selected), graphWidget(graphWidget)
{
    //setAcceptedMouseButtons(Qt::MouseButton);
    setAcceptHoverEvents(true);

    penWidth = 4;
    label = new EdgeLabel(se->get_id().c_str());
    label->setVisible(false);
    adjust();
}

QRectF GEdge::boundingRect() const
{
    if (!n1 || !n2)
        return QRectF();

    qreal extra = penWidth / 2.0;

    return QRectF(sourcePoint, QSizeF(destPoint.x() - sourcePoint.x(),
                                      destPoint.y() - sourcePoint.y()))
        .normalized()
        .adjusted(-extra, extra, extra, -extra);
}

QPainterPath GEdge::shape() const
{
    QPainterPath path;
    path.addPolygon(get_shapePolygon());
    return path;
}

EdgeLabel *GEdge::getLabel() const
{
    return label;
}

QPolygonF GEdge::get_shapePolygon() const
{
    return shapePolygon;
}

void GEdge::unselect()
{
    is_selected = 0;
    update();
}

void GEdge::select()
{
    is_selected = 1;
    update();
}

void GEdge::get_color_angle(Node *n, QColor & color, double & rad_angle)
{
    if (n == n1) {
        color = color_n1;
        rad_angle = angle_n1;
    } else if (n == n2) {
        color = color_n2;
        rad_angle = angle_n2;
    } else {
        assert(0);
    }
}

void GEdge::adjust()
{
    QLineF line(mapFromItem(n1, 0, 0), mapFromItem(n2, 0, 0));
    length = line.length();

    sourcePoint = line.p1();
    destPoint = line.p2();

    qreal a = destPoint.x() - sourcePoint.x();
    qreal b = destPoint.y() - sourcePoint.y();
    nx = b / sqrt(a*a + b*b);
    ny = -a / sqrt(a*a + b*b);

    angle_n1 = atan2(b, a);
    if (angle_n1 < 0)
        angle_n1 = 2*M_PI + angle_n1;
    angle_n1 = 2*M_PI - angle_n1;

    angle_n2 = atan2(-b, -a);
    if (angle_n2 < 0)
        angle_n2 = 2*M_PI + angle_n2;
    angle_n2 = 2*M_PI - angle_n2;

    qreal extra = penWidth / 2.0;

    QVector<QPointF> points;
    points.push_back(QPointF(sourcePoint.x() + extra*nx, sourcePoint.y() + extra*ny));
    points.push_back(QPointF(destPoint.x() + extra*nx, destPoint.y() + extra*ny));
    points.push_back(QPointF(destPoint.x() - extra*nx, destPoint.y() - extra*ny));
    points.push_back(QPointF(sourcePoint.x() - extra*nx, sourcePoint.y() - extra*ny));
    points.push_back(QPointF(sourcePoint.x() + extra*nx, sourcePoint.y() + extra*ny));
    shapePolygon = QPolygonF(points);

    label->set_position((sourcePoint + destPoint)/2);
}


void GEdge::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    if (!n1 || !n2)
        return;
    if (isUnderMouse()) {
        QLineF line(sourcePoint, destPoint);
        painter->setPen(QPen(Qt::green, penWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
        setZValue(1);
        painter->drawLine(line);
        return;
    }
    setZValue(0);

    int size = se->get_points_num();
    if (size < 2) size = 2;
    const double dx = (destPoint.x() - sourcePoint.x())/(size - 1);
    const double dy = (destPoint.y() - sourcePoint.y())/(size - 1);

    for (int i = 0; i < size - 1; i++) {
        QLineF line(sourcePoint.x() + i*dx,     sourcePoint.y() + i*dy,
                    sourcePoint.x() + (i+1)*dx, sourcePoint.y() + (i+1)*dy);


        QLinearGradient gradient;
        //int h1 = int(se->get_P_unsafe(i)*2/1333.22);
        //int h2 = int(se->get_P_unsafe(i+1)*2/1333.22);
        int h1 = int(40*log(fabs(se->get_Q_unsafe(i) ) + 1));
        int h2 = int(40*log(fabs(se->get_Q_unsafe((i+1))) + 1));
       // if (h1 < 0 || h2 < 0)
       //     std::cout << "TOO SMALL: " << h1 << " " << h2 << std::endl;
      //  if (h1 > 359 || h2 > 359)
       //     std::cout << "TOO BIG" << std::endl;
        QColor color1, color2;
        color1.setHsv(h1, 255, 255);
        color2.setHsv(h2, 255, 255);
        if (i == 0)
            color_n1 = color1;
        if (i == size - 2)
            color_n2 = color2;
        gradient.setColorAt(0, color1);
        gradient.setColorAt(1, color2);
        gradient.setStart(line.p1());
        gradient.setFinalStop(line.p2());

        painter->setPen(QPen(QBrush(gradient), penWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
        painter->drawLine(line);


  /*
        int h1 = int((se->get_Q_unsafe(downscale*i)+300)/350*300);
        int h2 = int((se->get_Q_unsafe(downscale*(i+1))+300)/350*300);
        QColor color;
        color.setHsv((h1+h2)/2, 255, 255);
        painter->setPen(QPen(color, penWidth));
        painter->drawLine(line);
*/
    }

    if (is_selected) {
        painter->setPen(QPen(Qt::black, 1));
        painter->drawPolygon(get_shapePolygon());
    }

}

void GEdge::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mousePressEvent(event);
}

void GEdge::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
    if (!is_selected) {
        is_selected = 1;
        graphWidget->select_me(this);
    }
    QGraphicsItem::mouseDoubleClickEvent(event);
}

void GEdge::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
    label->setVisible(true);
    QGraphicsItem::hoverEnterEvent(event);
    graphWidget->update();
}

void GEdge::hoverLeaveEvent(QGraphicsSceneHoverEvent *event)
{
    label->setVisible(false);
    QGraphicsItem::hoverLeaveEvent(event);
    graphWidget->update();
}
